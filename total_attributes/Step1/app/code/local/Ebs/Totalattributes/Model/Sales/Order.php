<?php

class Ebs_Totalattributes_Model_Sales_Order extends Mage_Sales_Model_Order
{
    protected $_realOrder = null;
    protected $_mockShippingAddress = null;
    protected $_mockBillingAddress = null;

    public function setRealOrder($order)
    {
        $this->_realOrder = $order;
        return $this;
    }

    public function __call($method, $args)
    {
        return call_user_func_array(
            array($this->_realOrder, $method),
            $args
        );
    }

    public function getId()
    {
        return $this->_realOrder->getId();
    }

    public function getRealOrder()
    {
        return $this->_realOrder;
    }

    public function getShippingAddress()
    {
        if (is_null($this->_mockShippingAddress)) {
            $realShippingAddress = $this->_realOrder->getShippingAddress();
            $this->_mockShippingAddress = Mage::getModel('ebs_totalattributes/sales_order_address');
            $this->_mockShippingAddress->setRealAddress($realShippingAddress);
        }
        return $this->_mockShippingAddress;
    }

    public function getBillingAddress()
    {
        if (is_null($this->_mockBillingAddress)) {
            $realBillingAddress = $this->_realOrder->getBillingAddress();
            $this->_mockBillingAddress = Mage::getModel('ebs_totalattributes/sales_order_address');
            $this->_mockBillingAddress->setRealAddress($realBillingAddress);
        }
        return $this->_mockBillingAddress;
    }

    public function getShippingDescription()
    {
        $shippingDescription = $this->_realOrder->getShippingDescription();
        $attributesBlock = Mage::app()->getLayout()->getBlock("oa.shipping.method.attributes");
        if ($attributesBlock) {
            $shippingDescription .= $attributesBlock->toHtml();
        }
        return $shippingDescription;
    }

    public function getPayment()
    {
        return $this->_realOrder->getPayment();
    }

    public function hasInvoices()
    {
        return $this->_realOrder->getInvoiceCollection()->count();
    }

    public function hasShipments()
    {
        return $this->_realOrder->getShipmentsCollection()->count();
    }

    public function hasCreditmemos()
    {
        return $this->_realOrder->getCreditmemosCollection()->count();
    }
}
