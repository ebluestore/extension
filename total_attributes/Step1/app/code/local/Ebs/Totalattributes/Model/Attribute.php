<?php

class Ebs_Totalattributes_Model_Attribute extends Mage_Core_Model_Abstract
{
    /**
     *
     */
    protected $_typeInstance = null;

    /**
     *
     */
    protected $_labels  = null;

    /**
     *
     */
    protected $_options = null;

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('ebs_totalattributes/attribute');
    }

    /**
     * @param string $attributeCode
     *
     * @return Ebs_Totalattributes_Model_Attribute
     */
    public function loadByCode($attributeCode)
    {
        return $this->load($attributeCode, 'code');
    }

    /**
     * @return mixed
     */
    public function getLabelCollection()
    {
        return Mage::getResourceModel('ebs_totalattributes/label_collection')->addAttributeFilter($this);
    }

    /**
     * @return Ebs_Totalattributes_Model_Resource_Option_Collection
     */
    public function getOptionCollection()
    {
        return Mage::getResourceModel('ebs_totalattributes/option_collection')->addAttributeFilter($this);
    }

    /**
     * @return Ebs_Totalattributes_Model_Attribute_TypeInterface|null
     */
    public function getTypeModel()
    {
        if (is_null($this->_typeInstance)) {
            $typeCode = $this->getData('type');
            $this->_typeInstance = Mage::getModel('ebs_totalattributes/source_type')->getModelByTypeCode($typeCode);
            if (!is_null($this->_typeInstance)) {
                $this->_typeInstance->setAttribute($this);
            }
        }
        return $this->_typeInstance;
    }


    /**
     * @return array
     */
    public function getStoreLabels()
    {
        if (is_null($this->_labels)) {
            $this->_labels = array();
            foreach ($this->getLabelCollection() as $label) {
                $this->_labels[$label->getStoreId()] = $label->getValue();
            }
        }
        return $this->_labels;
    }

    /**
     * @param int $storeId
     *
     * @return null|string
     */
    public function getLabel($storeId = 0)
    {
        $labels = $this->getStoreLabels();
        if (isset($labels[$storeId])) {
            return $labels[$storeId];
        }
        if (isset($labels[0])) {
            return $labels[0];
        }
        return null;
    }

    /**
     * @return array
     */
    public function getStoreOptions()
    {
        if (is_null($this->_options)) {
            $this->_options = array();
            foreach ($this->getOptionCollection() as $optionId => $option) {
                $this->_options[$optionId]['sort_order'] = $option->getSortOrder();
                $optionValues = $option->getValueCollection();
                foreach ($optionValues as $value) {
                    $this->_options[$optionId]['label'][$value->getStoreId()] = $value->getValue();
                }
            }
        }
        return $this->_options;
    }

    /**
     * @return Ebs_Totalattributes_Model_Attribute
     */
    public function unpackData()
    {
        $this->_getResource()->unserializeFields($this);
        $this->_afterLoad();
        return $this;
    }

    /**
     * Processing object after load data
     *
     * @return Ebs_Totalattributes_Model_Attribute
     */
    protected function _afterLoad()
    {
        if (!is_array($this->getStoreIds())) {
            $this->setStoreIds(explode(',', $this->getStoreIds()));
        }
        if (!is_array($this->getCustomerGroups())) {
            $this->setCustomerGroups(explode(',', $this->getCustomerGroups()));
        }
        if (!is_array($this->getDisplayOn())) {
            $this->setDisplayOn(explode(',', $this->getDisplayOn()));
        }
        return parent::_afterLoad();
    }

    /**
     * Processing object before save data
     *
     * @return Ebs_Totalattributes_Model_Attribute
     */
    protected function _beforeSave()
    {
        if (is_array($this->getStoreIds())) {
            $this->setStoreIds(implode(',', $this->getStoreIds()));
        }
        if (is_array($this->getCustomerGroups())) {
            $this->setCustomerGroups(implode(',', $this->getCustomerGroups()));
        }
        if (is_array($this->getDisplayOn())) {
            $this->setDisplayOn(implode(',', $this->getDisplayOn()));
        }
        return parent::_beforeSave();
    }

    /**
     * @return Mage_Core_Model_Abstract|void
     */
    protected function _afterDelete()
    {
        parent::_afterDelete();
        $this->getTypeModel()->afterAttributeDelete();
    }
}