<?php

class Ebs_Totalattributes_Model_Attribute_Type_Textarea extends Ebs_Totalattributes_Model_Attribute_TypeAbstract
{
    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Grid_Textarea
     */
    protected function _getBackendGridRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Grid_Textarea();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Form_Textarea
     */
    protected function _getBackendFormRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Form_Textarea();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_View_Textarea
     */
    protected function _getBackendViewRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_View_Textarea();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Pdf_Text
     */
    protected function _getBackendPdfRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Pdf_Text();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Frontend_Form_Textarea
     */
    protected function _getFrontendFormRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Frontend_Form_Textarea();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Frontend_View_Textarea
     */
    protected function _getFrontendViewRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Frontend_View_Textarea();
    }

    public function getValueType()
    {
        return Ebs_Totalattributes_Model_Resource_Value::TEXT_TYPE;
    }

    /**
     * @param mixed $value
     */
    public function validate($value)
    {
        $helper = Mage::helper('ebs_totalattributes');
        $storeId = Mage::app()->getStore()->getId();
        $label = $this->getAttribute()->getLabel($storeId);
        $label = Mage::helper('core')->escapeHtml($label);
        if (strlen(trim($value)) < 1) {
            if ($this->getAttribute()->getData('validation_rules/is_required')) {
                throw new Exception($helper->__('%s is required', $label));
            }
        }
        if ($length = $this->getAttribute()->getData('validation_rules/minimum_text_length')) {
            if (!Zend_Validate::is($value, 'StringLength', array('min' => $length, 'max' => null))) {
                throw new Exception($helper->__("%s is less than '%s' symbols", $label, $length));
            }
        }
        if ($length = $this->getAttribute()->getData('validation_rules/maximum_text_length')) {
            if (!Zend_Validate::is($value, 'StringLength', array('min' => 0, 'max' => $length))) {
                throw new Exception($helper->__("%s is greater than '%s' symbols", $label, $length));
            }
        }
        return;
    }
}