<?php

class Ebs_Productlabel_Model_Productlabelflatdata extends Mage_Core_Model_Abstract {
    
    /**
     * Initialize resource
     */
    protected function _construct() {
        $this->_init('productlabel/productlabelflatdata');
    }

    protected function _beforeSave() {
        parent::_beforeSave();
    }

    protected function _afterSave() {
        parent::_afterSave();
    }

} 

?>