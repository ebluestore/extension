<?php

class Ebs_Totalattributes_Model_Attribute_Type_Date extends Ebs_Totalattributes_Model_Attribute_TypeAbstract
{
    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Grid_Date
     */
    protected function _getBackendGridRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Grid_Date();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Form_Date
     */
    protected function _getBackendFormRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Form_Date();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_View_Date
     */
    protected function _getBackendViewRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_View_Date();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Pdf_Date
     */
    protected function _getBackendPdfRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Pdf_Date();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Frontend_Form_Date
     */
    protected function _getFrontendFormRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Frontend_Form_Date();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Frontend_View_Date
     */
    protected function _getFrontendViewRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Frontend_View_Date();
    }

    public function getValueType()
    {
        return Ebs_Totalattributes_Model_Resource_Value::DATE_TYPE;
    }

    /**
     * @param mixed $value
     */
    public function validate($value)
    {
        $helper = Mage::helper('ebs_totalattributes');
        $storeId = Mage::app()->getStore()->getId();
        $label = $this->getAttribute()->getLabel($storeId);
        $label = Mage::helper('core')->escapeHtml($label);
        if (strlen(trim($value)) < 1) {
            if ($this->getAttribute()->getData('validation_rules/is_required')) {
                throw new Exception($helper->__('%s is required', $label));
            }
        } else {
            try {
                Mage::app()->getLocale()->date(
                    $value,
                    Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
                );
            } catch (Exception $e) {
                throw new Exception($helper->__('%s is invalid', $label));
            }
        }
        return;
    }

    /**
     * @param Ebs_Totalattributes_Model_Value $valueModel
     *
     * @return Ebs_Totalattributes_Model_Value
     */
    public function beforeSave($valueModel)
    {
        $value = $valueModel->getData('value');
        if (strlen(trim($value)) > 0) {
            $zDate = Mage::app()->getLocale()->date(
                $value,
                Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
            );
            $value = $zDate->toString(Zend_Date::W3C);
        } else {
            $value = null;
        }
        $valueModel->setData('value', $value);
        return $valueModel;
    }
}