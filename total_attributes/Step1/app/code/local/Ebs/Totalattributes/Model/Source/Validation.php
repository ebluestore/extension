<?php

class Ebs_Totalattributes_Model_Source_Validation
{
    const NONE_CODE           = 'none';
    const NONE_LABEL          = 'None';

    const ALPHANUMERIC_CODE   = 'alphanumeric';
    const ALPHANUMERIC_LABEL  = 'Alphanumeric';

    const NUMERIC_CODE        = 'numeric';
    const NUMERIC_LABEL       = 'Numeric';

    const ALPHA_CODE          = 'alpha';
    const ALPHA_LABEL         = 'Alpha';

    const URL_CODE            = 'url';
    const URL_LABEL           = 'Url';

    const EMAIL_CODE          = 'email';
    const EMAIL_LABEL         = 'Email';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $helper = Mage::helper('ebs_totalattributes');
        return array(
            array(
                'value' => self::NONE_CODE,
                'label' => $helper->__(self::NONE_LABEL),
            ),
            array(
                'value' => self::ALPHANUMERIC_CODE,
                'label' => $helper->__(self::ALPHANUMERIC_LABEL),
            ),
            array(
                'value' => self::NUMERIC_CODE,
                'label' => $helper->__(self::NUMERIC_LABEL),
            ),
            array(
                'value' => self::ALPHA_CODE,
                'label' => $helper->__(self::ALPHA_LABEL),
            ),
            array(
                'value' => self::URL_CODE,
                'label' => $helper->__(self::URL_LABEL),
            ),
            array(
                'value' => self::EMAIL_CODE,
                'label' => $helper->__(self::EMAIL_LABEL),
            ),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $helper = Mage::helper('ebs_totalattributes');
        return array(
            self::NONE_CODE         => $helper->__(self::NONE_LABEL),
            self::ALPHANUMERIC_CODE => $helper->__(self::ALPHANUMERIC_LABEL),
            self::NUMERIC_CODE      => $helper->__(self::NUMERIC_LABEL),
            self::ALPHA_CODE        => $helper->__(self::ALPHA_LABEL),
            self::URL_CODE          => $helper->__(self::URL_LABEL),
            self::EMAIL_CODE        => $helper->__(self::EMAIL_LABEL),
        );
    }
}