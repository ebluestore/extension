<?php

class Ebs_Totalattributes_Model_Source_Showinblock
{
    const BILLING_ADDRESS_BLOCK_CODE   = 1;
    const BILLING_ADDRESS_BLOCK_LABEL  = 'Billing Address';

    const SHIPPING_ADDRESS_BLOCK_CODE  = 2;
    const SHIPPING_ADDRESS_BLOCK_LABEL = 'Shipping Address';

    const SHIPPING_METHOD_BLOCK_CODE  = 3;
    const SHIPPING_METHOD_BLOCK_LABEL = 'Shipping Method';

    const PAYMENT_METHOD_BLOCK_CODE  = 4;
    const PAYMENT_METHOD_BLOCK_LABEL = 'Payment Method';

    const ORDER_REVIEW_BLOCK_CODE  = 5;
    const ORDER_REVIEW_BLOCK_LABEL = 'Order Review';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $helper = Mage::helper('ebs_totalattributes');
        return array(
            array(
                'value' => self::BILLING_ADDRESS_BLOCK_CODE,
                'label' => $helper->__(self::BILLING_ADDRESS_BLOCK_LABEL),
            ),
            array(
                'value' => self::SHIPPING_ADDRESS_BLOCK_CODE,
                'label' => $helper->__(self::SHIPPING_ADDRESS_BLOCK_LABEL),
            ),
            array(
                'value' => self::SHIPPING_METHOD_BLOCK_CODE,
                'label' => $helper->__(self::SHIPPING_METHOD_BLOCK_LABEL),
            ),
            array(
                'value' => self::PAYMENT_METHOD_BLOCK_CODE,
                'label' => $helper->__(self::PAYMENT_METHOD_BLOCK_LABEL),
            ),
            array(
                'value' => self::ORDER_REVIEW_BLOCK_CODE,
                'label' => $helper->__(self::ORDER_REVIEW_BLOCK_LABEL),
            ),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $helper = Mage::helper('ebs_totalattributes');
        return array(
            self::BILLING_ADDRESS_BLOCK_CODE  => $helper->__(self::BILLING_ADDRESS_BLOCK_LABEL),
            self::SHIPPING_ADDRESS_BLOCK_CODE => $helper->__(self::SHIPPING_ADDRESS_BLOCK_LABEL),
            self::SHIPPING_METHOD_BLOCK_CODE  => $helper->__(self::SHIPPING_METHOD_BLOCK_LABEL),
            self::PAYMENT_METHOD_BLOCK_CODE   => $helper->__(self::PAYMENT_METHOD_BLOCK_LABEL),
            self::ORDER_REVIEW_BLOCK_CODE     => $helper->__(self::ORDER_REVIEW_BLOCK_LABEL),
        );
    }
}