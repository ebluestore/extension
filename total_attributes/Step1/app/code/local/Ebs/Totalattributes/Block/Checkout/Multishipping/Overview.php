<?php

class Ebs_Totalattributes_Block_Checkout_Multishipping_Overview extends Mage_Checkout_Block_Multishipping_Overview
{
    public function getBillingAddress()
    {
        $address = parent::getBillingAddress();
        $proxyAddress = new Ebs_Totalattributes_Block_Checkout_Multishipping_Overview_Address($address);
        return $proxyAddress;
    }

    public function getPaymentHtml()
    {
        $block = Mage::app()->getLayout()
            ->createBlock('ebs_totalattributes/checkout_multishipping_attributes_paymentmethod')
            ->setTemplate('ebs_totalattributes/checkout/multishipping/overview/payment_method.phtml')
        ;
        return parent::getPaymentHtml() . $block->toHtml();
    }
}