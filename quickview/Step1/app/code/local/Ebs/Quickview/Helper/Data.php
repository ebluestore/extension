<?php

class Ebs_Quickview_Helper_Data extends Mage_Core_Helper_Abstract{

	public function isEnable()
	{
		return Mage::getStoreConfig('quickview/general/enable');
	}

	public function getJsConfig()
	{
		$isLoadJs = Mage::getStoreConfig('quickview/general/load_jquery');
		if ($this->isEnable() && $isLoadJs) {
			return 'quickview/js/jquery.min.js';
		}
	}

	public function getJsUIConfig()
	{
		$isLoadJsUI = Mage::getStoreConfig('quickview/general/load_jquery_ui');
		if ($this->isEnable() && $isLoadJsUI) {
			return 'quickview/js/jquery-ui.min.js';
		}
	}

	public function getTemplate()
	{
		$version = Mage::getVersion();
		if (version_compare($version, '1.9', '>=')){
		    return 'quickview/product/list_new.phtml';
		} else {
		    return 'quickview/product/list.phtml';
		}
	}
}