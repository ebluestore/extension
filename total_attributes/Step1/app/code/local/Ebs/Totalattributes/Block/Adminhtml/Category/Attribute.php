<?php

class Ebs_Totalattributes_Block_Adminhtml_Category_Attribute extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_category_attribute';
		$this->_blockGroup = 'ebs_totalattributes';
		$this->_headerText = Mage::helper('ebs_totalattributes')->__('Manage Attributes');
		$this->_addButtonLabel = Mage::helper('ebs_totalattributes')->__('Add New Attribute');
        parent::__construct();
    }

}
