<?php

class Ebs_Dailydeal_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $uri = explode('/dailydeal', $_SERVER['REQUEST_URI']);
        $uri1 = explode('/dailydeal/index', $_SERVER['REQUEST_URI']);
        $uri2 = explode('/dailydeal/index/index', $_SERVER['REQUEST_URI']);
        
        if ((sizeof($uri) > 1) || (sizeof($uri1) > 1) || (sizeof($uri2) > 1)) {
            $link = Mage::helper('dailydeal')->getUrlHttp('dailydeal/index', true);
            $this->_redirectUrl($link);
            
        } else {
            $this->loadLayout();
            $this->renderLayout();
        }
        
    }

    public function subscribeAction()
    {
        $email        = $this->getRequest()->getParam('email_address');
        $customerName = $this->getRequest()->getParam('customer_name');
        $model        = Mage::getModel('dailydeal/subscriber');
        $flag         = $model->saveMail($email, $customerName);
        $result       = array();
        if ($flag) {
            $result['error']   = false;
            $result['message'] = Mage::helper('dailydeal/subscriber')->getThanksMessage();
        } else {
            $result['error']   = true;
            $result['message'] = Mage::helper('dailydeal')->__('There was already a subscriber with this email. Please use another email.');
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
    
    public function unsubscribeAction()
    {
        $id   = (int) $this->getRequest()->getParam('id');
        $code = (string) $this->getRequest()->getParam('code');
        if ($id && $code) {
            $session = Mage::getSingleton('core/session');
            try {
                Mage::getModel('dailydeal/subscriber')->load($id)->unsubscribe();
                $session->addSuccess($this->__('You have unsubscribed successfully. Thank you!'));
            }
            catch (Mage_Core_Exception $e) {
                $session->addException($e, $e->getMessage());
            }
            catch (Exception $e) {
                $session->addException($e, $this->__('There was a problem with your unsubscription. Please try again.'));
            }
        }
        Mage::app()->getResponse()->setRedirect(Mage::getUrl());
    }

}