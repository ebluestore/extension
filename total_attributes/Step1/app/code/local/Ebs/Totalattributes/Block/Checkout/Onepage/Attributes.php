<?php


class Ebs_Totalattributes_Block_Checkout_Onepage_Attributes extends Mage_Core_Block_Template
{
    public function isMageMoreThan1500()
    {
        return version_compare(Mage::getVersion(), '1.5.0.0', '<');
    }
}