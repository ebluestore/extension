<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Shopping cart controller
 */
class Ebs_Quickview_CartController extends Mage_Core_Controller_Front_Action
{
    
    /**
     * Action list where need check enabled cookie
     *
     * @var array
     */
    protected $_cookieCheckActions = array();
    
    /**
     * Retrieve shopping cart model object
     *
     * @return Mage_Checkout_Model_Cart
     */
    
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }
    
    /**
     * Get checkout session model instance
     *
     * @return Mage_Checkout_Model_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }
    
    /**
     * Get current active quote instance
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote()
    {
        return $this->_getCart()->getQuote();
    }
    
    /**
     * Initialize product instance from request data
     *
     * @return Mage_Catalog_Model_Product || false
     */
    protected function _initProduct()
    {
        $productId = (int) $this->getRequest()->getParam('product');
        if ($productId) {
            $product = Mage::getModel('catalog/product')->setStoreId(Mage::app()->getStore()->getId())->load($productId);
            if ($product->getId()) {
                return $product;
            }
        }
        return false;
    }
    
    /**
     * Add product to shopping cart action
     */
    protected function getCartLinkText()
    {
        
        $count = Mage::helper('checkout/cart')->getSummaryCount();
        
        if ($count == 1) {
            $text = $this->__('My Cart (%s item)', $count);
        } elseif ($count > 0) {
            $text = $this->__('My Cart (%s items)', $count);
        } else {
            $text = $this->__('My Cart');
        }
        
        
        return $text;
    }
    
    public function addAction()
    {
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        
        try {
            
            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');
            
            if (isset($params['isajaxform'])) {
                
                $_min_qty      = Mage::getBlockSingleton("catalog/product_view")->getMinimalQty($product);
                $_product_name = Mage::helper('catalog/output')->productAttribute($product, $product->getName(), 'name');
                $response      = array(
                    "result" => "succes",
                    "min_qty" => $_min_qty,
                    "product_name" => $_product_name
                );
                
                $this->getResponse()->clearHeaders()->setHeader('Content-Type', 'application/json')->setBody(json_encode($response));
                return;
            }
            if (isset($params['qty'])) {
                $filter        = new Zend_Filter_LocalizedToNormalized(array(
                    'locale' => Mage::app()->getLocale()->getLocaleCode()
                ));
                $params['qty'] = $filter->filter($params['qty']);
            }
            
            
            /**
             * Check product availability
             */
            if (!$product) {                
                $response = array(
                    "result" => "error",
                    "message" => $this->getLayout()->createBlock('core/messages')->addError($this->__("No product was specified"))->toHtml()
                );
                
                $this->getResponse()->clearHeaders()->setHeader('Content-Type', 'application/json')->setBody(json_encode($response));
                return;
            }
            
            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }
            $cart->save();
            $this->_getSession()->setCartWasUpdated(true);
            
            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete', array(
                'product' => $product,
                'request' => $this->getRequest(),
                'response' => $this->getResponse()
            ));
            if (!$cart->getQuote()->getHasError()) {
                $_cart_block = $this->getLayout()->createBlock('checkout/cart_sidebar')->setTemplate('checkout/cart/sidebar.phtml')->toHtml();
                $_cart_block = str_replace("/uenc/", "/_uenc/", $_cart_block);

                $_mini_cart = $this->getLayout()->createBlock('checkout/cart_sidebar')->setTemplate('checkout/cart/minicart/items.phtml')->toHtml();
                $_mini_cart = str_replace("/uenc/", "/_uenc/", $_mini_cart);

                $_cart_num = Mage::helper('checkout/cart')->getSummaryCount();

                $response = array(
                    "result" => "success",
                    "title" => $this->__("Shopping Cart"),
                    "message" => $this->getLayout()->createBlock('core/messages')->addSuccess($this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($product->getName())))->toHtml(),
                    // "cart_summary" => $_cart_block->setTemplate('quickview/cart_summary.phtml')->toHtml(),
                    "cart_sidebar" => $_cart_block,
                    "minicart" => $_mini_cart,
                    "cart_head" => $_cart_num,
                    "cart_link_text" => $this->getCartLinkText()
                );
            } else {
                
                $response = array(
                    "result" => "error",
                    "message" => $this->getLayout()->createBlock('core/messages')->addError($this->__('There is an error in your cart.'))->toHtml()
                );
            }
        }
        catch (Mage_Core_Exception $e) {
            $message  = nl2br($e->getMessage());
            $response = array(
                "result" => "error",
                "message" => $this->getLayout()->createBlock('core/messages')->addError($message)->toHtml()
            );
        }
        catch (Exception $e) {
            $response = array(
                "result" => "error",
                "message" => $this->getLayout()->createBlock('core/messages')->addError($this->__('Cannot add the item to shopping cart.'))->toHtml()
            );
        }
        
        $this->getResponse()->clearHeaders()->setHeader('Content-Type', 'application/json')->setBody(json_encode($response));
 
        return false;
    }   
}