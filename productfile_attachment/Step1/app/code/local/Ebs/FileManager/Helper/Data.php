<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @author      Ebluestore
 * @package     Ebs_FileManager
 * @copyright   Copyright (c) 2014 Ebluestore
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ebs_FileManager_Helper_Data extends Mage_Core_Helper_Abstract
{
protected $_productfile;    
    
    public function assignProducts($productfile,$productIds)
    {
        
        $Fproduct = Mage::getModel('ebs_filemanager/fproduct');
        
        if($productIds == array(0))
            return $this;            
        
        if(!count($productIds))
            $productIds = array(0);    
        foreach($productIds as $key=>$productId)
        {
            if($productId)
            {
                $Fproduct->loadByFilePrd($productfile->getId(),$productId);
                $Fproduct->setProductfileId($productfile->getId());
                $Fproduct->setProductId($productId);
                $Fproduct->save();
                $Fproduct->setId(null);
            }else{
                unset($productIds[$key]);
            }
        }
        
        if(!count($productIds))
            $productIds = array(0);
        
        $collection = Mage::getResourceModel('ebs_filemanager/fproduct_collection')
                        ->addFieldToFilter('product_id',array('nin'=>$productIds))
                        ->addFieldToFilter('productfile_id',$productfile->getId());
        
        if(count($collection))
        foreach($collection as $item)
            $item->delete();
        
        return $this;    
            
    }
    
    public function assignProductfiles($productfiles,$productId)
    {
        $productfileIds = array(0);
        $Fproduct = Mage::getModel('ebs_filemanager/fproduct');
        
        if($productId)
        {
            if (count($productfiles)) {
                foreach($productfiles as $productfile) {
                    $Fproduct->loadByFilePrd($productfile->getId(),$productId);
                    $Fproduct->setProductfileId($productfile->getId());
                    $Fproduct->setProductId($productId);
                    $Fproduct->save();
                    $Fproduct->setId(null);
                    $productfileIds[] = $productfile->getId();
                }
            }
        }
        
        $collection = Mage::getResourceModel('ebs_filemanager/fproduct_collection')
                        ->addFieldToFilter('productfile_id',array('nin'=>$productfileIds))
                        ->addFieldToFilter('product_id',$productId);
        
        if(count($collection))
        foreach($collection as $item)
            $item->delete();
        
        return $this;    
            
    }
    
    public function getProductfileUrl()
    {
        $url = $this->_getUrl("ebs_filemanager/index/view",array());
        return $url;    
    }
    
    function formatSize($size) 
    {
      $sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
      if ($size == 0) { return('n/a'); } else {
      return (round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizes[$i]); }
    }
    
    public function init($productfile)
    {
        $this->_productfile = $productfile;
        return $this;    
    }
    
} 

?>