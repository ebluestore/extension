<?php
class Ebs_OneStepCheckout_Model_Observer extends Mage_Core_Model_Abstract
{
    
    public function setAdditionalData($observer)
    {
        $enableComments = Mage::getStoreConfig('onestepcheckout/exclude_fields/enable_comments');
        $enableCommentsDefault = Mage::getStoreConfig('onestepcheckout/exclude_fields/enable_comments_default');
        $enableFeedback = Mage::getStoreConfig('onestepcheckout/feedback/enable_feedback');
        $orderComment = $this->_getRequest()->getPost('onestepcheckout_comments');
        $orderComment = trim($orderComment);
        $deliveryDate = $this->_getRequest()->getPost('deliverydate');

        if($enableComments && !$enableCommentsDefault) {
            if ($orderComment != ""){
                $observer->getEvent()->getOrder()->setOnestepcheckoutCustomercomment(Mage::helper('core')->escapeHtml($orderComment));
            }
        }

        if($enableComments && $enableCommentsDefault) {
            if ($orderComment != ""){
                $observer->getEvent()->getOrder()->setState($observer->getEvent()->getOrder()->getStatus(), true, Mage::helper('core')->escapeHtml($orderComment), false );
            }
        }

        if($enableFeedback){

            $feedbackValues = unserialize(Mage::getStoreConfig('onestepcheckout/feedback/feedback_values'));
            $feedbackValue = $this->_getRequest()->getPost('onestepcheckout-feedback');
            $feedbackValueFreetext = $this->_getRequest()->getPost('onestepcheckout-feedback-freetext');

            if(!empty($feedbackValue)){
                if($feedbackValue!='freetext'){
                    $feedbackValue = $feedbackValues[$feedbackValue]['value'];
                } else {
                    $feedbackValue = $feedbackValueFreetext;
                }

                $observer->getEvent()->getOrder()->setOnestepcheckoutCustomerfeedback(Mage::helper('core')->escapeHtml($feedbackValue));
            }
        }

        if ($deliveryDate != ""){
            if ($deliveryDate == 'now') {
                $observer->getEvent()->getOrder()->setOnestepcheckoutDeliverytime('As soon as possible');
            } else {
                $_date =  $this->_getRequest()->getPost('delivery_date');
                $observer->getEvent()->getOrder()->setOnestepcheckoutDeliverydate(Mage::helper('core')->escapeHtml($_date));
            }
        }
    }

    public function initialize_checkout($observer)
    {
        $helper = Mage::helper('onestepcheckout/checkout');
    }

}