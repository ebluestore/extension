<?php

class Ebs_Dailydeal_Adminhtml_SubscriberController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('dailydeal/dealsubscriber')->_addBreadcrumb(Mage::helper('adminhtml')->__('Subscriber Manager'), Mage::helper('adminhtml')->__('Subscriber Manager'));
        return $this;
    }
    
    public function indexAction()
    {
        $this->_initAction()->renderLayout();
    }
    
    public function editAction()
    {        
        $this->_title($this->__("Subscriber"));
        $this->_title($this->__("Edit Item"));
        
        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("dailydeal/subscriber")->load($id);
        if ($model->getId()) {
            Mage::register("subscriber_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("dealsubscriber/items");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Subscriber Manager"), Mage::helper("adminhtml")->__("Subscriber Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Subscriber Description"), Mage::helper("adminhtml")->__("Subscriber Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("dailydeal/adminhtml_subscriber_edit"))->_addLeft($this->getLayout()->createBlock("dailydeal/adminhtml_subscriber_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("dailydeal")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('dailydeal/subscriber');
                $model->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
                $this->_redirect('*/*/');
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')
                ));
            }
        }
        $this->_redirect('*/*/');
    }
    
    public function massDeleteAction()
    {
        $dailydealIds = $this->getRequest()->getParam('dailydeal');
        if (!is_array($dailydealIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($dailydealIds as $dailydealId) {
                    $dailydeal = Mage::getModel('dailydeal/subscriber')->load($dailydealId);
                    $dailydeal->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Total of %d record(s) were successfully deleted', count($dailydealIds)));
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
    
    public function massStatusAction()
    {
        $dailydealIds = $this->getRequest()->getParam('dailydeal');
        if (!is_array($dailydealIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($dailydealIds as $dailydealId) {
                    $dailydeal = Mage::getSingleton('dailydeal/subscriber')->load($dailydealId)->setStatus($this->getRequest()->getParam('status'))->setIsMassupdate(true)->save();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d record(s) were successfully updated', count($dailydealIds)));
            }
            catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
    
    public function exportCsvAction()
    {
        $fileName = 'subscriber.csv';
        $content  = $this->getLayout()->createBlock('dailydeal/adminhtml_subscriber_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }
    
    public function exportXmlAction()
    {
        $fileName = 'subscriber.xml';
        $content  = $this->getLayout()->createBlock('dailydeal/adminhtml_subscriber_grid')->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }
    
    public function importAction()
    {
        $this->_initAction()->renderLayout();
    }
    
    public function saveImportAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $flag = false;
            $i    = 0;
            if (isset($_FILES['subscriber_csv_file']['name']) && $_FILES['subscriber_csv_file']['name'] != '') {
                try {
                    $uploader = new Varien_File_Uploader('subscriber_csv_file');
                    $uploader->setAllowedExtensions(array('csv'));
                    $uploader->setAllowRenameFiles(false);
                    $uploader->setFilesDispersion(false);
                    $path = Mage::getBaseDir('media') . DS . 'dailydeal' . DS . 'subscriber' . DS;
                    $uploader->save($path, $_FILES['subscriber_csv_file']['name']);
                    $filepath   = $path . $_FILES['subscriber_csv_file']['name'];
                    $handler    = new Varien_File_Csv();
                    $importData = $handler->getData($filepath);
                    $keys       = $importData[0];
                    foreach ($keys as $key => $value) {
                        $keys[$key] = str_replace(' ', '_', strtolower($value));
                    }
                    $count             = count($importData);
                    $model             = Mage::getModel('dailydeal/subscriber');
                    $collection        = $model->getCollection();
                    $subscribersImport = array();
                    
                    while (--$count > 0) {
                        Mage::log('item' . $count);
                        $currentData    = $importData[$count];
                        $data           = array_combine($keys, $currentData);
                        // Set status "Enable" for all subscribers
                        $data['status'] = 1;
                        if (!Mage::helper('dailydeal/subscriber')->checkEmailSubscriber($data['email'])) {
                            $model->setData($data)->save();
                            $flag = true;
                            $i++;
                        }
                    }
                }
                catch (Exception $e) {
                    
                }
            }
            if ($flag) {
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('dailydeal')->__('Total of %d subscriber(s) were successfully imported', $i));
            } else {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('dailydeal')->__('There is no item to import'));
            }
            $this->_redirect('*/adminhtml_subscriber/index');
        }
    }

    public function saveAction()
    {
        $id = $this->getRequest()->getParam("id");
        $post_data = $this->getRequest()->getPost();
        try {            
            $model = Mage::getModel("dailydeal/subscriber")->addData($post_data)->setId($id)->save();
            
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Subscriber was successfully saved"));
            Mage::getSingleton("adminhtml/session")->setSubscriberData(false);
            
            if ($this->getRequest()->getParam("back")) {
                $this->_redirect("*/*/edit", array(
                    "id" => $model->getId()
                ));
                return;
            }
            $this->_redirect("*/adminhtml_subscriber/");
            return;
        }
        catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
            Mage::getSingleton("adminhtml/session")->setNewsData($this->getRequest()->getPost());
            $this->_redirect("*/*/edit", array(
                "id" => $id
            ));
            return;
        }
        $this->_redirect("*/adminhtml_subscriber/");
    }
}