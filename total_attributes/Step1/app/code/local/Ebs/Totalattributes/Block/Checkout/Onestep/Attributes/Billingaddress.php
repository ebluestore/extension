<?php


class Ebs_Totalattributes_Block_Checkout_Onestep_Attributes_Billingaddress extends Ebs_Totalattributes_Block_Checkout_Onestep_Attributes_Abstract
{
    /**
     * @return Ebs_Totalattributes_Model_Resource_Attribute_Collection
     */
    public function getAttributeCollection()
    {
        /** @var Ebs_Totalattributes_Model_Resource_Attribute_Collection $collection */
        $collection = parent::getAttributeCollection();
        $collection->addShowInBillingAddressBlockFilter();
        return $collection;
    }
}