// function listminicart(cellid,tomorrow){
//     var urlpath = '<?php echo $this->getUrl('dailydeal/deal/ajaxdeal') . '?currenttime='; ?>'+tomorrow; 
//     new Ajax.Request(urlpath,{
//         method: "GET",
//         onSuccess:  function(resp){
//             $('cell-'+cellid).addTip(resp.responseText, {style: 'grouped'});
//         }
//     });
// }

function countDownSecond(obj) {       
    if( obj.second > 0){
        obj.second--;
        
        setTimeout(function() {
            if(obj.callback != ''){
                obj.callback(obj);
            }else{
                showTime(obj);
            }
        }, 0);
        setTimeout(function() {
            countDownSecond(obj);
        }, 1000);
    }
}

function showTime(obj) {
    var seconds = Math.floor(obj.second);
    var minutes = Math.floor(seconds / 60);
    var hours = Math.floor(minutes / 60);
    var days = Math.floor(hours / 24);
    
    hours %= 24;
    minutes %= 60;
    seconds %= 60;
    
    var str_days = wrapperTagSpan ( insertOneZero(days) );
    var str_hours = wrapperTagSpan ( insertOneZero(hours) );
    var str_minutes = wrapperTagSpan ( insertOneZero(minutes) );
    var str_seconds = wrapperTagSpan ( insertOneZero(seconds) );
    
    if ($(obj.label_day)){
        if(obj.label_day_value){
            
            $(obj.label_day).innerHTML     = obj.label_day_value;
        }else{
            $(obj.label_day).innerHTML   = 'Days';
        }
    }
        
    if ($(obj.label_hour)) {
        if(obj.label_hour_value){
            $(obj.label_hour).innerHTML     = obj.label_hour_value;
        }else{
            $(obj.label_hour).innerHTML     = 'Hours';
        }
    }
        
    if ($(obj.label_minute)){
        if(obj.label_minute_value){
            $(obj.label_minute).innerHTML     = obj.label_minute_value;
        }else{
            $(obj.label_minute).innerHTML   = 'Minutes';
        }
    }
        
    if ($(obj.label_second)){
        if(obj.label_second_value){
            $(obj.label_second).innerHTML     = obj.label_second_value;
        }else{
            $(obj.label_second).innerHTML   = 'Seconds';
        }
    }
        
    if($(obj.id_day)) $(obj.id_day).innerHTML    = str_days;
    if($(obj.id_hour)) $(obj.id_hour).innerHTML    = str_hours;
    if($(obj.id_minute)) $(obj.id_minute).innerHTML    = str_minutes;
    if($(obj.id_second)) $(obj.id_second).innerHTML    = str_seconds;
        
    if(days <= 0){
        if ($(obj.label_day)) $(obj.label_day).innerHTML     = '';
        if($(obj.id_day)) $(obj.id_day).innerHTML    = '';
    }
}

function insertOneZero(value) {
    var result = '';
        
    if(value < 10){
        result += '0' + value;
    }else{
        result += value;
    }   
    return result;
}

function wrapperTagSpan(string) {
    var result = '';   
    string.toString();    
    for(var i=0; i<string.length; i++) {
        result += "<span>" + string.charAt(i)+"</span>";
    }
    return result;
}

function submit_dailydeal_newsletter(newsletter_url) {
    var parameters = {
        email_address: $('dailydeal_email').value,
        customer_name: $('dailydeal_customer_name').value
    };
    show_loading(true);
    var request = new Ajax.Request(
        newsletter_url, {
            parameters: parameters,
            method: 'post',
            onSuccess: function(transport) {
                var data = transport.responseText.evalJSON();
                if (data.error) {  
                    $('subscribe-result-message').addClassName('error');                  
                    $('subscribe-result-message').update(data.message);
                } else { 
                    $('subscribe-result-message').addClassName('success');                  
                    $('subscribe-result-message').update(data.message);
                }
                show_loading(false);
            },            
        }
    );
}

function show_loading(is_show) {
    if (is_show == true) {
        $('dailydeal-subscription-form').hide();
        $('subscribe-form-ajax').show();
    } else {   
        $('dailydeal-subscription-form').show();  
        $('subscribe-form-ajax').hide();
    }
}

jQuery(document).ready(function() {
    jQuery("#today-deal").owlCarousel({
        singleItem: true,
        lazyLoad: true,
        navigation: true,
        pagination: false
    });
    jQuery(".mini-products-list").owlCarousel({
        singleItem: true,
        lazyLoad: true,
        navigation: true,
        pagination: false
    });
});