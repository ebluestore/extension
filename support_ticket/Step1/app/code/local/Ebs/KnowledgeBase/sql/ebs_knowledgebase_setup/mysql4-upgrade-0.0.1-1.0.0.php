<?php

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Setup */

$installer->startSetup();

$collection = Mage::getModel('knowledgebase/faq')->getCollection();
$default = 0;
foreach ($collection as $row) {
    Mage::getModel('knowledgebase/faq_store')
        ->setFaqId($row->getId())
        ->setCategoryId($default)
        ->save();
}

$installer->endSetup();