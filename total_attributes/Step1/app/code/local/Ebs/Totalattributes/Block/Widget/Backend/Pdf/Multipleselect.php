<?php

class Ebs_Totalattributes_Block_Widget_Backend_Pdf_Multipleselect
    extends Ebs_Totalattributes_Block_Widget_Backend_PdfAbstract
{
    public function getLabel()
    {
        return $this->_getLabel();
    }

    public function getValue()
    {
        $labels = array();
        foreach($this->_getOptionsForSelect() as $value => $valueLabel) {
            if (in_array($value, $this->_getValue())) {
                $labels[] = $valueLabel;
            }
        }
        return $labels;
    }

    private function _getOptionsForSelect()
    {
        if (is_null($this->getTypeModel())) {
            return null;
        }
        $attribute = $this->getTypeModel()->getAttribute();
        if (is_null($attribute)) {
            return null;
        }
        $storeId = Mage::app()->getStore()->getId();
        $options = Mage::helper('ebs_totalattributes/options')->getOptionsForAttributeByStoreId($attribute, $storeId, false);
        foreach($options as $key => $value) {
            $options[$key] = htmlspecialchars($value);
        }
        return $options;
    }

    protected function _getValue()
    {
        $value = parent::_getValue();
        if (is_string($value)) {
            $value = explode(',', $value);
        }
        if (!is_array($value)) {
            $value = array();
        }
        return $value;
    }
}