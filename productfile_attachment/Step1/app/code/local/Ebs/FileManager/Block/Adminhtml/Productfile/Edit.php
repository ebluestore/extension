<?php

class Ebs_FileManager_Block_Adminhtml_Productfile_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'ebs_filemanager';
        $this->_controller = 'adminhtml_productfile';
        
        $this->_updateButton('save', 'label', Mage::helper('ebs_filemanager')->__('Save File'));
        $this->_updateButton('delete', 'label', Mage::helper('ebs_filemanager')->__('Delete File'));
        
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('productfile_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'productfile_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'productfile_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('productfile_data') && Mage::registry('productfile_data')->getId() ) {
            return Mage::helper('ebs_filemanager')->__("Edit File '%s'", $this->htmlEscape(Mage::registry('productfile_data')->getTitle()));
        } else {
            return Mage::helper('ebs_filemanager')->__('Add File');
        }
    }
} 

?>