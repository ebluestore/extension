<?php

class Ebs_Totalattributes_Block_Sales_Order_Info extends Mage_Sales_Block_Order_Info
{
    protected $_mockOrder = null;

    public function getOrder()
    {
        if (is_null($this->_mockOrder)) {
            $realOrderObject =  Mage::registry('current_order');
            $this->_mockOrder = Mage::getModel('ebs_totalattributes/sales_order')->setRealOrder($realOrderObject);
        }
        return $this->_mockOrder;
    }

    public function addLink($name, $path, $label)
    {
        $this->_links[$name] = new Varien_Object(array(
            'name' => $name,
            'label' => $label,
            'url' => empty($path) ? '' : Mage::getUrl($path, array('order_id' => Mage::registry('current_order')->getId()))
        ));
        return $this;
    }

    public function getPaymentInfoHtml()
    {
        $paymentInfoHtml = parent::getPaymentInfoHtml();
        $attributesBlock = Mage::app()->getLayout()->getBlock("oa.payment.method.attributes");
        if ($attributesBlock) {
            $paymentInfoHtml .= $attributesBlock->toHtml();
        }
        return $paymentInfoHtml;
    }

    /**
     * @param mixed $data
     * @param null $allowedTags
     * @return mixed|string
     */
    public function escapeHtml($data, $allowedTags = null)
    {
        return $data;
    }
}