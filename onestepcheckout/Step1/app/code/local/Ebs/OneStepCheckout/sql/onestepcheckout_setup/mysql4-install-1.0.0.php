<?php

$installer = $this;
/* @var $installer Mage_Sales_Model_Mysql4_Setup */

$installer->startSetup();

$resource = Mage::getResourceModel('sales/order_collection');
if(!method_exists($resource, 'getEntity'))   {
    $connection = $installer->getConnection();
    $connection->addColumn($installer->getTable('sales/order'), 'onestepcheckout_customercomment', "TEXT CHARACTER SET utf8 DEFAULT NULL");
    $connection->addColumn($installer->getTable('sales/order'), 'onestepcheckout_customerfeedback', "TEXT CHARACTER SET utf8 DEFAULT NULL");    
    $connection->addColumn($installer->getTable('sales/order'), 'onestepcheckout_deliverydate', "VARCHAR(255) CHARACTER SET utf8");
} else {
    $comment  = array(
        'type'              => 'textarea',
        'backend_type'      => 'text',
        'frontend_input'    => 'textarea',
        'is_user_defined'   => true,
        'label'             => 'Customer Comment',
        'visible'           => true,
        'visible_on_front'  => true,
        'global'            => true,
        'required'          => false,        
        'user_defined'      => false,
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'default'           =>  ''    
    );
    $feedback  = array(
        'type'              => 'textarea',
        'backend_type'      => 'text',
        'frontend_input'    => 'textarea',
        'is_user_defined'   => true,
        'label'             => 'Customer Feedback',
        'visible'           => true,
        'visible_on_front'  => true,
        'global'            => true,
        'required'          => false,        
        'user_defined'      => false,
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'default'           =>  ''    
    );
    $shipping_date  = array(
        'type'              => 'varchar',
        'backend_type'      => 'varchar',
        'frontend_input'    => 'varchar',
        'is_user_defined'   => true,
        'label'             => 'Delivery Date',
        'visible'           => true,
        'visible_on_front'  => true,
        'global'            => true,
        'required'          => false,        
        'user_defined'      => false,
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'default'           =>  ''    
    );
    $installer->addAttribute("order", "onestepcheckout_customercomment", $comment);
    $installer->addAttribute("order", "onestepcheckout_customerfeedback", $feedback);
    $installer->addAttribute("order", "onestepcheckout_deliverydate", $shipping_date);
}

$installer->endSetup();
