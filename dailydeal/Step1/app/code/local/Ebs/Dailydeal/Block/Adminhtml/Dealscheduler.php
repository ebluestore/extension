<?php

class Ebs_Dailydeal_Block_Adminhtml_Dealscheduler extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_dealscheduler';
        $this->_blockGroup = 'dailydeal';
        $this->_headerText = Mage::helper('dailydeal')->__('Deal Campaign');
        $this->_addButtonLabel = Mage::helper('dailydeal')->__('Add Deal Campaign');
        parent::__construct();
    }

}