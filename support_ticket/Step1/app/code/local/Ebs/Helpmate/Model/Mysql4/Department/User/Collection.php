<?php

class Ebs_Helpmate_Model_Mysql4_Department_User_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('helpmate/department_user');
    }

    public function addDepartmentFilter($departmentId)
    {
        if (!is_array($departmentId)) {
            $departments = array($departmentId);
        }
        $this->getSelect()->where('main_table.department_id IN (?)', $departments);
        return $this;
    }
    
    public function addUserFilter($user)
    {
        if (!is_array($user)) {
            $users = array($user);
        }
        $this->getSelect()->where('main_table.user_id IN (?)', $users);
        return $this;
    }

    public function addEmailData()
    {
        $this->getSelect()->join(
            array('u' => $this->getTable('admin/user')),
            'u.user_id = main_table.user_id',
            'email'
        );
        return $this;
    }

}