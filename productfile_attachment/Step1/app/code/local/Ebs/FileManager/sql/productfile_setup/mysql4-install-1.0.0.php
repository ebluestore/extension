<?php

$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('productfile')};
CREATE TABLE  {$this->getTable('productfile')} (
    `productfile_id` int(11) unsigned NOT NULL auto_increment,
    `title` varchar(255) NOT NULL,
    `filename` varchar(255) NOT NULL,
    `fileextension` varchar(255) NOT NULL,
    `filesize` int(11) NOT NULL,
    `filepath` text NOT NULL,
    `content` text NOT NULL,
    `status` smallint(6) NOT NULL DEFAULT '1',
     PRIMARY KEY (`productfile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS {$this->getTable('productfile_product')};
CREATE TABLE  {$this->getTable('productfile_product')} (
    `productfile_product_id` int(11) unsigned NOT NULL auto_increment,
    `productfile_id` int(11) unsigned NOT NULL,
    `product_id` int(11) unsigned NOT NULL,
     UNIQUE(`productfile_id`,`product_id`),
     FOREIGN KEY (`productfile_id`) REFERENCES {$this->getTable('productfile')} (`productfile_id`) ON DELETE CASCADE ON UPDATE CASCADE,
     FOREIGN KEY (`product_id`) REFERENCES {$this->getTable('catalog/product')} (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,  
     PRIMARY KEY (`productfile_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS {$this->getTable('productfile_value')}; 
CREATE TABLE {$this->getTable('productfile_value')} (   
    `value_id` int(10) unsigned NOT NULL auto_increment,   
    `productfile_id` int(11) unsigned NOT NULL,   
    `store_id` smallint(5) unsigned  NOT NULL,   
    `attribute_code` varchar(63) NOT NULL default '',   
    `value` text NOT NULL,   
     UNIQUE(`productfile_id`,`store_id`,`attribute_code`),   
     INDEX (`productfile_id`),   
     INDEX (`store_id`),   
     FOREIGN KEY (`productfile_id`) 
     REFERENCES {$this->getTable('productfile')} (`productfile_id`) 
     ON DELETE CASCADE ON UPDATE CASCADE,   
     FOREIGN KEY (`store_id`) 
     REFERENCES {$this->getTable('core/store')} (`store_id`) 
     ON DELETE CASCADE ON UPDATE CASCADE,   
     PRIMARY KEY (`value_id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    

");

$installer->endSetup();  