<?php

class Ebs_FileManager_Block_Productfile extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
    public function setProductfileBlock($alias)
    {
        $this->setData('alias',$alias);
    }
           
    public function getProductfile()
    {
        $collection = Mage::getModel('ebs_filemanager/productfile')
                                ->getProductfiles()
                                ->addFieldToFilter('status', 1);
        return $collection;
    }
    
    public function getProduct()
    {
        $collection = Mage::getModel('ebs_filemanager/fproduct')
                    ->getCollection()
                    ->addFieldToFilter('product_id',$this->getRequest()->getParam('id'));
                                                        
        return $collection;    
    }

    public function canViewFile()
    {
        return Mage::getStoreConfig('ebs_filemanager/productfile/enable_viewfile');
    }
} 

?>