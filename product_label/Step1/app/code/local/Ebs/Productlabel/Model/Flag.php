<?php

class Ebs_Productlabel_Model_Flag extends Mage_Core_Model_Flag
{
    /**
     * Flag code
     *
     * @var string
     */
    protected $_flagCode = 'label_rules_dirty';
}

?>