<?php

class Ebs_FileManager_Block_Adminhtml_File extends Mage_Adminhtml_Block_Template
{
    
  	public function fileProductfile()
  	{
    	$collection = null;
    	$id = $this->getRequest()->getParam('id');    
    	if($id)
    	{
        	$collection = Mage::getModel('ebs_filemanager/productfile')->setStoreId($this->getRequest()->getParam('store'))->load($id);
    	}
    	return $collection;
  	}
} 

?>