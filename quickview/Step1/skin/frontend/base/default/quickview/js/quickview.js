jQuery(document).ready(function() {
    jQuery(".item .product-image").live("mouseenter", function() {
        jQuery(this).parent().find(".quick-view-link").show();
    }).live("mouseleave", function() {
        jQuery(this).parent().find(".quick-view-link").hide();
    });

    jQuery(".quick-view-link").button().each(function(i) {
        _this = jQuery(this);
        x = ((_this.parent().width()) - (_this.outerWidth())) * 0.5;
        jQuery(this).css({
            'left': x
        });
    }).live("click", function() {
        url = jQuery(this).attr("rel");
        jQuery("#quickview-dialog").dialog({
            width: window._quickview_dialogWidth,
            /*height: 400,*/
            modal: true,
            open: function(event, ui) {
                window._quickview_Loading();
                jQuery.get(url,
                    function(data) {
                        window._quickview_ReloadJsonData(data);
                        //  jQuery( "#quickview-dialog" ).dialog("option" , "title", data.product_name);
                        jQuery("#quickview-tabs").tabs();
                        jQuery("#quickview-options-tabs").tabs();
                        window._quickview_AfterLoading();

                    }, "json");
            },
            buttons: window.quickview_dialogButtons,
            close: function(event, ui) {
                window._quickview_CloseDialog();
            }
        });
        return false;
    });

    jQuery(".quickview-more-views ul li a").live("click", function() {
        jQuery(".quickview-product-image img#quickview-product-image").attr("src", jQuery(this).attr("rel"));
        return false;
    });
});

function _quickview_CloseDialog() {
    jQuery("#quickview-dialog .quickview-message").html("");
    jQuery("#quickview-dialog .quickview-summary").html("");
    jQuery(".ui-dialog .ui-dialog-buttonpane, .quickview-qty-form, #quickview-dialog .product-name").hide();
    /*for configurable options*/
    truncateOptions();
    return;
}

function _quickview_Loading() {
    jQuery("#quickview-dialog").dialog("option", "title", window._quickview_loading_text);
    jQuery("#quickview-dialog .quickview-message").hide().html("");
    jQuery("#quickview-dialog .quickview-summary").hide().html("");
    jQuery(".ui-dialog .ui-dialog-buttonpane, .quickview-qty-form, #quickview-dialog .product-name").hide();
    jQuery("#quickview-dialog .quickview-loader").show();
    return;
}


function _quickview_AfterLoading() {
    jQuery("#quickview-dialog .quickview-loader").hide();
    jQuery(".ui-dialog .ui-dialog-buttonpane").show();
    jQuery("#quickview-dialog").dialog({
        position: {
            my: "center",
            at: "center",
            of: window
        },
        resizable: false,
        draggable: false
    });
    return;
}


function _quickview_ReloadJsonData(data) {
    if (data.title) jQuery("#quickview-dialog").dialog("option", "title", data.title);
    if (data.cart_sidebar) jQuery(".block.block-cart").replaceWith(data.cart_sidebar);
    jQuery("#quickview-dialog .quickview-message").html(data.message).fadeIn();
    if (data.cart_link_text) jQuery(".top-link-cart").text(data.cart_link_text);    
    if (data.cart_head) jQuery(".header-minicart .skip-cart .count").text(data.cart_head);
    if (data.minicart) jQuery(".header-minicart #header-cart").html(data.minicart);
    return;
}


function _quickview_addtocart_ConvertUrl(url) {
    if (url.indexOf("/checkout/cart/add") >= 0) {
        url = url.replace("/checkout/", "/quickview/");
        return url;
    } else {
        return false;
    }
}