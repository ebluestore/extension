<?php

class Ebs_Akismet_Helper_Data extends Mage_Core_Helper_Abstract
{
    const LOGFILE = 'akismet.log';
    
    public function isActive()
    {
        return Mage::getSingleton('akismet/akismet')->isActive();
    }
    
    public function log($message)
    {
        Mage::log($message, null, self::LOGFILE, true);
    }
}