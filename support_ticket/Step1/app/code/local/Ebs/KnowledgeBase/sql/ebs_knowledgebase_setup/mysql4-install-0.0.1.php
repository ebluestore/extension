<?php

$installer = $this;


$installer->startSetup();


$installer->run("
    
-- DROP TABLE IF EXISTS {$this->getTable('ebs_knowledgebase_faq')};
    CREATE TABLE IF NOT EXISTS {$this->getTable('ebs_knowledgebase_faq')} (
        `id` int(11) unsigned NOT NULL auto_increment,
        `title` VARCHAR(200) NULL,
        `meta_keywords` text NOT NULL,
        `meta_description` text NOT NULL,
        `content` text,
        `identifier` varchar(255) NOT NULL DEFAULT '',
        `author` mediumint(9) UNSIGNED DEFAULT NULL,
        `status` tinyint(1) NOT NULL default 1,
        `rate` int(11) unsigned NOT NULL,
        `created_at` datetime DEFAULT NULL,
        `modified_at` datetime DEFAULT NULL,
        `sort_order` INT NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    FULLTEXT KEY (title,content)
) ENGINE = MyISAM DEFAULT CHARSET=utf8;


-- DROP TABLE IF EXISTS {$this->getTable('ebs_knowledgebase_category')};
    CREATE  TABLE IF NOT EXISTS {$this->getTable('ebs_knowledgebase_category')} (
        `id` int(11) unsigned NOT NULL auto_increment,
        `name` VARCHAR(45) NULL ,
        `identifier` varchar(255) NOT NULL DEFAULT '',
        `store_id` smallint(5) unsigned NOT NULL,
        `active` tinyint(1) NOT NULL default 1,
        `created_at` datetime NULL,
        `sort_order` INT NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    CONSTRAINT `FK_LINK_STORE_KNOWLEDGEBASE_CATEGORY` FOREIGN KEY (`store_id`)
        REFERENCES {$this->getTable('core_store')} (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET=utf8;


-- DROP TABLE IF EXISTS {$this->getTable('ebs_knowledgebase_faq_category')};
    CREATE  TABLE IF NOT EXISTS {$this->getTable('ebs_knowledgebase_faq_category')} (
        `id` int(11) unsigned NOT NULL auto_increment,
        `faq_id` int(11) unsigned NOT NULL ,
        `category_id` int(11) unsigned NOT NULL ,
    PRIMARY KEY (`id`) ,
    CONSTRAINT `FK_LINK_CATEGORY_KNOWLEDGEBASE_FAQ_CATEGORY` FOREIGN KEY (`category_id`)
        REFERENCES {$this->getTable('ebs_knowledgebase_category')} (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB DEFAULT CHARSET=utf8;


-- DROP TABLE IF EXISTS {$this->getTable('ebs_knowledgebase_faq_store')};
    CREATE TABLE IF NOT EXISTS {$this->getTable('ebs_knowledgebase_faq_store')} (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `faq_id` int(11) unsigned NOT NULL,
        `store_id` smallint(5) unsigned NOT NULL,
    PRIMARY KEY (`id`),
    KEY `FK_LINK_STORE_KNOWLEDGEBASE_FAQ_STORE` (`store_id`),
        CONSTRAINT `FK_LINK_STORE_KNOWLEDGEBASE_FAQ_STORE` FOREIGN KEY (`store_id`) REFERENCES {$this->getTable('core_store')} (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

");

$installer->endSetup();

$installer->installEntities();
