<?php
class Ebs_Bannerslider_Block_Adminhtml_Bannerslider_Edit_Tab_Page extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $_model = Mage::registry('bannerslider_data');
		if($_model->getPageId())
		{			
			$_model->setPageId(explode(',',$_model->getPageId()));
		}
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('bannerslider_form', array('legend'=>Mage::helper('bannerslider')->__('Banner Pages')));
        $fieldset->addField('pages', 'multiselect', array(
            'label'     => Mage::helper('bannerslider')->__('Visible In'),            
            'name'      => 'pages[]',
            'values'    => Mage::getSingleton('bannerslider/config_source_page')->toOptionArray(),
            'value'     => $_model->getPageId()
        ));
        
        return parent::_prepareForm();
    }
}
