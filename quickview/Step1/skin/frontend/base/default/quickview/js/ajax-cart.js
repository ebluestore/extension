jQuery(document).ready(function() {
    window._ajaxaddtocart_RemoveConfirmMessage();
    OldSetLocation = window.setLocation;
    window.setLocation = function(url) {
        if (url.indexOf("product_type=bundle") > -1) {
            OldSetLocation(url);
        } else if (url.indexOf("/checkout/cart/add") > -1) {
            if (url.indexOf("product_type=grouped") > -1) {
                AjaxAddToCartGrouped(url);
            } else {
                if (window._ajaxaddtocart_simpleqtyform) {
                    AjaxAddToCartSimple(url);
                } else {
                    AjaxAddToCart(url);
                }
            }

        } else if (url.indexOf('options=cart') > -1) {
            AjaxAddToCartConfigurable(url);
        } else {
            OldSetLocation(url);
        }
    }

    AjaxAddToCart = function(url) {
        /* replace url */
        url = window._ajaxaddtocart_ConvertUrl(url);
        if (!url) return false;
        /* get url response */
        jQuery("#ajaxaddtocart-dialog").dialog({
            width: 400,
            modal: true,
            open: function(event, ui) {
                window._ajaxaddtocart_Loading();
                jQuery.get(url,
                    function(data) {
                        window._ajaxaddtocart_ReloadJsonData(data);
                        window._ajaxaddtocart_AfterLoading();
                        window._ajaxaddtocart_RemoveConfirmMessage();
                    }, "json");
            },
            buttons: window.dialogButtons,
            close: function(event, ui) {
                window._ajaxaddtocart_CloseDialog();
            }
        });
        return true;
    }


    AjaxAddToCartSimple = function(url) {

        url = window._ajaxaddtocart_ConvertUrl(url);
        /* get url response */
        jQuery("#ajaxaddtocart-dialog").dialog({
            width: window._ajaxaddtocart_dialogWidth,
            modal: true,
            open: function(event, ui) {
                _ajaxaddtocart_Loading();

                jQuery.get(url,

                    {
                        isajaxform: "1"
                    },

                    function(data) {
                        //jQuery( "#ajaxaddtocart-dialog .ajaxaddtocart-message").html(data);
                        window._ajaxaddtocart_AfterLoading();

                        jQuery(".ajaxaddtocart-qty-form, #ajaxaddtocart-dialog .product-name").show();
                        jQuery(".ajaxaddtocart-qty-form form#simpleproduct_ajaxaddtocart_form").attr("action", url);
                        jQuery(".ajaxaddtocart-qty-form form#simpleproduct_ajaxaddtocart_form input#qty").val(data.min_qty);
                        jQuery("#ajaxaddtocart-dialog .product-name h1").html(data.product_name);

                    },
                    "json"
                );


            },
            buttons: window.dialogButtonsSimple,
            close: window._ajaxaddtocart_CloseDialog
        });
    }


    AjaxAddToCartConfigurable = function(url) {
        /* replace url */
        if (url.indexOf("options=cart") >= 0) {
            url = url.replace("options=cart", "options=cart&ajaxaddtocart=1");
        }
        /* get url response */
        jQuery("#ajaxaddtocart-dialog").dialog({
            width: window._ajaxaddtocart_dialogWidth,
            modal: true,
            open: function(event, ui) {
                _ajaxaddtocart_Loading();
                jQuery.get(url,
                    function(data) {
                        jQuery("#ajaxaddtocart-dialog .ajaxaddtocart-message").show().html(data);
                        window._ajaxaddtocart_AfterLoading();
                    });
            },
            buttons: window.dialogButtonsConfigurable,
            close: window._ajaxaddtocart_CloseDialog
        });
    }

    AjaxAddToCartGrouped = function(url) {
        url = window._ajaxaddtocart_ConvertUrl(url);
        /* get url response */
        jQuery("#ajaxaddtocart-dialog").dialog({
            width: window._ajaxaddtocart_dialogWidth,
            modal: true,
            open: function(event, ui) {
                _ajaxaddtocart_Loading();
                jQuery.get(url,
                    function(data) {
                        jQuery("#ajaxaddtocart-dialog .ajaxaddtocart-message").show().html(data);
                        window._ajaxaddtocart_AfterLoading();
                    });
            },
            buttons: window.dialogButtonsConfigurable,
            close: window._ajaxaddtocart_CloseDialog
        });
    }

    jQuery(".sidebar .block.block-cart li a.btn-remove").live("click", function() {
        if (!confirm(window._ajaxaddtocart_confirmdelete)) return false;
        x = AjaxAddToCart(jQuery(this).attr("href"));
        return !x;
    });

    jQuery(".ajaxaddtocart-summary .summary a").live("click", function() {
        window.location = jQuery(this).attr("href");
    });

    /* form actions */
    jQuery("#ajaxaddtocart-dialog form#product_addtocart_form").live("submit", function() {
        productAddToCartForm.submit();
        return false;
    });

    jQuery("#ajaxaddtocart-dialog form#simpleproduct_ajaxaddtocart_form").live("submit", function() {
        simpleProductAddToCartForm.submit();
        return false;
    });


    jQuery("#ajaxaddtocart-dialog form#simpleproduct_ajaxaddtocart_form input").live("keydown", function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            simpleProductAddToCartForm.submit();
        }
    });

    jQuery("#ajaxaddtocart-dialog form#product_addtocart_form input").live("keydown", function(e) {
        if (e.keyCode == 13) {
            productAddToCartForm.submit();
            e.preventDefault();
        }
    });
    jQuery("#ajaxaddtocart-dialog a#ajaxaddtocart-qty-plus").live("click", function(e) {
        _qty = jQuery(this).siblings("#qty");
        _qty.val(parseInt(_qty.val()) + 1);
        return false;
    });

    jQuery("#ajaxaddtocart-dialog a#ajaxaddtocart-qty-minus").live("click", function(e) {
        _qty = jQuery(this).siblings("#qty");
        if (parseInt(_qty.val()) > 0) _qty.val(parseInt(_qty.val()) - 1);
        return false;
    });

    jQuery(".ajaxaddtocart-summary .ajaxaddtocart li a.btn-remove").live("click", function() {
        if (!confirm(window._ajaxaddtocart_confirmdelete)) return false;
        url = window._ajaxaddtocart_ConvertUrl(jQuery(this).attr("href"));
        /* replace url */
        if (!url) {
            return true;
        }
        _ajaxaddtocart_Loading();
        jQuery.get(url,
            function(data) {
                window._ajaxaddtocart_ReloadJsonData(data);
                window._ajaxaddtocart_AfterLoading();
                window._ajaxaddtocart_RemoveConfirmMessage();
            }, "json");
        return false;
    });


    if (typeof productAddToCartForm == 'object') {
        productAddToCartForm.submit = function() {
            if (this.validator.validate()) {
                _form = jQuery(".product-view #product_addtocart_form");
                _form_vars = _form.serialize();
                url = _form.attr("action");
                if (!url) this.form.submit();
                if (!AjaxAddToCart(url + "?" + _form_vars)) this.form.submit();
            }
        }
    }

});

function _ajaxaddtocart_CloseDialog() {
    jQuery("#ajaxaddtocart-dialog .ajaxaddtocart-message").html("");
    jQuery("#ajaxaddtocart-dialog .ajaxaddtocart-summary").html("");
    jQuery(".ui-dialog .ui-dialog-buttonpane, .ajaxaddtocart-qty-form, #ajaxaddtocart-dialog .product-name").hide();
    /*for configurable options*/
    truncateOptions();
    return;
}

function _ajaxaddtocart_Loading() {
    jQuery("#ajaxaddtocart-dialog .ajaxaddtocart-message").hide().html("");
    jQuery("#ajaxaddtocart-dialog .ajaxaddtocart-summary").hide().html("");
    jQuery(".ui-dialog .ui-dialog-buttonpane, .ajaxaddtocart-qty-form, #ajaxaddtocart-dialog .product-name").hide();
    jQuery("#ajaxaddtocart-dialog .ajaxaddtocart-loader").show();
    return;
}


function _ajaxaddtocart_AfterLoading() {
    jQuery("#ajaxaddtocart-dialog .ajaxaddtocart-loader").hide();
    jQuery(".ui-dialog .ui-dialog-buttonpane").show();
    jQuery("#ajaxaddtocart-dialog").dialog({
        position: "center"
    });
    return;
}

function _ajaxaddtocart_ReloadJsonData(data) {    
    if (data.message) jQuery("#ajaxaddtocart-dialog .ajaxaddtocart-message").show().html(data.message);
    if (data.cart_sidebar) jQuery(".block.block-cart").replaceWith(data.cart_sidebar);
    if (data.cart_summary) jQuery("#ajaxaddtocart-dialog .ajaxaddtocart-summary").show().html(data.cart_summary);
    if (data.cart_link_text) jQuery(".top-link-cart").text(data.cart_link_text);
    if (data.cart_head) jQuery(".header-minicart .skip-cart .count").text(data.cart_head);
    if (data.minicart) jQuery(".header-minicart #header-cart").html(data.minicart);

    if (data.form_action) jQuery(".col-main #product_addtocart_form").attr("action", data.form_action);

    if (data.redirect) window.location = data.redirect;

    return;
}

function _ajaxaddtocart_ConvertUrl(url) {
    if (url.indexOf("/checkout/cart/updateItemOptions") >= 0 || url.indexOf("/checkout/cart/add") >= 0 || url.indexOf("/checkout/cart/delete") >= 0) {
        url = url.replace("/checkout/", "/ajaxcart/");
        if (url.indexOf("product_type=grouped") > -1) {
            url = url.replace("/add/", "/groupedform/");
        }
        return url;
    } else {
        return false;
    }
}

function _ajaxaddtocart_RemoveConfirmMessage() { /*to remove the default remove from cart message*/
    jQuery(".block.block-cart li a.btn-remove").each(function() {
        jQuery(this)[0].onclick = null;
    });
}