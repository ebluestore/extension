<?php

class Ebs_Totalattributes_Block_Adminhtml_Category_Attribute_Set_Toolbar_Main extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		parent::__construct();
		$this->setTemplate('ebs_totalattributes/category/attribute/set/toolbar/main.phtml');
	}

	protected function _prepareLayout()
	{
		$this->setChild('addButton',
			$this->getLayout()->createBlock('adminhtml/widget_button')
				->setData(array(
					'label'     => Mage::helper('ebs_totalattributes')->__('Add New Group'),
					'onclick'   => 'setLocation(\'' . $this->getUrl('*/*/add') . '\')',
					'class' => 'add',
				))
		);
		return parent::_prepareLayout();
	}

	protected function getNewButtonHtml()
	{
		return $this->getChildHtml('addButton');
	}

	protected function _getHeader()
	{
		return Mage::helper('ebs_totalattributes')->__('Manage Attribute Group');
	}

	protected function _toHtml()
	{
		Mage::dispatchEvent('adminhtml_catalog_category_attribute_set_toolbar_main_html_before', array('block' => $this));
		return parent::_toHtml();
	}
}
