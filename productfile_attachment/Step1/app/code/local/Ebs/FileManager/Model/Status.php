<?php

class Ebs_FileManager_Model_Status extends Varien_Object
{
    const STATUS_ENABLED    = 1;
    const STATUS_DISABLED   = 2;

    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('ebs_filemanager')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('ebs_filemanager')->__('Disabled')
        );
    }
} 

?>