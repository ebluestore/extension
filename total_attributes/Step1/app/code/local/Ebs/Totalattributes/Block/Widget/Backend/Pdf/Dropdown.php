<?php

class Ebs_Totalattributes_Block_Widget_Backend_Pdf_Dropdown
    extends Ebs_Totalattributes_Block_Widget_Backend_PdfAbstract
{
    public function getLabel()
    {
        return $this->_getLabel();
    }

    public function getValue()
    {
        $label = '';
        foreach($this->_getOptionsForSelect() as $value => $valueLabel) {
            if ($this->_getValue() == $value) {
                $label = $valueLabel;
                continue;
            }
        }
        $value = htmlspecialchars($label);
        $result = $this->iconvWordwrap($value, 50, "@newline@", true);
        return explode("@newline@", $result);
    }

    private function _getOptionsForSelect()
    {
        if (is_null($this->getTypeModel())) {
            return null;
        }
        $attribute = $this->getTypeModel()->getAttribute();
        if (is_null($attribute)) {
            return null;
        }
        $storeId = Mage::app()->getStore()->getId();
        $options = Mage::helper('ebs_totalattributes/options')->getOptionsForAttributeByStoreId($attribute, $storeId, false);
        foreach($options as $key => $value) {
            $options[$key] = htmlspecialchars($value);
        }
        return $options;
    }
}