<?php

class Ebs_FileManager_Block_Adminhtml_Productfile extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_productfile';
    $this->_blockGroup = 'ebs_filemanager';
    $this->_headerText = Mage::helper('ebs_filemanager')->__('File Manager');
    $this->_addButtonLabel = Mage::helper('ebs_filemanager')->__('Add File');
    parent::__construct();
  }
  
} 

?>