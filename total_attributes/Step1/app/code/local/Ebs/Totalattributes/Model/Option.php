<?php

class Ebs_Totalattributes_Model_Option extends Mage_Core_Model_Abstract
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('ebs_totalattributes/option');
    }

    /**
     * @return ebs_totalattributes_Model_Resource_Option_Value_Collection
     */
    public function getValueCollection()
    {
        return Mage::getResourceModel('ebs_totalattributes/option_value_collection')->addOptionFilter($this);
    }
}