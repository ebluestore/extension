<?php

class Ebs_Totalattributes_Model_Resource_Value_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected $_tableAliases = array();

    /**
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ebs_totalattributes/value');
    }

    /**
     * Adding item to item array
     * FIX duplicate id in collection
     *
     * @param   Varien_Object $item
     * @return  Ebs_Totalattributes_Model_Resource_Value_Collection
     */
    public function addItem(Varien_Object $item)
    {
        $this->_items[] = $item;
        return $this;
    }

    /**
     * @param Varien_Db_Select $select
     *
     * @return string|Varien_Db_Select
     */
    protected function _prepareSelect(Varien_Db_Select $select)
    {
        //Hack for fix magento bug
        parent::_prepareSelect($select);
        return $select;
    }

    /**
     * @param Zend_Db_Select $select
     *
     * @return array|void
     */
    protected function _fetchAll($select)
    {
        $uSelects = $this->getResource()->getSelectsFromMainSelect($select);
        $gSelect = $this->_conn->select();
        $gSelect->union($uSelects);
        return parent::_fetchAll($gSelect);
    }

    /**
     * @param $quoteId
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function addQuoteFilter($quoteId)
    {
        $this->addFieldToFilter('quote_id',  array('eq' => $quoteId));
        return $this;
    }

    /**
     * @param $attributeId
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function addAttributeFilter($attributeId)
    {
        $this->addFieldToFilter('attribute_id',  array('eq' => $attributeId));
        return $this;
    }
}