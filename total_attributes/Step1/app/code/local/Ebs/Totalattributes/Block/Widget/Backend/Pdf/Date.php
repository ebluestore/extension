<?php

class Ebs_Totalattributes_Block_Widget_Backend_Pdf_Date
    extends Ebs_Totalattributes_Block_Widget_Backend_PdfAbstract
{
    public function getLabel()
    {
        return $this->_getLabel();
    }

    public function getValue()
    {
        $dateHtml = $this->_getValue();;
        if (!is_null($dateHtml)) {
            $format = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_FULL);
            $dateHtml = $dateHtml->toString($format);
        } else {
            return array('');
        }
        return array($dateHtml);
    }

    protected function _getValue()
    {
        if (is_null($this->_value)) {
            $value = $this->getProperty('default_value');
            if (!is_null($value) && strlen(trim($value)) > 0) {
                try {
                    return Mage::app()->getLocale()->date($value, Zend_Date::ISO_8601);
                } catch(Exception $e) {
                    return $value;
                }
            } else {
                return null;
            }
        }
        return Mage::app()->getLocale()->date($this->_value, Zend_Date::ISO_8601);
    }
}