<?php

class Ebs_Dailydeal_Model_Layout extends Mage_Core_Model_Abstract
{

    /**
     * @return Ebs_Dailydeal_Model_Layout
     */
    public static function getInstance()
    {
        return Mage::getSingleton('dailydeal/layout');
    }

    /**
     * @return Ebs_Dailydeal_Model_Layout
     */
    public static function getModel()
    {
        return Mage::getModel('dailydeal/layout');
    }

    public static function overrideProductView($observer)
    {
        if( !Mage::helper('dailydeal')->getConfigProductViewCountdown() || !Mage::helper('dailydeal/dailydeal')->isModuleOutputEnabled() ){
            return;
        }
        
        $action = $observer->getEvent()->getAction();
        $layout = $observer->getEvent()->getLayout();
        $full_action = Mage::app()->getFrontController()->getAction()->getFullActionName();
        if ($full_action == 'catalog_product_view') {
            $layout->getUpdate()->addUpdate('
                <reference name="product.info">
                    <action method="setTemplate"><template>dailydeal/catalog/product/view.phtml</template></action>
                </reference>
            ');
            $layout->generateXml();
        }
    }

    public static function overrideProductList($observer)
    {
        if( !Mage::helper('dailydeal')->getConfigProductListCountdown() || !Mage::helper('dailydeal/dailydeal')->isModuleOutputEnabled() ){
            return;
        }
        
        $action = $observer->getEvent()->getAction();
        $layout = $observer->getEvent()->getLayout();
        $full_action = Mage::app()->getFrontController()->getAction()->getFullActionName();        
        if ($full_action == 'catalog_category_view') {
            $layout->getUpdate()->addUpdate('
                <reference name="product_list">
                    <action method="setTemplate"><template>dailydeal/catalog/product/list.phtml</template></action>
                </reference>
            ');
            $layout->generateXml();
        }
    }

}