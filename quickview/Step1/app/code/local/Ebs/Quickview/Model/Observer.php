<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2009 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog Observer
 *
 * @category   Ebluestore
 * @package    Ebs_Quickview 
 */
class Ebs_Quickview_Model_Observer {

    public function showConfigurableOptions(Varien_Event_Observer $observer) {
        $_vars = $observer->getControllerAction()->getRequest()->getParams();
        if (isset($_vars['options']) && $_vars['options'] == 'cart' && isset($_vars['quickview'])) {
            $_product = $observer->getEvent()->getProduct();
            if (!$_product->isConfigurable() && $_product->getTypeId() != 'simple' && $_product->getTypeId() != 'downloadable')
                return false;
            $_block = Mage::app()->getLayout();
            $_js_block = $_block->createBlock('core/template', 'product_js')->setTemplate('catalog/product/view/options/js.phtml');
            $_options_block = $_block->createBlock('catalog/product_view_options', 'product_options')
                            ->setTemplate('catalog/product/view/options.phtml')
                            ->addOptionRenderer('file', 'catalog/product_view_options_type_file', 'catalog/product/view/options/type/file.phtml')
                            ->addOptionRenderer('date', 'catalog/product_view_options_type_date', 'catalog/product/view/options/type/date.phtml')
                            ->addOptionRenderer('text', 'catalog/product_view_options_type_text', 'catalog/product/view/options/type/text.phtml')
                            ->addOptionRenderer('select', 'catalog/product_view_options_type_select', 'catalog/product/view/options/type/select.phtml');

            $_price_block = $_block->createBlock('catalog/product_view', 'product_price')->setTemplate('catalog/product/view/price_clone.phtml');

            $_qty_block = $_block->createBlock('catalog/product_view', 'product_qty')->setTemplate('quickview/qty.phtml');

            $_html = $_block->createBlock('catalog/product_view')->setProduct($_product)->setTemplate('quickview/options_form.phtml')
                            ->append($_options_block)->append($_js_block)->append($_price_block);

            if ($_product->isConfigurable()) {
                $_html  ->append(
                            $_block->createBlock('catalog/product_view_type_configurable', 'product_configurable_options')
                                ->setTemplate('quickview/options/configurable.phtml')
                        )
                        ->append(
                            $_block->createBlock('catalog/product_view_type_configurable', 'product_type_data')
                                ->setTemplate('catalog/product/view/type/configurable.phtml')
                        )
                        ->append($_qty_block);

            }
            if ($_product->getTypeId() == 'downloadable') {
                $_html  ->append(
                            $_block->createBlock('downloadable/catalog_product_links', 'product_downloadable_options')
                                ->setTemplate('quickview/options/downloadable.phtml')
                        )
                        ->append(
                            $_block->createBlock('downloadable/catalog_product_view_type', 'product_type_data')
                                ->setTemplate('downloadable/catalog/product/type.phtml')
                        );
            }
            echo $_html->toHtml();
            exit;
        }
    }

}