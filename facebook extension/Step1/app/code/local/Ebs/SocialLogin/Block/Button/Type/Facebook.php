<?php
/**
 * Ebluestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade InstagramConnect to newer
 * versions in the future.
 *
 * @category    Ebluestore
 * @package     Ebs_SocialLogin
 * @copyright   Copyright (c) 2014 Ebluestore LLC
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ebs_SocialLogin_Block_Button_Type_Facebook extends Ebs_SocialLogin_Block_Button_Type{

    protected $_class = 'ico-fb';
    protected $_title = 'Facebook';
    protected $_name = 'facebook';
    protected $_width = 500;
    protected $_height = 270;
    protected $_disconnect = 'ebs_sociallogin/facebook/disconnect';

    public function __construct($name = null, $class = null,$title=null){
        parent::__construct();

        $this->client = Mage::getSingleton('ebs_sociallogin/facebook_client');

        if(!($this->client->isEnabled())) {
            return;
        }

        $this->userInfo = Mage::registry('ebs_sociallogin_facebook_userinfo');

        // CSRF protection
        //Mage::getSingleton('core/session')->setFacebookCsrf($csrf = md5(uniqid(rand(), TRUE)));
        //$this->client->setState($csrf);

        if(!($redirect = Mage::getSingleton('customer/session')->getBeforeAuthUrl())) {
            $redirect = Mage::helper('core/url')->getCurrentUrl();
        }

        // Redirect uri
        Mage::getSingleton('core/session')->setFacebookRedirect($redirect);

    }

}