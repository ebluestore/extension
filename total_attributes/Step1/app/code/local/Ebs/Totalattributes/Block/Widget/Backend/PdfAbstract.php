<?php


abstract class Ebs_Totalattributes_Block_Widget_Backend_PdfAbstract
    implements Ebs_Totalattributes_Block_Widget_Backend_PdfInterface
{
    /**
     * @var Ebs_Totalattributes_Model_Attribute_TypeInterface
     */
    protected $_type = null;

    protected $_value = null;

    /**
     * @param Ebs_Totalattributes_Model_Attribute_TypeInterface $type
     *
     * @return Ebs_Totalattributes_Block_Widget_Backend_PdfAbstract
     */
    public function setTypeModel(Ebs_Totalattributes_Model_Attribute_TypeInterface $type)
    {
        $this->_type = $type;
        return $this;
    }

    /**
     * @return Ebs_Totalattributes_Model_Attribute_TypeInterface
     */
    public function getTypeModel()
    {
        return $this->_type;
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function getProperty($key)
    {
        if (is_null($this->getTypeModel())) {
            return null;
        }
        if (is_null($this->getTypeModel()->getAttribute())) {
            return null;
        }
        return $this->getTypeModel()->getAttribute()->getData($key);
    }

    /**
     * @param mixed $value
     *
     * @return Ebs_Totalattributes_Block_Widget_Frontend_FormAbstract
     */
    public function setValue($value)
    {
        $this->_value = $value;
        return $this;
    }

    /**
     * getter
     *
     * @return string
     */
    protected function _getLabel()
    {
        $storeId = Mage::app()->getStore()->getId();
        $label = $this->getTypeModel()->getAttribute()->getLabel($storeId);
        return Mage::helper('core')->escapeHtml($label);
    }

    /**
     * @return string
     */
    protected function _getCode()
    {
        return Ebs_Totalattributes_Model_Attribute_TypeAbstract::FRONTEND_ATTRIBUTE_CODE_PREFIX .
        $this->getProperty('code');
    }

    /**
     * getter
     *
     * @return mixed
     */
    protected function _getValue()
    {
        if (is_null($this->_value)) {
            return $this->getProperty('default_value');
        }
        return $this->_value;
    }

    public function iconvWordwrap($string, $width = 75, $break = "\n", $cut = false, $charset = 'utf-8') {


        /* If mbstring lib is not available return not truncated string */
        if (!in_array('mbstring', get_loaded_extensions())) {
            return $string;
        }

        /* Get endoding fallback to utf-8 */
        if (!$encoding = mb_detect_encoding($string)) {
            $charset = 'UTF-8';
        }

        $stringWidth = mb_strlen($string, $charset);
        $breakWidth = mb_strlen($break, $charset);

        if (strlen($string) === 0) {
            return '';
        } elseif ($breakWidth === null) {
            throw new Zend_Text_Exception('Break string cannot be empty');
        } elseif ($width === 0 && $cut) {
            throw new Zend_Text_Exception('Can\'t force cut when width is zero');
        }

        $result = '';
        $lastStart = $lastSpace = 0;

        for ($current = 0; $current < $stringWidth; $current++) {
            $char = mb_substr($string, $current, 1, $charset);

            if ($breakWidth === 1) {
                $possibleBreak = $char;
            } else {
                $possibleBreak = mb_substr($string, $current, $breakWidth, $charset);
            }

            if ($possibleBreak === $break) {
                $result .= mb_substr($string, $lastStart, $current - $lastStart + $breakWidth, $charset);
                $current += $breakWidth - 1;
                $lastStart = $lastSpace = $current + 1;
            } elseif ($char === ' ') {
                if ($current - $lastStart >= $width) {
                    $result .= mb_substr($string, $lastStart, $current - $lastStart, $charset) . $break;
                    $lastStart = $current + 1;
                }

                $lastSpace = $current;
            } elseif ($current - $lastStart >= $width && $cut && $lastStart >= $lastSpace) {
                $result .= mb_substr($string, $lastStart, $current - $lastStart, $charset) . $break;
                $lastStart = $lastSpace = $current;
            } elseif ($current - $lastStart >= $width && $lastStart < $lastSpace) {
                $result .= mb_substr($string, $lastStart, $lastSpace - $lastStart, $charset) . $break;
                $lastStart = $lastSpace = $lastSpace + 1;
            }
        }

        if ($lastStart !== $current) {
            $result .= mb_substr($string, $lastStart, $current - $lastStart, $charset);
        }

        return $result;
    }
}