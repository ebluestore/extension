<?php
class Ebs_LayeredNavigation_Model_Search_Layer extends Mage_CatalogSearch_Model_Layer
{
	public function getFilterableAttributes() {
		$collection = Mage::getResourceModel('catalog/product_attribute_collection');
		$collection->setItemObjectClass('catalog/resource_eav_attribute')->addStoreLabel(Mage::app()->getStore()->getId())->setOrder('position', 'ASC');
		$collection = $this->_prepareAttributeCollection($collection);
		$collection->load();
		
		return $collection;
	}
}
