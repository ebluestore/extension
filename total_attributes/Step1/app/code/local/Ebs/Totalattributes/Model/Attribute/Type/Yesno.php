<?php

class Ebs_Totalattributes_Model_Attribute_Type_Yesno extends Ebs_Totalattributes_Model_Attribute_TypeAbstract
{
    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Grid_Yesno
     */
    protected function _getBackendGridRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Grid_Yesno();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Form_Yesno
     */
    protected function _getBackendFormRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Form_Yesno();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_View_Yesno
     */
    protected function _getBackendViewRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_View_Yesno();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Pdf_Yesno
     */
    protected function _getBackendPdfRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Pdf_Yesno();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Frontend_Form_Yesno
     */
    protected function _getFrontendFormRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Frontend_Form_Yesno();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Frontend_View_Yesno
     */
    protected function _getFrontendViewRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Frontend_View_Yesno();
    }

    public function getValueType()
    {
        return Ebs_Totalattributes_Model_Resource_Value::INT_TYPE;
    }

    public function getAvailableOptionArray()
    {
        $helper = Mage::helper('ebs_totalattributes');
        return array(
            array('value' => 0, 'label' => $helper->__('---Please Select---')),
            array('value' => 1, 'label' => $helper->__('Yes')),
            array('value' => 2, 'label' => $helper->__('No'))
        );
    }

    /**
     * @param mixed $value
     */
    public function validate($value)
    {
        $helper = Mage::helper('ebs_totalattributes');
        $storeId = Mage::app()->getStore()->getId();
        $label = $this->getAttribute()->getLabel($storeId);
        $label = Mage::helper('core')->escapeHtml($label);
        if (strlen(trim($value)) < 1 || $value == 0) {
            if ($this->getAttribute()->getData('validation_rules/is_required')) {
                throw new Exception($helper->__('%s is required', $label));
            }
        }
        $options = $this->getAvailableOptionArray();
        $isFounded = false;
        foreach($options as $item) {
            if ($item['value'] == $value) {
                $isFounded = true;
                break;
            }
        }
        if (!$isFounded) {
            throw new Exception($helper->__('%s has incorrect selected option', $label));
        }
        return;
    }
}