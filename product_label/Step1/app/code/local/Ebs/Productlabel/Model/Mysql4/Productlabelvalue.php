<?php
class Ebs_Productlabel_Model_Mysql4_Productlabelvalue extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('productlabel/productlabelvalue', 'value_id');
    }
    public function loadAttributeValue($labelId, $storeId, $attributeCode)
    {
        $attributeValue = $this->getCollection()->addFieldToFilter('label_id', $labelId)->addFieldToFilter('store_id', $storeId)->addFieldToFilter('attribute_code', $attributeCode)->getFirstItem();
        $this->setData('label_id', $labelId)->setData('store_id', $storeId)->setData('attribute_code', $attributeCode);
        if ($attributeValue)
            $this->addData($attributeValue->getData())->setId($attributeValue->getId());
        return $this;
    }
}
?>