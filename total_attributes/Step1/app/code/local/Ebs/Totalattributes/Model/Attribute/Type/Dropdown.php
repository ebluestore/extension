<?php

class Ebs_Totalattributes_Model_Attribute_Type_Dropdown extends Ebs_Totalattributes_Model_Attribute_TypeAbstract
{
    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Grid_Dropdown
     */
    protected function _getBackendGridRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Grid_Dropdown();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Form_Dropdown
     */
    protected function _getBackendFormRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Form_Dropdown();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_View_Dropdown
     */
    protected function _getBackendViewRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_View_Dropdown();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Pdf_Dropdown
     */
    protected function _getBackendPdfRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Pdf_Dropdown();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Frontend_Form_Dropdown
     */
    protected function _getFrontendFormRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Frontend_Form_Dropdown();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Frontend_View_Dropdown
     */
    protected function _getFrontendViewRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Frontend_View_Dropdown();
    }

    public function getValueType()
    {
        return Ebs_Totalattributes_Model_Resource_Value::INT_TYPE;
    }

    /**
     * @param mixed $value
     */
    public function validate($value)
    {
        $helper = Mage::helper('ebs_totalattributes');
        $storeId = Mage::app()->getStore()->getId();
        $label = $this->getAttribute()->getLabel($storeId);
        $label = Mage::helper('core')->escapeHtml($label);
        if (strlen(trim($value)) < 1 || $value == 0) {
            if ($this->getAttribute()->getData('validation_rules/is_required')) {
                throw new Exception($helper->__('%s is required', $label));
            }
        } else {
            $options = $this->getAttribute()->getStoreOptions();
            if (!array_key_exists($value, $options)) {
                throw new Exception($helper->__('%s has incorrect selected option', $label));
            }
        }
        return;
    }
}