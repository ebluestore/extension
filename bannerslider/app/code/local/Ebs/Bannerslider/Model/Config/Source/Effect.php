<?php
class Ebs_Bannerslider_Model_Config_Source_Effect
{
    const SKITTER_SLIDER    = 'skitter_slider';
    const OWL_CAROUSEL      = 'owl_carousel';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => self::SKITTER_SLIDER, 'label'=>Mage::helper('adminhtml')->__('Skitter Slider')), 
            array('value' => self::OWL_CAROUSEL, 'label'=>Mage::helper('adminhtml')->__('Owl Carousel')),           
        );
    }
}
