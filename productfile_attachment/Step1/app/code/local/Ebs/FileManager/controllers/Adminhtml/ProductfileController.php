<?php

class Ebs_FileManager_Adminhtml_ProductfileController extends Mage_Adminhtml_Controller_action
{

    protected function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('cms/filemanager')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
        
        return $this;
    }   
 
    public function indexAction() {        
        $this->_initAction()->renderLayout();
    }
    
    public function productAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('productfile.edit.tab.product')
             ->setProducts($this->getRequest()->getPost('fproducts',null));
        $this->renderLayout();
    
    }
    
    public function productGridAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('productfile.edit.tab.product')
             ->setProducts($this->getRequest()->getPost('ebs_filemanager',null));
        $this->renderLayout();
    
    }
    
    public function productfileAction()
    {        
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.grid')
            ->setProductfiles($this->getRequest()->getPost('ebs_filemanager', null));
        $this->renderLayout();
    
    }
    
    public function productfileGridAction()
    {        
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.grid')
             ->setProductfiles($this->getRequest()->getPost('ebs_filemanager', null));
        $this->renderLayout();
    
    }
    
    //tab in product
    public function fileAction() {
        $this->loadLayout();
        $this->_setActiveMenu('cms/filemanager');

        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
        $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addContent($this->getLayout()->createBlock('ebs_filemanager/adminhtml_file')->setTemplate('ebs/productfile/catalog/product/uploader.phtml'));

        $this->renderLayout();
    }
        
    public function editAction() {        
        $id     = $this->getRequest()->getParam('id');
        $store  = $this->getRequest()->getParam('store');
        $model  = Mage::getModel('ebs_filemanager/productfile')->setStoreId($store)->load($id);
                
        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('productfile_data', $model);

            $this->loadLayout();
            $this->_setActiveMenu('cms/filemanager');

            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('adminhtml')->__('New Items'), Mage::helper('adminhtml')->__('New Items'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('ebs_filemanager/adminhtml_productfile_edit'))
                 ->_addLeft($this->getLayout()->createBlock('ebs_filemanager/adminhtml_productfile_edit_tabs'));

            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ebs_filemanager')->__('File does not exist'));
            $this->_redirect('*/*/');
        }
    }
 
    public function newAction() {
        $this->editAction();
    }
 
    public function saveAction() {        
        $store = $this->getRequest()->getParam('store');        
        if ($data = $this->getRequest()->getPost()) {    
            //Zend_debug::dump($data);die();
            if (isset($data['file_status']) && $data['file_status'] != NULL){
                $data['status'] = $data['file_status'];
            }                    
            if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
                try {    
                    /* Starting upload */    
                    $uploader = new Varien_File_Uploader('filename');
                    
                    // Any extention would work
                    //$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png','doc','docx','rar','zip','mp3','txt','pdf','wmv','flv','mp4','avi','xlsx','xls'));
                    $uploader->setAllowRenameFiles(false);
                    
                    // Set the file upload mode 
                    // false -> get the file directly in the specified folder
                    // true -> get the file in the product like folders 
                    //    (file.jpg will go in something like /media/f/i/file.jpg)
                    //$uploader->setFilesDispersion(false);
                    $uploader->setFilesDispersion(true);                    
                    $path = Mage::getBaseDir('media') . DS .'productfile' ;
                    $uploader->save($path, $_FILES['filename']['name'] );    
                    $result = $uploader->save($path, $_FILES['filename']['name'] );
                } catch (Exception $e) {
              
                }
                               
                $data['filename'] = $_FILES['filename']['name'];
                $data['filesize'] = $_FILES['filename']['size'];
                $data['fileextension'] = $_FILES['filename']['type'];
            }
            
            //get productId    
            if(isset($data['productfile_products']))
            {
                $productIds = array();
                parse_str($data['productfile_products'],$productIds);
                $productIds = array_keys($productIds);
            } else {
                $productIds = array(0);
            }
            //pid for tab in manage products    
            $pid     = $this->getRequest()->getParam('pid');
            if($pid ) {
                $productIds = array($pid);
            }                    
            
            $model = Mage::getModel('ebs_filemanager/productfile');                    
            $model->setData($data)
                    ->setStoreId($store)                
                    ->setId($this->getRequest()->getParam('id'));                        
            try {
                
                $model->save();    
                    
                Mage::helper('ebs_filemanager')->assignProducts($model,$productIds);
                                
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('ebs_filemanager')->__('File was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(),'store'=>$store));
                    return;
                }
                
                //uploader
                if($pid){
                    $this->_redirect('*/*/file',array('pid'=>$pid,'id'=>$model->getId(), 'store'=>$store));
                    return;
                }
                
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
            
            
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ebs_filemanager')->__('Unable to find file to save'));
        $this->_redirect('*/*/');
    }
 
    public function deleteAction() {
        if( $this->getRequest()->getParam('id') > 0 ) {
            try {
                $model = Mage::getModel('ebs_filemanager/productfile');
                 
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                     
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('File was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }
    
    public function deleteAttachedFileAction() {
        if( $this->getRequest()->getParam('id') > 0 && $this->getRequest()->getParam('pid') > 0) {
            try {
                $model = Mage::getModel('ebs_filemanager/productfile');
                 
                $model->setId($this->getRequest()->getParam('id'))
                    ->delete();
                     
                $this->loadLayout();
                $this->_addContent($this->getLayout()->createBlock('core/template')
                     ->setTemplate('ebs/productfile/catalog/product/deleteAttachedFile.phtml'));
                $this->renderLayout();
                

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/file', array('id' => $this->getRequest()->getParam('id')));
            }
        }
    
    }
    

    public function massDeleteAction() {
        $productfileIds = $this->getRequest()->getParam('ebs_filemanager');
        if(!is_array($productfileIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select file(s)'));
        } else {
            try {
                foreach ($productfileIds as $productfileId) {
                    $productfile = Mage::getModel('ebs_filemanager/productfile')->load($productfileId);
                    $productfile->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d file(s) were successfully deleted', count($productfileIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
    
    public function massStatusAction()
    {
        $storeId = $this->getRequest()->getParam('store');        
        $productfileIds = $this->getRequest()->getParam('ebs_filemanager');
        if(!is_array($productfileIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select file(s)'));
        } else {
            if (!$storeId || $storeId == 0){
                try {
                    foreach ($productfileIds as $productfileId) {
                        $productfile = Mage::getSingleton('ebs_filemanager/productfile')
                            ->load($productfileId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Total of %d file(s) were successfully updated', count($productfileIds))
                    );
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }            
              $this->_redirect('*/*/index/');
            } else {
                foreach ($productfileIds as $productfileId) {
                    $valueCollection = Mage::getModel('ebs_filemanager/value')->getCollection()
                                ->addFieldToFilter('productfile_id', $productfileId)
                                ->addFieldToFilter('store_id', $storeId)
                                ->addFieldToFilter('attribute_code', 'status');
                    $item = $valueCollection->getFirstItem();
                    $item->setProductfileId($productfileId);
                    $item->setStoreId($storeId);
                    $item->setAttributeCode('status');
                    $item->setValue($this->getRequest()->getParam('status'));
                    $item->save();                    
                }
                $this->_redirect('*/*/index/', array('store' => $storeId));
            }            
        }        
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'productfile.csv';
        $content    = $this->getLayout()->createBlock('ebs_filemanager/adminhtml_productfile_grid')->getCsv();
        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'productfile.xml';
        $content    = $this->getLayout()->createBlock('ebs_filemanager/adminhtml_productfile_grid')->getXml();
        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
} 

?>