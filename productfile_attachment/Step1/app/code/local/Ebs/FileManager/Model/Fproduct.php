<?php

class Ebs_FileManager_Model_Fproduct extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('ebs_filemanager/fproduct');
    }
    
    public function loadByFilePrd($productfile_id,$product_id)
    {
        $collection = $this->getCollection()
                        ->addFieldToFilter('productfile_id',$productfile_id)
                        ->addFieldToFilter('product_id',$product_id);
        $item = $collection->getFirstItem();
        $this->setData($item->getData());
        return $this;
    }    
  
} 

?>