<?php

interface Ebs_Totalattributes_Model_Attribute_TypeInterface
{
    /**
     * @return Ebs_Orderattribute_Block_Widget_Backend_GridInterface
     */
    public function getBackendGridRenderer();

    /**
     * @return Ebs_Orderattribute_Block_Widget_Backend_FormInterface
     */
    public function getBackendFormRenderer();

    /**
     * @return Ebs_Orderattribute_Block_Widget_Backend_ViewInterface
     */
    public function getBackendViewRenderer();

    /**
     * @return Ebs_Orderattribute_Block_Widget_Backend_PdfInterface
     */
    public function getBackendPdfRenderer();

    /**
     * @return Ebs_Orderattribute_Block_Widget_Frontend_FormInterface
     */
    public function getFrontendFormRenderer();

    /**
     * @return Ebs_Orderattribute_Block_Widget_Frontend_ViewInterface
     */
    public function getFrontendViewRenderer();

    /**
     * @param Ebs_Totalattributes_Model_Value $valueModel
     *
     * @return Ebs_Totalattributes_Model_Value
     */
    public function beforeSave($valueModel);

    /**
     * @return Ebs_Totalattributes_Model_Attribute_TypeInterface
     */
    public function afterAttributeDelete();

    /**
     * @param mixed $value
     * @param array $rules
     *
     * @throws Exception
     */
    public function validate($value);

    /**
     * @param Ebs_Totalattributes_Model_Attribute $attribute
     *
     * @return Ebs_Totalattributes_Model_Attribute_TypeAbstract
     */
    public function setAttribute(Ebs_Totalattributes_Model_Attribute $attribute);

    /**
     * @param Ebs_Totalattributes_Model_Attribute $attribute
     *
     * @return Ebs_Totalattributes_Model_Attribute_TypeAbstract
     */
    public function getAttribute();

    /**
     * @return int|varchar|text|date
     */
    public function getValueType();
}