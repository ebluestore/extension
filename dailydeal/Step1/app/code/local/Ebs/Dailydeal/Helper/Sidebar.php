<?php

class Ebs_Dailydeal_Helper_Sidebar extends Mage_Core_Helper_Abstract
{

    public function displayTodaydealLeft()
    {
        if (!Mage::helper('dailydeal/dailydeal')->isModuleOutputEnabled()) {
            return;
        }
        if (Mage::getStoreConfig('dailydeal/general/sidebardeal') == 1)
            return "dailydeal/sidebar/todaydeal.phtml";
    }

    public function displayActivedealLeft()
    {
        if (!Mage::helper('dailydeal/dailydeal')->isModuleOutputEnabled()) {
            return;
        }
        if (Mage::getStoreConfig('dailydeal/general/sidebaractive') == 1)
            return "dailydeal/sidebar/activedeal.phtml";
    }

    public function displayTodaydealRight()
    {
        if (!Mage::helper('dailydeal/dailydeal')->isModuleOutputEnabled()) {
            return;
        }
        if (Mage::getStoreConfig('dailydeal/general/sidebardeal', Mage::app()->getStore()->getStoreId()) == 2)
            return "dailydeal/sidebar/todaydeal.phtml";
    }

    public function displayActivedealRight()
    {
        if (!Mage::helper('dailydeal/dailydeal')->isModuleOutputEnabled()) {
            return;
        }
        if (Mage::getStoreConfig('dailydeal/general/sidebaractive', Mage::app()->getStore()->getStoreId()) == 2)
            return "dailydeal/sidebar/activedeal.phtml";
    }
}
