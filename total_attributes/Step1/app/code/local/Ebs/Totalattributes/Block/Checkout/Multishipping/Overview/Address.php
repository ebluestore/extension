<?php

class Ebs_Totalattributes_Block_Checkout_Multishipping_Overview_Address
{
    protected $_realAddress = null;

    public function __construct($address)
    {
        $this->_realAddress = $address;
    }

    public function __call($method, $args)
    {
        return call_user_func_array(
            array($this->_realAddress, $method),
            $args
        );
    }

    public function format($type)
    {
        $html = $this->_realAddress->format($type);
        $block = Mage::app()->getLayout()
            ->createBlock('ebs_totalattributes/checkout_multishipping_attributes_billingaddress')
            ->setTemplate('ebs_totalattributes/checkout/multishipping/overview/billing_address.phtml')
        ;
        return $html . $block->toHtml();
    }
}