<?php

class Ebs_Totalattributes_Model_Sales_Order_Address extends Varien_Object
{
    protected $_realAddress = null;

    public function setRealAddress($address)
    {
        $this->_realAddress = $address;
        return $this;
    }

    public function format($type)
    {
        $attributesBlock = Mage::app()->getLayout()->getBlock("oa.{$this->_realAddress->getData('address_type')}.address.attributes");
        $addressHtml = $this->_realAddress->format($type);
        if ($attributesBlock) {
            $addressHtml .= $attributesBlock->toHtml();
        }
        return $addressHtml;
    }

    public function __call($method, $args)
    {
        return call_user_func_array(
            array($this->_realAddress, $method),
            $args
        );
    }
}