<?php

class Ebs_FileManager_Model_Value extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('ebs_filemanager/value');
    }
    
    public function loadAttributeValue($productfile_id,$store_id, $attributeCode)
    {
        $attributeValue = $this->getCollection()
                        ->addFieldToFilter('productfile_id',$productfile_id)
                        ->addFieldToFilter('store_id', $store_id)
                        ->addFieldToFilter('attribute_code',$attributeCode)
                        ->getFirstItem();                        
        
        $this->setData('productfile_id', $productfile_id)
            ->setData('store_id',$store_id)
            ->setData('attribute_code',$attributeCode);
        if ($attributeValue)
            $this->addData($attributeValue->getData())
                ->setId($attributeValue->getId());
        return $this;    
    }    
  
} 

?>