<?php

class Ebs_Productlabel_Block_Productdetail extends Mage_Catalog_Block_Product_View {

    public function getCurrentProduct() {
        return Mage::registry('current_product');
    }

    public function getAllProductLabel($productId, $storeId) {
        $product_label_flat_data = Mage::getModel('core/resource')->getTableName('product_label_flat_data');
        $select = '(SELECT position,product_id,max(priority) FROM '.$product_label_flat_data.' group by store_id,position, product_id )';
        $colection = Mage::getModel('productlabel/productlabelflatdata')->getCollection();
        $colection->addFieldToFilter('product_id', $productId)->addFieldToFilter('display', 1)
                ->addFieldToFilter('store_id', $storeId)->getSelect()
                ->where('(position,product_id,priority) IN ' . $select)->group(array('position', 'priority'));
        return $colection;
    }

    public function getProductLabel($productId, $storeId) {
        $collection = Mage::getModel('productlabel/productlabelentity')->getCollection()->setStoreId($storeId);
        $label = $collection->addFieldToFilter('product_id', $productId)->getFirstItem();
        return $label;
    }

} 

?>