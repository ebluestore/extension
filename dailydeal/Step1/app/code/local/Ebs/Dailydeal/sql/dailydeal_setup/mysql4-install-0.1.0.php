<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('dailydeal/dailydeal')};
CREATE TABLE {$this->getTable('dailydeal/dailydeal')} (
    `dailydeal_id` int(11) unsigned NOT NULL auto_increment,
    `product_id`	int(11) DEFAULT NULL,
    `cur_product` varchar(1024) DEFAULT NULL,
    `product_sku` varchar(255) DEFAULT NULL,
    `product_price` float NULL,
    `discount` float(11) DEFAULT '0',
    `discount_type` int(11) DEFAULT NULL,
    `start_date_time` datetime DEFAULT NULL,
    `end_date_time` datetime DEFAULT NULL,
    `dailydeal_price` float(11) DEFAULT '0',
    `deal_qty` int(11) NOT NULL DEFAULT '0',
    `status` smallint(6) NOT NULL DEFAULT '0',
    `description` text,
    `website_ids` text,
    `website_id` int(11) DEFAULT NULL,
    `store_ids` text,
    `customer_group_ids` text,
    `promo` text,
    `featured` smallint(6) DEFAULT '0',
    `disableproduct` smallint(6) DEFAULT '0', 
    `requiredlogin` smallint(6) DEFAULT '0',
    `impression` int(32) NOT NULL DEFAULT '0',
    `sold_qty` int(11) default 0,
    `order_id` text default '',
    `store_view` text default '',
    `limit_customer` int(11),
    `disable_product_after_finish` int(11),
    `expire`  int(11) DEFAULT '1',
    `active`  int(11),
    `deal_scheduler_id`  int(11),
    `thread`  int(11),
    PRIMARY KEY (`dailydeal_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$this->getTable('dailydeal/dealscheduler')};
CREATE TABLE {$this->getTable('dailydeal/dealscheduler')} (
    `deal_scheduler_id` int(11) unsigned NOT NULL auto_increment,
    `title` varchar(1024),
    `deal_time` int (11),
    `deal_price` varchar(1024),
    `deal_qty` varchar(1024),
    `number_deal` int(11),
    `number_day` int(11),
    `generate_type` int(11),
    `start_date_time` datetime DEFAULT NULL,
    `end_date_time` datetime DEFAULT NULL,
    `status` smallint(6) NOT NULL,
  PRIMARY KEY (`deal_scheduler_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$this->getTable('dailydeal/dealschedulerproduct')};
CREATE TABLE {$this->getTable('dailydeal/dealschedulerproduct')} (
    `dealschedulerproduct_id` bigint(11) unsigned auto_increment,
    `deal_scheduler_id` int(11) unsigned,
    `product_id` int(11) unsigned,
    `deal_time` int (11),
    `deal_price` float(11),
    `deal_qty` int(11),
    `deal_position` int(11),
    PRIMARY KEY (`dealschedulerproduct_id`),
    FOREIGN KEY (`deal_scheduler_id`) REFERENCES {$this->getTable('dailydeal/dealscheduler')}     (`deal_scheduler_id`)   ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (`product_id`) REFERENCES {$this->getTable('catalog/product')}                    (`entity_id`)           ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- DROP TABLE IF EXISTS {$this->getTable('dailydeal/subscriber')};
CREATE TABLE {$this->getTable('dailydeal/subscriber')} (
    `deal_email_id` int(11) unsigned NOT NULL auto_increment,
    `customer_name` varchar(255) NOT NULL default '',
    `email` varchar(255) NOT NULL default '',
    `status` smallint(6) NOT NULL default '1',
    `subscriber_confirm_code` varchar(255) NULL default '',
    PRIMARY KEY (`deal_email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO {$this->getTable('core_url_rewrite')} (`id_path`, `request_path`, `target_path`, `is_system`) VALUES ('dailydeal/index', 'daily-deals','dailydeal/index/index',1);
INSERT INTO {$this->getTable('core_url_rewrite')} (`id_path`, `request_path`, `target_path`, `is_system`) VALUES ('dailydeal/past', 'daily-deals/past','dailydeal/past/index',1);
INSERT INTO {$this->getTable('core_url_rewrite')} (`id_path`, `request_path`, `target_path`, `is_system`) VALUES ('dailydeal/coming', 'daily-deals/coming','dailydeal/coming/index',1);

");

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->addAttribute('catalog_product', 'activedeal', array(
    'label' => 'Active Deal',
    'type' => 'int',
    'input' => 'hidden',  
    'required' => false,
    'position' => 10,
));

$installer->endSetup(); 