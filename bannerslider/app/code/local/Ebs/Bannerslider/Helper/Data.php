<?php
class Ebs_Bannerslider_Helper_Data extends Mage_Core_Helper_Abstract
{
	/**
     * Path for config.
     */
    const XML_CONFIG_PATH = 'bannerslider/general/';

    /**
     * Name library directory.
     */
    const NAME_DIR_JS = 'bannerslider/jquery/';

    /**
     * List files for include.
     *
     * @var array
     */
    protected $_files = array(
        'jquery.min.js',
        'jquery-migrate.min.js',
        'jquery.noconflict.js',
    );

    /**
     * Check enabled.
     *
     * @return bool
     */
    public function isJqueryEnabled()
    {
        return (bool) $this->_getConfigValue('jquery', $store = '');
    }

    /**
     * Return path file.
     *
     * @param $file
     *
     * @return string
     */
    public function getJQueryPath($file)
    {
        return self::NAME_DIR_JS . $file;
    }

    /**
     * Return list files.
     *
     * @return array
     */
    public function getFiles()
    {
        return $this->_files;
    }

	public function isBannerNextModuleEnabled()
    {
        return (bool) $this->_getConfigValue('active', $store = '');
    }
	
    protected function _getConfigValue($key, $store)
    {
        return Mage::getStoreConfig(self::XML_CONFIG_PATH . $key, $store = '');
    }
	
	public function resizeImg($fileName,$width='',$height='')
	{
		$baseURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
		$imageURL = $baseURL .'/'.'bannerslider'.'/'.$fileName;
		
		$basePath = Mage::getBaseDir('media');
		$imagePath = $basePath.DS.'bannerslider'.str_replace('/', DS,$fileName);
		
		$extra =$width . 'x' . $height;
		$newPath = Mage::getBaseDir('media') . DS .'bannerslider'.DS."resized".DS.$extra.str_replace('/', DS,$fileName);
		//if width empty then return original size image's URL
		if ($width != '' && $height != '') {
			//if image has already resized then just return URL
			if (file_exists($imagePath) && is_file($imagePath) && !file_exists($newPath)) {
				$imageObj = new Varien_Image($imagePath);
				$imageObj->constrainOnly(TRUE);
				$imageObj->keepAspectRatio(FALSE);
				$imageObj->keepFrame(FALSE);
				//$width, $height - sizes you need (Note: when keepAspectRatio(TRUE), height would be ignored)
				$imageObj->resize($width, $height);
				$imageObj->save($newPath);
			}
			$resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "bannerslider".'/'."resized".'/'.$extra.'/'.$fileName;
		 } else {
			$resizedURL = $imageURL;
		 }
		 return $resizedURL;
	}
    
    public function getSortedImages($content)
    {
        $imagesArray = json_decode($content, true);
        if (isset($imagesArray) && !empty($imagesArray) && count($imagesArray) > 0) {
            $temp = array();
            foreach ($imagesArray as $key => $image) {
                if ($image['disabled']) {
                    unset($imagesArray[$key]);
                    continue;
                }
                $temp[$key] = $image['position'];
            }
            array_multisort($temp, SORT_ASC, $imagesArray);
        }
        return $imagesArray;
    }
	
}