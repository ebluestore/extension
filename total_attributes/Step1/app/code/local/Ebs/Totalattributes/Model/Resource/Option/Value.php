<?php

class Ebs_Totalattributes_Model_Resource_Option_Value extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('ebs_totalattributes/option_value', 'value_id');
    }
}