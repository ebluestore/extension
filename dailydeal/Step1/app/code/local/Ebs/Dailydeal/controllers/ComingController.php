<?php

class Ebs_Dailydeal_ComingController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $uri = explode('/dailydeal/coming', $_SERVER['REQUEST_URI']);
        $uri1 = explode('/dailydeal/coming/index', $_SERVER['REQUEST_URI']);

        if ((sizeof($uri) > 1) || (sizeof($uri1) > 1)) {
            $link = Mage::helper('dailydeal')->getUrlHttp('dailydeal/coming', true);
            $this->_redirectUrl($link);
        } else {
            $this->loadLayout();
            $this->renderLayout();
        }
    }

}