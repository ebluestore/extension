<?php
class Ebs_Bannerslider_Block_Adminhtml_Bannerslider_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$model = Mage::registry('bannerslider_data');	
		
		if($model->getStores())
		{			
			$model->setStores(explode(',',$model->getStores()));
		}
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('bannerslider_form', array('legend'=>Mage::helper('bannerslider')->__('General Information')));
		
		$fieldset->addField('title', 'text', array(
			'label'     => Mage::helper('bannerslider')->__('Title'),
			'class'     => 'required-entry',
			'required'  => true,
			'name'      => 'title',
		));
		
		$fieldset->addField('position', 'select', array(
            'label'     => Mage::helper('bannerslider')->__('Position'),
            'name'      => 'position',
            'values'    => Mage::getSingleton('bannerslider/config_source_position')->toOptionArray(),
            'value'     => $model->getPosition()
        ));

        $fieldset->addField('effect', 'select', array(
            'label'     => Mage::helper('bannerslider')->__('Slider Effect'),
            'name'      => 'effect',
            'values'    => Mage::getSingleton('bannerslider/config_source_effect')->toOptionArray(),
            'value'     => $model->getEffect(),
            'note'   	=>'This setting will not apply if you choose position is background slider'
        ));
		
		$fieldset->addField('height', 'text', array(
			'label'     => Mage::helper('bannerslider')->__('Height'),
			'required'  => false,
			'name'      => 'height',
			'class'     =>'validate-number',
			'note'   	=>'E.g you want to height 100px then need to only 100 into textbox'
		));
		
		$fieldset->addField('width', 'text', array(
			'label'     => Mage::helper('bannerslider')->__('Width'),
			'required'  => false,
			'name'      => 'width',
			'class'     =>'validate-number',
			'note'   	=>'E.g you want to width 100px then need to only 100 into textbox'
		));
		
		 if (!Mage::app()->isSingleStoreMode()) {
			$field = $fieldset->addField('stores', 'multiselect', array(
				'name'      => 'stores[]',
				'label'     => Mage::helper('cms')->__('Store View'),
				'title'     => Mage::helper('cms')->__('Store View'),
				'required'  => true,			
				'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
				 'value'     => $model->getStores()				
			));
			$renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
			$field->setRenderer($renderer);
		} else {
			$fieldset->addField('stores', 'hidden', array(
				'name'      => 'stores[]',
				'value'     => Mage::app()->getStore(true)->getId()
			));
			$model->setStoreId(Mage::app()->getStore(true)->getId());
		} 
		
		$fieldset->addField('status', 'select', array(
			'label'     => Mage::helper('bannerslider')->__('Is Active'),
			'name'      => 'status',
			'values'    => array(
				array(
					'value'     => 1,
					'label'     => Mage::helper('bannerslider')->__('Yes'),
				),
				array(
					'value'     => 2,
					'label'     => Mage::helper('bannerslider')->__('No'),
				),
			),
		));
		
		if (Mage::getSingleton('adminhtml/session')->getBannerNextData())
		{
			$form->setValues(Mage::getSingleton('adminhtml/session')->getBannerNextData());
			Mage::getSingleton('adminhtml/session')->setBannerNextData(null);
		} elseif ( Mage::registry('bannerslider_data') ) {
			$form->setValues(Mage::registry('bannerslider_data')->getData());
		}
		return parent::_prepareForm();
	}
}