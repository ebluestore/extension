<?php
require_once  Mage::getModuleDir('controllers', 'Mage_Checkout').DS.'CartController.php';
class Ebs_OneStepCheckout_CartController extends Mage_Checkout_CartController
{
    /**
     * Set back redirect url to response
     *
     * @return Mage_Checkout_CartController
     */
    protected function _goBack()
    {
        $returnUrl = $this->getRequest()->getParam('return_url');
        if ($returnUrl) {
            // clear layout messages in case of external url redirect
            if ($this->_isUrlInternal($returnUrl)) {
                $this->_getSession()->getMessages(true);
            }
            $this->getResponse()->setRedirect($returnUrl);
        } elseif (!Mage::getStoreConfig('checkout/cart/redirect_to_cart') && !$this->getRequest()->getParam('in_cart') && $backUrl = $this->_getRefererUrl()) {
            $this->getResponse()->setRedirect($backUrl);
        } else {
            if (($this->getRequest()->getActionName() == 'add') && !$this->getRequest()->getParam('in_cart')) {
                $this->_getSession()->setContinueShoppingUrl($this->_getRefererUrl());
            }
            //if config enabled, clear messages and redirect to checkout
            if(Mage::getStoreConfig('onestepcheckout/direct_checkout/redirect_to_cart')){

                $customerGroupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
                $allowedGroups = Mage::getStoreConfig('onestepcheckout/direct_checkout/group_ids');

                if(!empty($allowedGroups)){
                    $allowedGroups = explode(',',$allowedGroups);
                } else {
                    $allowedGroups = array();
                }

                if(!in_array($customerGroupId, $allowedGroups)){

                    $this->_getSession()->getMessages(true);
                    $this->_redirect('onestepcheckout', array('_secure'=>true));
                } else {
                    $this->_redirect('checkout/cart');
                }

            } else {
                $this->_redirect('checkout/cart');
            }


        }
        return $this;
    }

    public function removeProductAction()
    {
        $response = array(
            'success' => false,            
            'message' => false,
            'redirect' => false
        );
        $id = (int) $this->getRequest()->getParam('id');        
        if ($id) {
            try {
                Mage::getSingleton('checkout/cart')->removeItem($id)->save();
                $qty = Mage::getSingleton('checkout/cart')->getItemsQty();
                if($qty == 0)
                    $response['redirect'] = Mage::getUrl('checkout/cart');
                $response['success'] = true;
                $response['message'] = $this->__('Item was removes successfully.');
                $html = $this->getLayout()->createBlock('onestepcheckout/summary')->setTemplate('onestepcheckout/summary.phtml')->toHtml();        
                $response['summary'] = $html;
            } catch (Exception $e) {                    
                $response['success'] = false;
                $response['message'] = $this->__('Can not remove item');
            }
        } else {
            $response['success'] = false;
            $response['message'] = $this->__('Product ID can not be nulled');
        }       
        
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }

    public function updateQtyAction()
    {        
        $response = array(
            'success' => false,            
            'message' => false
        );
        $cart   = $this->_getCart();
        $_productID = (int)$this->getRequest()->getParam('id');
        $_action = $this->getRequest()->getParam('update');
        $quote = $cart->getQuote();
        $product = $quote->getItemById($_productID);
        if($_action=="more" && $_productID ){
            $qty = intval($product->getQty()+1);
            $maximumQty = intval(Mage::getModel('catalog/product')->load($product->getProductId())->getStockItem()->getMaxSaleQty());
            
            if($qty > $maximumQty){   
                $response['success'] = false;     
                $response['message'] = $this->__('Product Has Reached To Maximum Allowed Qty: %s', $maximumQty);
                $html = $this->getLayout()->createBlock('onestepcheckout/summary')->setTemplate('onestepcheckout/summary.phtml')->toHtml();
                $response['summary'] = $html;
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
                return;
            }
            
            $product->setQty($qty);
            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
            
            $quote->collectTotals()->save();
            $response['message'] = $this->__('Increased');
            $response['success'] = true;
            $html = $this->getLayout()->createBlock('onestepcheckout/summary')->setTemplate('onestepcheckout/summary.phtml')->toHtml();
            $response['summary'] = $html;
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
            return;     
        } elseif ($_action=="less" && $_productID) {
            $qty = intval($product->getQty()-1);
            $minimumQty = intval(Mage::getModel('catalog/product')->load($product->getProductId())->getStockItem()->getMinSaleQty());
                        
            if($qty < $minimumQty){
                $response['message'] = $this->__('Product Has Reached To Minimal Allowed Qty: %s', $minimumQty);
                $response['success'] = false;
                $html = $this->getLayout()->createBlock('onestepcheckout/summary')->setTemplate('onestepcheckout/summary.phtml')->toHtml();
                $response['summary'] = $html;
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
                return;
            }   
            if($qty > 0){
                $product->setQty($qty);
            }else{
                $quote->removeItem($_productID);
            }
            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
                    
            $quote->collectTotals()->save();
            $response['message'] = $this->__('Decreased');
            $response['success'] = true;
            $html = $this->getLayout()->createBlock('onestepcheckout/summary')->setTemplate('onestepcheckout/summary.phtml')->toHtml();
            $response['summary'] = $html;
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
            return;
        }
    }

}
