<?php


class Ebs_Totalattributes_Block_Widget_Backend_Grid_Date
    extends Ebs_Totalattributes_Block_Widget_Backend_GridAbstract
{
    public function getColumnId()
    {
        return $this->getProperty('code') ? $this->getProperty('code') : null;
    }

    public function getColumnProperties()
    {
        return array(
            'header'                    => $this->_getLabel(),
            'index'                     => $this->getColumnId(),
            'type'                      => 'date',
            'filter_condition_callback' => array($this, 'filterCallback')
        );
    }

    public function filterCallback($collection, $column)
    {
        $condition = $column->getFilter()->getValue();
        if (is_array($condition) && array_key_exists('from', $condition)) {
            $condition['from'] = $condition['orig_from'];
        }
        if (is_array($condition) && array_key_exists('to', $condition)) {
            $condition['to'] = $condition['orig_to'];
        }
        if ($this->getColumnId() && isset($condition)) {
            $collection->addFieldToFilter($this->getColumnId() , $condition);
        }
    }
}