function reset_message_deal_qty(){    
    if ($('dailydeal_general_deal_qty_on_product_page')) {
        $('dailydeal_general_deal_qty_on_product_page').value = 'Hurry, there are only <span class="deal-qty">{{qty}} items</span> left!';
    }
    
    if ($('dailydeal_general_deal_qty_on_catalog_page')) {
        $('dailydeal_general_deal_qty_on_catalog_page').value = 'Hurry, just <span class="deal-qty">{{qty}} items</span> left!';
    }
}