<?php

class Ebs_Totalattributes_Model_Resource_Category_Attribute_Collection extends Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Attribute_Collection
{

    /**
     * Specify filter by "is_visible" field
     *
     * @return Ebs_Totalattributes_Model_Resource_Category_Attribute_Collection
     */
    public function addVisibleFilter()
    {
        $this->getSelect()->where('additional_table.is_visible = ?', 1);
        return $this;
    }
}
