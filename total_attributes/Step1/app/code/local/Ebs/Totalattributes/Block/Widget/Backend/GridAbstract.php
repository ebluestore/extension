<?php


abstract class Ebs_Totalattributes_Block_Widget_Backend_GridAbstract
    implements Ebs_Totalattributes_Block_Widget_Backend_GridInterface
{
    /**
     * @var Ebs_Totalattributes_Model_Attribute_TypeInterface
     */
    protected $_type = null;

    protected $_value = null;

    /**
     * @param Ebs_Totalattributes_Model_Attribute_TypeInterface $type
     *
     * @return Ebs_Totalattributes_Block_Widget_Backend_GridAbstract
     */
    public function setTypeModel(Ebs_Totalattributes_Model_Attribute_TypeInterface $type)
    {
        $this->_type = $type;
        return $this;
    }

    /**
     * @return Ebs_Totalattributes_Model_Attribute_TypeInterface
     */
    public function getTypeModel()
    {
        return $this->_type;
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function getProperty($key)
    {
        if (is_null($this->getTypeModel())) {
            return null;
        }
        if (is_null($this->getTypeModel()->getAttribute())) {
            return null;
        }
        return $this->getTypeModel()->getAttribute()->getData($key);
    }

    /**
     * @param mixed $value
     *
     * @return Ebs_Totalattributes_Block_Widget_Backend_GridAbstract
     */
    public function setValue($value)
    {
        $this->_value = $value;
        return $this;
    }

    /**
     * getter
     *
     * @return string
     */
    protected function _getLabel()
    {
        $storeId = Mage::app()->getStore()->getId();
        $label = $this->getTypeModel()->getAttribute()->getLabel($storeId);
        return Mage::helper('core')->escapeHtml($label);
    }
}