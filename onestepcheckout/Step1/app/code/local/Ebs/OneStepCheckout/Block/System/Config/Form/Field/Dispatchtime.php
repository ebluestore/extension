<?php
class Ebs_Onestepcheckout_Block_System_Config_Form_Field_Dispatchtime extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function __construct()
    {        
        parent::__construct();
    }

    protected function _prepareToRender()
    {         
        $this->addColumn('day', array(
            'label' => $this->__('Day'),
            'style' => 'width:120px',
        ));
        $this->addColumn('month', array(
            'label' => $this->__('Month'),
            'style' => 'width:120px',
        ));
        $this->addColumn('year', array(
            'label' => $this->__('Year'),
            'style' => 'width:120px',
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = $this->__('Add more options');
    }
}