<?php


interface Ebs_Totalattributes_Block_Widget_Backend_ViewInterface
{
    /**
     * @return mixed
     */
    public function getHtml();

    /**
     * @param Ebs_Totalattributes_Model_Attribute_TypeInterface $type
     *
     * @return Ebs_Totalattributes_Block_Widget_Backend_ViewAbstract
     */
    public function setTypeModel(Ebs_Totalattributes_Model_Attribute_TypeInterface $type);

    /**
     * @return Ebs_Totalattributes_Model_Attribute_TypeInterface
     */
    public function getTypeModel();

    /**
     * @param mixed $value
     *
     * @return Ebs_Totalattributes_Block_Widget_Backend_ViewInterface
     */
    public function setValue($value);
}