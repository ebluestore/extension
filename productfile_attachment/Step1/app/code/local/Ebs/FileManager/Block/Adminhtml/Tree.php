<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @author      Ebluestore
 * @package     Ebs_FileManager
 * @copyright   Copyright (c) 2014 Ebluestore
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ebs_FileManager_Block_Adminhtml_Tree extends Mage_Adminhtml_Block_Cms_Wysiwyg_Images_Tree
{
    /**
     * @return Ebs_FileManager_Helper_Media
     */
    protected function _getMediaHelper()
    {
        return Mage::helper('ebs_filemanager/media');
    }

    /**
     * @return Ebs_FileManager_Model_Storage
     */
    protected function _getStorage()
    {
        return Mage::registry('storage');
    }

    /**
     * Json tree builder
     *
     * @return string
     */
    public function getTreeJson()
    {
        $helper = $this->_getMediaHelper();
        $collection = $this->_getStorage()->getDirsCollection($helper->getCurrentPath());
        $jsonArray = array();
        foreach ($collection as $item) {
            $jsonArray[] = array(
                'text'  => $helper->getShortFilename($item->getBasename(), 20),
                'id'    => $helper->convertPathToId($item->getFilename()),
                'cls'   => 'folder'
            );
        }
        return Zend_Json::encode($jsonArray);
    }

    /**
     * Return tree node full path based on current path
     *
     * @return string
     */
    public function getTreeCurrentPath()
    {
        $treePath = '/root';
        if ($path = $this->_getStorage()->getSession()->getCurrentPath()) {
            $helper = $this->_getMediaHelper();
            $path = str_replace($helper->getStorageRoot(), '', $path);
            $relative = '';
            foreach (explode(DS, $path) as $dirName) {
                if ($dirName) {
                    $relative .= DS . $dirName;
                    $treePath .= '/' . $helper->idEncode($relative);
                }
            }
        }
        return $treePath;
    }
}
