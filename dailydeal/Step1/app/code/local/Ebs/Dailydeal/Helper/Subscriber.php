<?php
class Ebs_Dailydeal_Helper_Subscriber extends Mage_Core_Helper_Abstract {

	const XML_PATH_SELECTED_EMAIL_TEMPLATE          = 'dailydeal/deal_subscriber/email_template';
    const XML_PATH_SELECTED_EMAIL_SENDER_IDENTITY   = 'dailydeal/deal_subscriber/email_sender';
    const XML_PATH_INTRO_MESSAGE                    = 'dailydeal/deal_subscriber/intro_message';
    const XML_PATH_THANKS_MESSAGE                   = 'dailydeal/deal_subscriber/thanks_message';

    public function getIntroMessage()
    {
        return Mage::getStoreConfig(self::XML_PATH_INTRO_MESSAGE);
    }
    
    public function getThanksMessage()
    {
        return Mage::getStoreConfig(self::XML_PATH_THANKS_MESSAGE);
    }

    public function unsubscribe($emailId)
    {
        $model = Mage::getModel('dailydeal/subscriber')->load($emailId);
        $model->setStatus(Ebs_Dailydeal_Model_Status::UNSUBSCRIBED);
        $model->save();
    }
    
    public function sendNotificationEmails()
    {
        $store_id = Mage::app()->getStore()->getId();        
        $tblCatalogStockItem = Mage::getSingleton('core/resource')->getTableName('cataloginventory_stock_item');
        $currenttime = date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp(time()));
        $deals = Mage::getModel('dailydeal/dailydeal')->getCollection()
                ->addFieldToFilter('status', Ebs_Dailydeal_Model_Status::STATUS_ENABLED)
                ->addFieldToFilter('expire', Ebs_Dailydeal_Model_Status::STATUS_EXPIRE_FALSE)
                ->addFieldToFilter('store_view', array(array('like' => '%' . Mage::app()->getStore()->getId() . '%'), array('like' => '0')))
                ->addFieldToFilter('start_date_time', array('to' => $currenttime))
                ->addFieldToFilter('end_date_time', array('from' => $currenttime))
                ->addProductStatusFilter($store_id)
                ->getConfigSortBy();
        
        $deals->getSelect()->joinLeft(
            array('stock' => $tblCatalogStockItem), 'stock.product_id = main_table.product_id', array('stock.qty', 'stock.is_in_stock')
        );
        $deals->getSelect()->where("stock.is_in_stock = " . Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK);

        $emails     = Mage::getModel('dailydeal/subscriber')->getCollection()->addFieldToFilter('status', Ebs_Dailydeal_Model_Status::SUBSCRIBED);
        $storeId    = Mage::app()->getStore()->getId();
        $templateId = Mage::getStoreConfig(self::XML_PATH_SELECTED_EMAIL_TEMPLATE, $storeId);
        $mailer     = Mage::getModel('core/email_template_mailer');
        
        if (count($emails)) {
            foreach ($emails as $email) {
                $emailInfo   = Mage::getModel('core/email_info');
                $emailId     = $email->getId();
                $to          = $email->getEmail();
                $name        = $email->getCustomerName();
                $confirmCode = $email->getSubscriberConfirmCode();
                $linkConfirm = Mage::getUrl('dailydeal/index/unsubscribe', array(
                    'id' => $emailId,
                    'code' => $confirmCode
                ));
                $emailInfo->addTo($to, $name);
                $mailer->addEmailInfo($emailInfo);
                $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_SELECTED_EMAIL_SENDER_IDENTITY, $storeId));
                $mailer->setStoreId($storeId);
                $mailer->setTemplateId($templateId);
                $mailer->setTemplateParams(array(
                    'deals' => $deals,
                    'customer_name' => $name,
                    'unsubscribe_link' => $linkConfirm
                ));
                $mailer->send();
            }
        }
    }

    public function checkEmailSubscriber($email)
    {
        $emails = Mage::getModel('dailydeal/subscriber')->getCollection()->addFieldToFilter('email', $email)->addFieldToFilter('status', Ebs_Dailydeal_Model_Status::SUBSCRIBED);
        if (count($emails) > 0) {
            return true;
        } else {
            return false;
        }
    }
}