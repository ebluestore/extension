<?php


abstract class Ebs_Totalattributes_Block_Checkout_Multishipping_Attributes_Abstract extends Mage_Core_Block_Template
{
    protected $_valueData = null;

    /**
     * @return Ebs_Totalattributes_Model_Resource_Attribute_Collection
     */
    public function getAttributeCollection()
    {
        $customerGroupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
        /** @var Ebs_Totalattributes_Model_Resource_Attribute_Collection $collection */
        $collection = Mage::helper('ebs_totalattributes/order')
            ->getAttributeCollectionForCheckoutByCustomerGroupId($customerGroupId);
        return $collection;
    }

    public function getValueByAttributeId($attributeId)
    {
        if (is_null($this->_valueData)) {
            $this->_valueData = array();
            $collection = Mage::helper('ebs_totalattributes/order')
                ->getAttributeValueCollectionForQuote(Mage::helper('checkout')->getQuote());
            foreach ($collection as $item) {
                $this->_valueData[$item->getData('attribute_id')] = $item->getData('value');
            }
        }
        return isset($this->_valueData[$attributeId])?$this->_valueData[$attributeId]:null;
    }
}