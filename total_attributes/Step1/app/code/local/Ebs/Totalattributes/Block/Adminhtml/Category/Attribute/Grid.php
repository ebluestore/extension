<?php

class Ebs_Totalattributes_Block_Adminhtml_Category_Attribute_Grid extends Mage_Eav_Block_Adminhtml_Attribute_Grid_Abstract
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('ebs_categoryAttributesGrid');
        $this->setDefaultSort('attribute_code');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('attribute_filter');
    }
    
    /**
     * Prepare category attributes grid collection object
     *
     * @return Ebs_Totalattributes_Block_Adminhtml_Category_Attribute_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('catalog/category_attribute_collection')->addVisibleFilter();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare category attributes grid columns
     *
     * @return Ebs_Totalattributes_Block_Adminhtml_Category_Attribute_Grid
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumnAfter('is_visible', array(
            'header'=>Mage::helper('ebs_totalattributes')->__('Visible'),
            'sortable'=>true,
            'index'=>'is_visible_on_front',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('ebs_totalattributes')->__('Yes'),
                '0' => Mage::helper('ebs_totalattributes')->__('No'),
            ),
            'align' => 'center',
        ), 'frontend_label');

        $this->addColumnAfter('is_global', array(
            'header'=>Mage::helper('ebs_totalattributes')->__('Scope'),
            'sortable'=>true,
            'index'=>'is_global',
            'type' => 'options',
            'options' => array(
                Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE =>Mage::helper('ebs_totalattributes')->__('Store View'),
                Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE =>Mage::helper('ebs_totalattributes')->__('Website'),
                Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL =>Mage::helper('ebs_totalattributes')->__('Global'),
            ),
            'align' => 'center',
        ), 'is_visible');

        $this->addColumn('is_searchable', array(
            'header'=>Mage::helper('ebs_totalattributes')->__('Searchable'),
            'sortable'=>true,
            'index'=>'is_searchable',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('ebs_totalattributes')->__('Yes'),
                '0' => Mage::helper('ebs_totalattributes')->__('No'),
            ),
            'align' => 'center',
        ), 'is_user_defined');

        $this->addColumnAfter('is_filterable', array(
            'header'=>Mage::helper('ebs_totalattributes')->__('Use in Layered Navigation'),
            'sortable'=>true,
            'index'=>'is_filterable',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('ebs_totalattributes')->__('Filterable (with results)'),
                '2' => Mage::helper('ebs_totalattributes')->__('Filterable (no results)'),
                '0' => Mage::helper('ebs_totalattributes')->__('No'),
            ),
            'align' => 'center',
        ), 'is_searchable');

        $this->addColumnAfter('is_comparable', array(
            'header'=>Mage::helper('ebs_totalattributes')->__('Comparable'),
            'sortable'=>true,
            'index'=>'is_comparable',
            'type' => 'options',
            'options' => array(
                '1' => Mage::helper('ebs_totalattributes')->__('Yes'),
                '0' => Mage::helper('ebs_totalattributes')->__('No'),
            ),
            'align' => 'center',
        ), 'is_filterable');

        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}
