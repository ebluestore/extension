<?php

class Ebs_Totalattributes_Model_Attribute_Type_Multipleselect extends Ebs_Totalattributes_Model_Attribute_TypeAbstract
{
    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Grid_Multipleselect
     */
    protected function _getBackendGridRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Grid_Multipleselect();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Form_Multipleselect
     */
    protected function _getBackendFormRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Form_Multipleselect();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_View_Multipleselect
     */
    protected function _getBackendViewRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_View_Multipleselect();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Pdf_Multipleselect
     */
    protected function _getBackendPdfRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Pdf_Multipleselect();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Frontend_Form_Multipleselect
     */
    protected function _getFrontendFormRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Frontend_Form_Multipleselect();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Frontend_View_Multipleselect
     */
    protected function _getFrontendViewRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Frontend_View_Multipleselect();
    }

    public function getValueType()
    {
        return Ebs_Totalattributes_Model_Resource_Value::TEXT_TYPE;
    }

    /**
     * @param mixed $value
     */
    public function validate($value)
    {
        //START: fix for OA-15 and OA-16 - different js-serialize on mage 1.4 & 1.7
        if (is_array($value) && count($value) === 1) {
            $value = array_pop($value);
        }
        //END
        if (!is_array($value)) {
            if (strlen($value) > 0) {
                $value = explode(',', $value);
            } else {
                //not selected
                $value = '0';
            }
        }
        $helper = Mage::helper('ebs_totalattributes');
        $storeId = Mage::app()->getStore()->getId();
        $label = $this->getAttribute()->getLabel($storeId);
        $label = Mage::helper('core')->escapeHtml($label);
        if ($value === '0') {
            if ($this->getAttribute()->getData('validation_rules/is_required')) {
                throw new Exception($helper->__('%s is required', $label));
            }
        } else {
            $options = $this->getAttribute()->getStoreOptions();
            foreach ($value as $valueItem) {
                if (!array_key_exists($valueItem, $options)) {
                    throw new Exception($helper->__('%s has incorrect selected option', $label));
                }
            }
        }
        return;
    }

    /**
     * @param Ebs_ORderattributes_Model_Value $valueModel
     *
     * @return Ebs_Totalattributes_Model_Value
     */
    public function beforeSave($valueModel)
    {
        $value = $valueModel->getData('value');
        if (is_array($value)) {
            $value = implode(',', $value);
        } else {
            //not selected
            $value = '0';
        }
        $valueModel->setData('value', $value);
        return $valueModel;
    }
}