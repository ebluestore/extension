<?php

class Ebs_Helpmate_Model_Mysql4_Ticket_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('helpmate/ticket');
    }

    public function addCustomerFilter($customerId)
    {
        $this->getSelect()->where('main_table.customer_id=?', $customerId);
        return $this;
    }
    
    public function addCustomerData()
    {
        $customer   = Mage::getModel('customer/customer');
        
        $attributes = array(
            'customer_email'        => 'email'
        );
        
        foreach ($attributes as $alias => $attributeCode) {
            $attribute = $customer->getAttribute($attributeCode);
            /* @var $attribute Mage_Eav_Model_Entity_Attribute_Abstract */

            if ($attribute->getBackendType() == 'static') {
                $tableAlias = 'customer_' . $attribute->getAttributeCode();

                $this->getSelect()->joinLeft(
                    array($tableAlias => $attribute->getBackend()->getTable()),
                    sprintf('%s.entity_id=main_table.customer_id', $tableAlias),
                    array($alias => $attribute->getAttributeCode())
                );

                $this->_fields[$alias] = sprintf('%s.%s', $tableAlias, $attribute->getAttributeCode());
            } else {
                $tableAlias = 'customer_' . $attribute->getAttributeCode();

                $joinConds  = array(
                    sprintf('%s.entity_id=main_table.customer_id', $tableAlias),
                    $this->getConnection()->quoteInto($tableAlias . '.attribute_id=?', $attribute->getAttributeId())
                );

                $this->getSelect()->joinLeft(
                    array($tableAlias => $attribute->getBackend()->getTable()),
                    join(' AND ', $joinConds),
                    array($alias => 'value')
                );

                $this->_fields[$alias] = sprintf('%s.value', $tableAlias);
            }
        }
        $this->setFlag('has_customer_data', true);
        return $this;
    }

    public function addStoreFilter($store)
    {
        if ($store instanceof Mage_Core_Model_Store) {
            $store = array($store->getId());
        }
        if (!is_array($store)) {
            $store = array($store);
        }
        $this->getSelect()->where('main_table.store_id IN (?)', $store);
        return $this;
    }

    public function addFieldToFilter($field, $condition=null)
    {
        if (isset($this->_fields[$field])) {
            $field = $this->_fields[$field];
        }

        return parent::addFieldToFilter($field, $condition);
    }

    public function addStatusFilter($status = true)
    {
        $this->getSelect()->where('main_table.status = ?', $status);
        return $this;
    }
    
    public function addLessModifiedAtFilter(Zend_Date $time)
    {
        $time = $time->toString('YYYY-MM-dd HH:mm:ss', 'iso');
        $this->getSelect()->where('main_table.modified_at < ?', $time);
        return $this;
    }
}