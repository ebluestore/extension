<?php

class Ebs_FileManager_Block_Adminhtml_Productfile_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('productfileGrid');
        $this->setDefaultSort('productfile_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }
    
    protected function _prepareCollection()
    {
        $store_id   = $this->getRequest()->getParam('store');
        $collection = Mage::getModel('ebs_filemanager/productfile')->getCollection()->setStoreId($store_id);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
        $this->addColumn('productfile_id', array(
            'header' => Mage::helper('ebs_filemanager')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'productfile_id'
        ));
        
        $this->addColumn('title', array(
            'header' => Mage::helper('ebs_filemanager')->__('Name'),
            'align' => 'left',
            'index' => 'title'
        ));
        
        $this->addColumn('filename', array(
            'header' => Mage::helper('ebs_filemanager')->__('File Name'),
            'align' => 'left',
            'index' => 'filename'
        ));
        
        $this->addColumn('fileextension', array(
            'header' => Mage::helper('ebs_filemanager')->__('File Extension'),
            'align' => 'left',
            'index' => 'fileextension'
        ));
                
        $this->addColumn('filesize', array(
            'header' => Mage::helper('ebs_filemanager')->__('File Size'),
            'align' => 'left',
            'type' => 'number',
            'index' => 'filesize'
        ));
                
        $this->addColumn('status', array(
            'header' => Mage::helper('ebs_filemanager')->__('Status'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                1 => 'Enabled',
                2 => 'Disabled'
            )
        ));
        
        $storeId = $this->getRequest()->getParam('store', 0);
        $this->addColumn('action', array(
            'header' => Mage::helper('ebs_filemanager')->__('Action'),
            'width' => '100',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('ebs_filemanager')->__('Edit'),
                    'url' => array(
                        'base' => '*/*/edit/store/' . $storeId
                    ),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'is_system' => true
        ));
        
        $this->addExportType('*/*/exportCsv', Mage::helper('ebs_filemanager')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('ebs_filemanager')->__('XML'));
        
        return parent::_prepareColumns();
    }
    
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('productfile_id');
        $this->getMassactionBlock()->setFormFieldName('ebs_filemanager');
        
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('ebs_filemanager')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('ebs_filemanager')->__('Are you sure?')
        ));
        
        $statuses = Mage::getSingleton('ebs_filemanager/status')->getOptionArray();
        $storeId  = $this->getRequest()->getParam('store', 0);
        array_unshift($statuses, array(
            'label' => '',
            'value' => ''
        ));
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('ebs_filemanager')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus/store/' . $storeId, array(
                '_current' => true
            )),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('ebs_filemanager')->__('Status'),
                    'values' => $statuses
                )
            )
        ));
        return $this;
    }
    
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $row->getId(),
            'store' => $this->getRequest()->getParam('store')
        ));
    }
    
}

?>