jQuery( document ).ready(function() {
    jQuery('#onestepcheckout-login-popup').popup();
    jQuery('#onestepcheckout-toc-popup').popup();

    if(jQuery('.view-term').val() == 1){
    	jQuery('.checkout-agreements li p label').each(function(element){
            jQuery( this ).before('<a href="javascript:void(0);" class="term_item_' + jQuery( this ).attr('termid') + '_open">' + jQuery( this ).text() + '</a>');
            jQuery( this ).hide();
            jQuery('#term_item_' + jQuery( this ).attr('termid')).popup();
        });        
    }

    jQuery('.additional-info').click(function(e) {
        e.preventDefault();
        jQuery(this).next('.additional-info-content').slideToggle('slow');
    });
});