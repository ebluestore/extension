<?php


class Ebs_Totalattributes_Block_Adminhtml_Sales_Order extends Mage_Adminhtml_Block_Sales_Order
{
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'ebs_totalattributes';
        $this->_controller = 'adminhtml_sales_order';
    }

    public function getCreateUrl()
    {
        return $this->getUrl('adminhtml/sales_order_create/start');
    }
}