<?php


class Ebs_Totalattributes_Block_Widget_Backend_Grid_Multipleselect
    extends Ebs_Totalattributes_Block_Widget_Backend_GridAbstract
{
    public function getColumnId()
    {
        return $this->getProperty('code') ? $this->getProperty('code') : null;
    }

    public function getColumnProperties()
    {
        return array(
            'header'   => $this->_getLabel(),
            'index'    => $this->getColumnId(),
            'sortable' => false,
            'type'     => 'options',
            'options'  => $this->_getOptionsForSelect(),
            'renderer' => 'ebs_totalattributes/widget_backend_grid_multipleselect_renderer',
        );
    }

    private function _getOptionsForSelect()
    {
        if (is_null($this->getTypeModel())) {
            return null;
        }
        $attribute = $this->getTypeModel()->getAttribute();
        if (is_null($attribute)) {
            return null;
        }
        $storeId = Mage::app()->getStore()->getId();
        $options = Mage::helper('ebs_totalattributes/options')
            ->getOptionsForAttributeByStoreId($attribute, $storeId, false);
        foreach($options as $key => $value) {
            $options[$key] = htmlspecialchars($value);
        }
        return $options;
    }
}