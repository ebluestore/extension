<?php
/**
 * Ebluestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Abtesting to newer
 * versions in the future.
 *
 * @category    Ebluestore
 * @package     Ebs_Abtesting
 * @copyright   Copyright (c) 2014 Ebluestore LLC (http://www.ebluestore.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
$installer = $this;
$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$setup->addAttribute('catalog_category', 'abtesting_category_text', array(
    'group'         => 'A/B Testing Google Experiment Code',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'Google Experiment Code',
    'backend'       => '',
    'visible'       => 1,
    'required'      => 0,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

$setup->addAttribute('catalog_product', 'abtesting_product_text', array(
    'group'         => 'A/B Testing Google Experiment Code',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'Google Experiment Code',
    'backend'       => '',
    'visible'       => 1,
    'required'      => 0,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));


 
$installer->run(' ALTER TABLE `cms_page` ADD `abtesting_content_fieldset` text NULL; ');

//$setup->removeAttribute('catalog_category', 'abtesting_category_text');
//$setup->removeAttribute('catalog_product', 'abtesting_product_text');

$installer->endSetup();
	 