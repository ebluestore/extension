<?php

class Ebs_Akismet_Model_Akismet extends Mage_Core_Model_Abstract
{
    const XML_PATH_AKISMET_ACTIVE = 'default/akismet/general/active';
    const XML_PATH_AKISMET_API_KEY = 'default/akismet/general/api_key';
    
    protected $_service;

    public function isSpam(array $data, $commentType = 'contact')
    {
        $isSpam = false;
        $service = $this->getService();
        $apiKey = $this->getApiKey();
        $helper = $this->getHelper();
        if ($service->verifyKey($apiKey)) {
            $data = new Varien_Object($data);
            $httpHelper = $this->getHttpHelper();
            $data = array(
                'user_ip'              => $httpHelper->getRemoteAddr(),
                'user_agent'           => $httpHelper->getHttpUserAgent(),
                'comment_type'         => $commentType,
                'comment_author'       => $data->getName(),
                'comment_author_email' => $data->getEmail(),
                'comment_content'      => $data->getComment()
            );
            if ($service->isSpam($data)) {
                $logMessage = sprintf('Akismet Spam Detected "%s" from IP: "%s"', $data['comment_content'], $data['user_ip']);
                $helper->log($logMessage);
                $isSpam = true;
            }
        } else {
            $logMessage = sprintf('Akismet ApiKey "%s" is not valid', $apiKey);
            $helper->log($logMessage);
        }
        return $isSpam;
    }
    
    public function isActive()
    {
        $isActive = (int)Mage::getConfig()->getNode(self::XML_PATH_AKISMET_ACTIVE);
        return $isActive;
    }
    
    public function getService()
    {
        if (!$this->_service) {
            $this->_service =  new Zend_Service_Akismet(self::getApiKey(), Mage::app()->getStore()->getBaseUrl());
        }
        return $this->_service;
    }
    
    public function getApiKey()
    {
        $apiKey = (string)Mage::getConfig()->getNode(self::XML_PATH_AKISMET_API_KEY);
        return trim($apiKey);
    }
    
    public function getHttpHelper()
    {
        return Mage::helper('core/http');
    }
    
    public function getHelper()
    {
    	return Mage::helper('akismet');
    }
}