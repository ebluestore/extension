<?php

class Ebs_Totalattributes_Model_Resource_Attribute extends Mage_Core_Model_Mysql4_Abstract
{
    protected $_serializableFields = array(
        'validation_rules' => array(null, array()),
    );

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('ebs_totalattributes/attribute', 'attribute_id');
    }
}