<?php

class Ebs_Productlabel_Model_Productlabelentityvalue extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('productlabel/productlabelentityvalue');
    }

    public function loadAttributeValue($labelentityId, $storeId, $attributeCode) {
        $attributeValue = $this->getCollection()
                ->addFieldToFilter('product_label_entity_id', $labelentityId)
                ->addFieldToFilter('store_id', $storeId)
                ->addFieldToFilter('attribute_code', $attributeCode)
                ->getFirstItem();

        $this->setData('product_label_entity_id', $labelentityId)
                ->setData('store_id', $storeId)
                ->setData('attribute_code', $attributeCode);
        if ($attributeValue) {

            $this->addData($attributeValue->getData())
                    ->setId($attributeValue->getId());
        }

        return $this;
    }

} 

?>