<?php

class Ebs_Stockstatus_Model_Rewrite_Sales_Quote_Item extends Mage_Sales_Model_Quote_Item
{
    public function getMessage($string = true)
    {
        if (('checkout' == Mage::app()->getRequest()->getModuleName()) && Mage::getStoreConfig('stockstatus/general/displayincart')) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $this->getSku());
            if (!$product) {
                $product = Mage::getModel('catalog/product')->load($this->getProduct()->getId());
            }
            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            if (!(Mage::getStoreConfig('stockstatus/general/displayforoutonly') && $product->isSaleable()) || ($product->isInStock() && $stockItem->getData('qty') <= Mage::helper('stockstatus')->getBackorderQnt())) {
                $status = Mage::helper('stockstatus')->getCustomStockStatusText(Mage::getModel('catalog/product')->load($product->getId()));
                if ($status && (!Mage::registry('is_duplicate') || (Mage::registry('is_duplicate') && !array_key_exists($product->getId(), Mage::registry('is_duplicate'))))) {
                    
                    $this->addMessage($status);
                    if (Mage::registry('is_duplicate'))
                        $massKey = Mage::registry('is_duplicate');
                    else
                        $massKey = array();
                    $massKey[$product->getId()] = $product->getId();
                    Mage::unregister('is_duplicate');
                    Mage::register('is_duplicate', $massKey);
                }
            }
        }
        return parent::getMessage($string);
    }
}