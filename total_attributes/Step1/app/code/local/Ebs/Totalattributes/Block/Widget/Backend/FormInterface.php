<?php


interface Ebs_Totalattributes_Block_Widget_Backend_FormInterface
{
    /**
     * @return string
     */
    public function getFieldId();

    /**
     * @return string
     */
    public function getFieldType();

    /**
     * @return string|null
     */
    public function getFieldTypeRenderer();

    /**
     * @return array
     */
    public function getFieldProperties();

    /**
     * @param Ebs_Totalattributes_Model_Attribute_TypeInterface $type
     *
     * @return Ebs_Totalattributes_Block_Widget_Backend_FormAbstract
     */
    public function setTypeModel(Ebs_Totalattributes_Model_Attribute_TypeInterface $type);

    /**
     * @return Ebs_Totalattributes_Model_Attribute_TypeInterface
     */
    public function getTypeModel();

    /**
     * @param mixed $value
     *
     * @return Ebs_Totalattributes_Block_Widget_Backend_FormInterface
     */
    public function setValue($value);
}