<?php


class Ebs_Totalattributes_Block_Widget_Backend_Form_Yesno
    extends Ebs_Totalattributes_Block_Widget_Backend_FormAbstract
{
    public function getFieldId()
    {
        return $this->_getCode();
    }

    public function getFieldType()
    {
        return 'select';
    }

    public function getFieldTypeRenderer()
    {
        return null;
    }

    public function getFieldProperties()
    {
        $properties = array(
            'label'    => $this->_getLabel(),
            'name'     => $this->_getCode(),
            'values'   => Mage::helper('ebs_totalattributes/options')->getOptionsForYesnoAttribute(true, true),
            'required' => $this->getProperty('validation_rules/is_required') ? true : false,
        );
        return $properties;
    }
}