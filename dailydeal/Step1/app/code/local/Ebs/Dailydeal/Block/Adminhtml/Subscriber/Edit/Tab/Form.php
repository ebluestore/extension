<?php

class Ebs_Dailydeal_Block_Adminhtml_Subscriber_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('dailydeal_');
        $this->setForm($form);
        $fieldset = $form->addFieldset('dailydeal_form', array(
            'legend' => Mage::helper('dailydeal')->__('Email information')
        ));
        
        if (Mage::getSingleton('adminhtml/session')->getSubscriberData()) {
            $data = Mage::getSingleton('adminhtml/session')->getSubscriberData();
            Mage::getSingleton('adminhtml/session')->setSubscriberData(null);
        } elseif (Mage::registry('subscriber_data')) {
            $data = Mage::registry('subscriber_data')->getData();
        }
        
        $fieldset->addField('customer_name', 'text', array(
            'label' => Mage::helper('dailydeal')->__('Customer Name'),
            'required' => false,
            'name' => 'customer_name',
            'width' => '500px'
        ));

        $fieldset->addField('email', 'text', array(
            'label' => Mage::helper('dailydeal')->__('Email'),
            'required' => false,
            'name' => 'email',
            'width' => '500px'
        ));
        
        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('dailydeal')->__('Status'),
            'name' => 'status',
            'values' => array(
                array(
                    'value' => Ebs_Dailydeal_Model_Status::SUBSCRIBED,
                    'label' => Mage::helper('dailydeal')->__('Subscribed')
                ),
                
                array(
                    'value' => Ebs_Dailydeal_Model_Status::UNSUBSCRIBED,
                    'label' => Mage::helper('dailydeal')->__('Unsubscribed')
                )
            )
        ));
        
        $form->setValues($data);
        return parent::_prepareForm();
    }
}