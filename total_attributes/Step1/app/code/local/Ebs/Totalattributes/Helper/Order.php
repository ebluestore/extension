<?php

class Ebs_Totalattributes_Helper_Order extends Mage_Core_Helper_Data
{
    /**
     * @return Ebs_Totalattributes_Model_Resource_Attribute_Collection
     */
    public function getAttributeCollectionForOrderGrid()
    {
        $collection = Mage::getModel('ebs_totalattributes/attribute')->getCollection();
        $collection
            ->addIsEnabledFilter()
            ->addIsDisplayOnGridFilter()
            ->sortBySortOrder(Varien_Data_Collection::SORT_ORDER_DESC)
        ;
        return $collection;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return Ebs_Totalattributes_Model_Resource_Attribute_Collection
     */
    public function getAttributeCollectionForOrderViewByAdmin()
    {
        $collection = Mage::getModel('ebs_totalattributes/attribute')->getCollection();
        $collection
            ->addIsEnabledFilter()
            ->sortBySortOrder()
        ;
        return $collection;
    }

    /**
     * @return Ebs_Totalattributes_Model_Resource_Attribute_Collection
     */
    public function getAttributeCollectionForPdf()
    {
        $collection = Mage::getModel('ebs_totalattributes/attribute')->getCollection();
        $collection
            ->addIsEnabledFilter()
            ->addIsDisplayOnPdfFilter()
            ->sortBySortOrder()
        ;
        return $collection;
    }

    /**
     * @return Ebs_Totalattributes_Model_Resource_Attribute_Collection
     */
    public function getAttributeCollectionForCustomerAccount()
    {
        $collection = Mage::getModel('ebs_totalattributes/attribute')->getCollection();
        $collection
            ->addIsEnabledFilter()
            ->addIsDisplayOnCustomerAccountFilter()
            ->sortBySortOrder()
        ;
        return $collection;
    }

    /**
     * @param int $customerGroupId
     * @return Ebs_Totalattributes_Model_Resource_Attribute_Collection
     */
    public function getAttributeCollectionForCheckoutByCustomerGroupId($customerGroupId)
    {
        $collection = Mage::getModel('ebs_totalattributes/attribute')->getCollection();
        $collection
            ->addIsEnabledFilter()
            ->addStoreFilter(Mage::app()->getStore()->getId())
            ->addCustomerGroupFilter($customerGroupId)
            ->sortBySortOrder()
        ;
        return $collection;
    }

    /**
     * @param Mage_Sales_Model_Quote|int $quote
     * @return object
     */
    public function getAttributeValueCollectionForQuote($quote)
    {
        if ($quote instanceof Mage_Sales_Model_Quote) {
            $quote = $quote->getId();
        } elseif (!is_numeric($quote)) {
            $quote = 0;
        }
        $collection = Mage::getModel('ebs_totalattributes/value')->getCollection();
        $collection->addQuoteFilter($quote);
        return $collection;
    }
}