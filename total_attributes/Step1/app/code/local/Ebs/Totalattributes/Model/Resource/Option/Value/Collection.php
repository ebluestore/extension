<?php

class Ebs_Totalattributes_Model_Resource_Option_Value_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ebs_totalattributes/option_value');
    }

    /**
     * @param $attribute Ebs_Totalattributes_Model_Option|int
     *
     * @return Ebs_Totalattributes_Model_Resource_Option_Value_Collection
     */
    public function addOptionFilter($option)
    {
        if ($option instanceof Ebs_Totalattributes_Model_Option) {
            $optionId = $option->getId();
        } elseif(is_numeric($option)) {
            $optionId = $option;
        } else {
            return $this;
        }
        $this->addFieldToFilter('option_id', $optionId);
        return $this;
    }
}