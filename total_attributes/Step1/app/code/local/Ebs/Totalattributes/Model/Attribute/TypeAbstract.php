<?php

abstract class Ebs_Totalattributes_Model_Attribute_TypeAbstract implements Ebs_Totalattributes_Model_Attribute_TypeInterface
{

    const FRONTEND_ATTRIBUTE_CODE_PREFIX = 'oa_';

    /**
     * @var Ebs_Totalattributes_Model_Attribute
     */
    protected $_attribute = null;

    /**
     * @var Ebs_Totalattributes_Block_Widget_Backend_GridInterface|null
     */
    protected $_backendGridRenderer = null;
    /**
     * @var Ebs_Totalattributes_Block_Widget_Backend_FormInterface|null
     */
    protected $_backendFormRenderer  = null;
    /**
     * @var Ebs_Totalattributes_Block_Widget_Backend_ViewInterface|null
     */
    protected $_backendViewRenderer  = null;
    /**
     * @var Ebs_Totalattributes_Block_Widget_Backend_PdfInterface|null
     */
    protected $_backendPdfRenderer  = null;
    /**
     * @var Ebs_Totalattributes_Block_Widget_Frontend_FormInterface|null
     */
    protected $_frontendFormRenderer  = null;
    /**
     * @var Ebs_Totalattributes_Block_Widget_Frontend_ViewInterface|null
     */
    protected $_frontendViewRenderer  = null;

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_GridInterface
     * @throws Exception
     */
    public function getBackendGridRenderer()
    {
        if (is_null($this->_backendGridRenderer)) {
            $this->_backendGridRenderer = $this->_getBackendGridRenderer();
            if (!($this->_backendGridRenderer instanceof Ebs_Totalattributes_Block_Widget_Backend_GridInterface)) {
                throw new Exception('Wrong renderer');
            }
            $this->_backendGridRenderer->setTypeModel($this);
        }
        return $this->_backendGridRenderer;
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_FormInterface
     * @throws Exception
     */
    public function getBackendFormRenderer()
    {
        if (is_null($this->_backendFormRenderer)) {
            $this->_backendFormRenderer = $this->_getBackendFormRenderer();
            if (!($this->_backendFormRenderer instanceof Ebs_Totalattributes_Block_Widget_Backend_FormInterface)) {
                throw new Exception('Wrong renderer');
            }
            $this->_backendFormRenderer->setTypeModel($this);
        }
        return $this->_backendFormRenderer;
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_FormInterface
     * @throws Exception
     */
    public function getBackendViewRenderer()
    {
        if (is_null($this->_backendViewRenderer)) {
            $this->_backendViewRenderer = $this->_getBackendViewRenderer();
            if (!($this->_backendViewRenderer instanceof Ebs_Totalattributes_Block_Widget_Backend_ViewInterface)) {
                throw new Exception('Wrong renderer');
            }
            $this->_backendViewRenderer->setTypeModel($this);
        }
        return $this->_backendViewRenderer;
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_FormInterface
     * @throws Exception
     */
    public function getBackendPdfRenderer()
    {
        if (is_null($this->_backendPdfRenderer)) {
            $this->_backendPdfRenderer = $this->_getBackendPdfRenderer();
            if (!($this->_backendPdfRenderer instanceof Ebs_Totalattributes_Block_Widget_Backend_PdfInterface)) {
                throw new Exception('Wrong renderer');
            }
            $this->_backendPdfRenderer->setTypeModel($this);
        }
        return $this->_backendPdfRenderer;
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Frontend_FormInterface
     * @throws Exception
     */
    public function getFrontendFormRenderer()
    {
        if (is_null($this->_frontendFormRenderer)) {
            $this->_frontendFormRenderer = $this->_getFrontendFormRenderer();
            if (!($this->_frontendFormRenderer instanceof Ebs_Totalattributes_Block_Widget_Frontend_FormInterface)) {
                throw new Exception('Wrong renderer');
            }
            $this->_frontendFormRenderer->setTypeModel($this);
        }
        return $this->_frontendFormRenderer;
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Frontend_ViewInterface
     * @throws Exception
     */
    public function getFrontendViewRenderer()
    {
        if (is_null($this->_frontendViewRenderer)) {
            $this->_frontendViewRenderer = $this->_getFrontendViewRenderer();
            if (!($this->_frontendViewRenderer instanceof Ebs_Totalattributes_Block_Widget_Frontend_ViewInterface)) {
                throw new Exception('Wrong renderer');
            }
            $this->_frontendViewRenderer->setTypeModel($this);
        }
        return $this->_frontendViewRenderer;
    }


    /**
     * @param Ebs_Totalattributes_Model_Value $valueModel
     *
     * @return Ebs_Totalattributes_Model_Value
     */
    public function beforeSave($valueModel)
    {
        return $valueModel;
    }

    /**
     * @return Ebs_Totalattributes_Model_Attribute_TypeAbstract
     */
    public function afterAttributeDelete()
    {
        return $this;
    }

    /**
     * @param mixed $value
     * @param array $rules
     *
     */
    public function validate($value)
    {
        return;
    }

    /**
     * @param Ebs_Totalattributes_Model_Attribute $attribute
     *
     * @return Ebs_Totalattributes_Model_Attribute_TypeAbstract
     */
    public function setAttribute(Ebs_Totalattributes_Model_Attribute $attribute)
    {
        $this->_attribute = $attribute;
        return $this;
    }

    /**
     * @param Ebs_Totalattributes_Model_Attribute $attribute
     *
     * @return Ebs_Totalattributes_Model_Attribute_TypeAbstract
     */
    public function getAttribute()
    {
        return $this->_attribute;
    }
}