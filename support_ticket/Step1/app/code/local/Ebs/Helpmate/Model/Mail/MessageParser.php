<?php
class Ebs_Helpmate_Model_Mail_MessageParser
{
    /**
     *
     * @var Zend_Mail_Message 
     */
    protected $_message;
    
    /**
     *
     * @var string 
     */
    protected $_content   = '';
    
    /**
     *
     * @var string 
     */
    protected $_fileNames = '';

    public function __construct(Zend_Mail_Message $message)
    {
        $this->_message = $message;
    }
    
    public function getContent() 
    {
        if (!empty($this->_content)) {
            return $this->_content;
        }
        $content = '';
        
        $message = $this->_message;
        if ($message->isMultipart()) {
            foreach (new RecursiveIteratorIterator($message) as $part) {
                /* @var $part Zend_Mail_Part */
                try {
                    $type     = $part->getHeaderField('content-type');
                    $charset  = $part->getHeaderField('content-type', 'charset');
                    $encoding = null;
                    if ($part->headerExists('content-transfer-encoding')) {
                        $encoding = $part->getHeader('content-transfer-encoding');
                    }
                    
                    switch ($type) {
                        case 'text/plain':
                            $content = $part->getContent();
                            
                            $content = $this->_decode($content, $encoding);
                            $content = $this->_toUtf8($content, $charset);
                            
                            break;

                        case 'text/html':
                            if (empty($content)) {
                                $content = $part->getContent();
                            
                                $content = $this->_decode($content, $encoding);
                                $content = $this->_toUtf8($content, $charset);
                                $content = $this->_htmlToText($content);
                            }
                            break;

                        default: 
                        break;
                    }
                } catch (Zend_Mail_Exception $e) {
                    Mage::log($e.'\n', null, 'email.log');
                }
            }
        // is not multipart
        } else {
            $type     = $message->getHeaderField('content-type');
            $charset  = $message->getHeaderField('content-type', 'charset');
            $encoding = null;
            if ($message->headerExists('content-transfer-encoding')) {
                $encoding = $message->getHeader('content-transfer-encoding');
            }
            
            $content = $message->getContent();
            $content = $this->_decode($content, $encoding);
            $content = $this->_toUtf8($content, $charset);
        }
        $this->_content = $content;
        return $this->_content;
    }

    protected function _htmlToText($html)
    {
        $tags = array (
            0 => '~<h[123][^>]+>~si',
            1 => '~<h[456][^>]+>~si',
            2 => '~<table[^>]+>~si',
            3 => '~<tr[^>]+>~si',
            4 => '~<li[^>]+>~si',
            5 => '~<br[^>]+>~si',
            6 => '~<p[^>]+>~si',
            7 => '~<div[^>]+>~si',
        );
        $html = preg_replace($tags, "\n", $html);
        $html = preg_replace('~</t(d|h)>\s*<t(d|h)[^>]+>~si', ' - ', $html);
        $html = preg_replace('~<[^>]+>~s', '', $html);
        // reducing spaces
        $html = preg_replace('~ +~s', ' ', $html);
        $html = preg_replace('~^\s+~m', '', $html);
        $html = preg_replace('~\s+$~m', '', $html);
        // reducing newlines
        $html = preg_replace('~\n+~s', "\n", $html);
        return $html;
    }
    
    private function _toUtf8($content, $charset) 
    {
        if (empty($charset)
            || 'UTF-8' == $charset
            || !in_array($charset, mb_list_encodings())) {
            
            return $content;
        }
        return mb_convert_encoding($content, "UTF-8", $charset);
    }
    
    private function _decode($content, $encoding)
    {
        switch ($encoding) {
            case 'quoted-printable':
                $content = quoted_printable_decode($content);
                break;
            case 'base64':
                $content = base64_decode($content);
                break;
            default:
                $content = $content;
                break;
        }
        
        return $content;
    }
    
    public function getFilenames() 
    {
        if (!empty($this->_fileNames)) {
            return $this->_fileNames;
        }
        if (true != Mage::getStoreConfig('helpmate/general/enabledAttached')) {
            return '';
        }
        
        if (!$this->_message->isMultipart()) {
            return '';
        }
        
        $file = '';
        $attachedAllowedExtensions = explode(',', Mage::getStoreConfig('helpmate/general/attachedAllowedExtensions'));
        
        $path = Mage::getBaseDir('media') . DS . 'helpmate' . DS;
        
        foreach (new RecursiveIteratorIterator($this->_message) as $part) {
            /* @var $part Zend_Mail_Part */
            try {
                $type = $part->getHeaderField('content-type');
                
                if (in_array($type, array('text/plain', 'text/html'))) {
                    continue;
                }
                
                $encoding = null;
                if ($part->headerExists('content-transfer-encoding')) {
                    $encoding = $part->getHeader('content-transfer-encoding');
                }
                if ($part->headerExists('content-disposition')) {
                    $name = preg_replace(
                        '/^attachment; filename=(.+)$/', 
                        "$1", 
                        $part->getHeader('content-disposition')
                    );
                } else {
                    $name = rand(time());
                }
                $name = trim($name, "\'\"");

                $content = $part->getContent();
                $content = $this->_decode($content, $encoding);
                
                $fileExtension = substr($name, strrpos($name, '.') + 1);
                $name = time() . '.' . $name;
	        
		if (!in_array(strtolower($fileExtension), $attachedAllowedExtensions)) {
                    throw new Exception('Disallowed file type.');	             
		}
		file_put_contents($path . $name, $content);
                
            } catch (Exception $e) {

            }
            $file .= ';' . $name;
        }     
        $this->_fileNames = trim($file, ';');
        return $this->_fileNames;
    }
    
    /**
     *
     * @return string 
     */
    public function getMessageId() 
    {
        $message = $this->_message;
        
        $value = null;
        if ($message->headerExists('Message-ID')) {
            $value = $message->getHeader('Message-ID');
        } elseif($message->headerExists('message-id')) {
            $value = $message->getHeader('message-id');
        }
        
        if (preg_match('|<(.*?)>|', $value, $regs)) {
            $value = $regs[1];
        }
        return $value;
    }
        
    /**
     *
     * @return string 
     */
    public function getInReplyTo() 
    {
        $message = $this->_message;
        
        $value = null;
        if ($message->headerExists('In-Reply-To')) {
            $value = $message->getHeader('In-Reply-To');
        } elseif($message->headerExists('in-reply-to')) {
            $value = $message->getHeader('in-reply-to');
        }
        
        if (preg_match('|<(.*?)>|', $value, $regs)) {
            $value = $regs[1];
        }
        return $value;      
    }
    
    /**
     *
     * @return string 
     */
    public function getCreatedAt() 
    {
        return date('Y-m-d H:i:s', strtotime($this->_message->date));
    }
    
    /**
     *
     * @return string 
     */
    public function getFrom() 
    {       
        $value = $this->_message->from;  
        if (preg_match('|<(.*?)>|', $value, $regs)) {
            $value = $regs[1];
        }
        return $value;
    }
    
    /**
     *
     * @return string 
     */
    public function getSubject() 
    {
        return iconv_mime_decode(
            $this->_message->subject, 
            ICONV_MIME_DECODE_CONTINUE_ON_ERROR, 
            "UTF-8"
        );
    }
    
    /**
     *
     * @param string $subject [optional]
     * @return mixed int|void 
     */
    public function getTicketIdFromSubject($subject = null) 
    {
        if (null === $subject) {
            $subject = $this->getSubject();
        }
        
        preg_match("/\s*\(ticket#\s*(?P<ticketId>\d+)\)/", $subject, $matches);
        
        return isset($matches['ticketId']) ?
           (int) $matches['ticketId'] - 1000000 : null;
    }
    
}
