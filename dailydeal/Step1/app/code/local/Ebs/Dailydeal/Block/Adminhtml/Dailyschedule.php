<?php

class Ebs_Dailydeal_Block_Adminhtml_Dailyschedule extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	
    public function __construct()
    {
        $this->_controller = 'adminhtml_dailyschedule';
        $this->_blockGroup = 'dailydeal';
        $this->_headerText = Mage::helper('dailydeal')->__('Manage Deals by Day');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('desc');	
        parent::__construct();
        		
        $this->_removeButton('add');
    }

}