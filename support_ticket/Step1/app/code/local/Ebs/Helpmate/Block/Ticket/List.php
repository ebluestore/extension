<?php
class Ebs_Helpmate_Block_Ticket_List extends Mage_Customer_Block_Account_Dashboard
{
    protected $_collection;

    public function count()
    {
        return $this->_collection->getSize();
    }

    public function getItems()
    {
        return $this->_getCollection()->getItems();
    }

    public function getCollection()
    {
        return $this->_getCollection();
    }

    protected function _getCollection()
    {
        $customerId = (int) Mage::getSingleton('customer/session')->getCustomerId();
        if(!$this->_collection /*&& $this->getProductId() */) {
            $this->_collection = Mage::getModel('helpmate/ticket')
                ->getCollection()
                ->addCustomerFilter($customerId)
                ->addStoreFilter(array(0, Mage::app()->getStore()->getId()))
//                ->setorder('created_at','DESC')
                ->load();

            $departments = Mage::getModel('helpmate/department')->getOptionArray();
            $priorities  = Mage::getModel('helpmate/priority')->getOptionArray();
            $statusses   = Mage::getModel('helpmate/status')->getOptionArray();

            foreach ($this->_collection as &$ticket) {
                $ticket->setDepartmentName($departments[$ticket->getDepartmentId()]);
                $ticket->setPriority($priorities[$ticket->getPriority()]);
                $ticket->setStatus($statusses[$ticket->getStatus()]);
            }

        }
        return $this->_collection;
    }

    public function dateFormat($date)
    {
        return $this->formatDate($date, Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
    }
}
