<?php

class Ebs_FileManager_Block_Adminhtml_Catalog_Product_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('productfileGrid');
        $this->setDefaultSort('productfile_id');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        //$this->setSaveParametersInSession(true);
        if ($this->getProduct()) {
            $this->setDefaultFilter(array(
                'in_productfiles' => 1
            ));
        }
    }
    
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in product flag
        if ($column->getId() == 'in_productfiles') {            
            $productfileIds = array_keys($this->getSelectedRelatedProductfiles());
            if (empty($productfileIds)) {
                $productfileIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('productfile_id', array(
                    'in' => $productfileIds
                ));
            } else {
                if ($productfileIds) {
                    $this->getCollection()->addFieldToFilter('productfile_id', array(
                        'nin' => $productfileIds
                    ));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }
    
    
    protected function _prepareLayout()
    {
        $this->setChild('new_button', $this->getLayout()->createBlock('adminhtml/widget_button')->setData(array(
            'label' => Mage::helper('adminhtml')->__('New File'),
            'onclick' => "window.open('" . $this->getUrl('*/*/file', array(
                'pid' => $this->getRequest()->getParam('id')
            )) . "','Gamekings','width=1024,height=650')",
            'class' => 'task'
        )));
        return parent::_prepareLayout();
    }
    
    public function getNewButtonHtml()
    {
        return $this->getChildHtml('new_button');
    }
    
    public function getMainButtonsHtml()
    {
        $html = '';
        if ($this->getFilterVisibility()) {
            $html .= $this->getNewButtonHtml();
            $html .= $this->getResetFilterButtonHtml();
            $html .= $this->getSearchButtonHtml();
            
        }
        return $html;
    }
    
    
    
    protected function _prepareCollection()
    {
        $store_id   = $this->getRequest()->getParam('store');
        $collection = Mage::getModel('ebs_filemanager/productfile')->getCollection()->setStoreId($store_id);
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
        $this->addColumn('in_productfiles', array(
            
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => 'in_productfiles',
            'values' => $this->_getSelectedProductfiles(),
            'align' => 'center',
            'index' => 'productfile_id'
            
        ));
        
        
        $this->addColumn('productfile_id', array(
            'header' => Mage::helper('ebs_filemanager')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'productfile_id'
        ));
        
        $this->addColumn('title', array(
            'header' => Mage::helper('ebs_filemanager')->__('Title'),
            'align' => 'left',
            'index' => 'title'
        ));
        
        $this->addColumn('filename', array(
            'header' => Mage::helper('ebs_filemanager')->__('Filename'),
            'align' => 'left',
            'index' => 'filename'
        ));
        
        $this->addColumn('status', array(
            'header' => Mage::helper('ebs_filemanager')->__('Status'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                1 => 'Enabled',
                2 => 'Disabled'
            )
        ));
        
        
        $this->addColumn('action', array(
            'header' => Mage::helper('ebs_filemanager')->__('Action'),
            'renderer' => 'ebs_filemanager/adminhtml_renderer_edit',
            'index' => 'action'
        ));
        
        $this->addColumn('position', array(
            'header' => Mage::helper('ebs_filemanager')->__('Position'),
            'name' => 'position',
            'type' => 'number',
            'validate_class' => 'validate-number',
            'index' => 'position',
            'width' => 60,
            'editable' => true,
            'edit_only' => true
        ));
        
        return parent::_prepareColumns();
    }
    
    
    public function _getSelectedProductfiles()
    {
        
        $productfiles = array();
        $productfiles = $this->getProductfiles();
        
        
        if (!is_array($productfiles)) {
            $productfiles = array_keys($this->getSelectedRelatedProductfiles());
        }
        
        return $productfiles;
        
        
    }
    
    public function getSelectedRelatedProductfiles()
    {
        $productfileIds = array();
        $productfileIds = $this->getFileIds();
        
        $productfiles = array();
        if (count($productfileIds))
            foreach ($productfileIds as $productfileId) {
                $productfiles[$productfileId] = array(
                    'position' => 1
                );
            }
        return $productfiles;
        
    }
    
    
    public function getFileIds()
    {
        $fileIds    = array();
        $collection = Mage::getModel('ebs_filemanager/fproduct')->getCollection()->addFieldToFilter('product_id', $this->getProduct()->getId());        
        if (count($collection))
            foreach ($collection as $item)
                $fileIds[] = $item->getProductfileId();
        return $fileIds;
        
    }
    
    public function getProduct()
    {
        return Mage::getModel('catalog/product')->load($this->getRequest()->getParam('id'));
        
    }
    
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $row->getId(),
            'store' => $this->getRequest()->getParam('store')
        ));
    }
    
    public function getGridUrl()
    {
        return $this->getData('grid_url') ? $this->getData('grid_url') : $this->getUrl('*/*/productfileGrid', array(
            '_current' => true,
            'id' => $this->getRequest()->getParam('id')
        ));
    }
}

?>