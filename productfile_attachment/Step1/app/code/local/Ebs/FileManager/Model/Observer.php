<?php

class Ebs_FileManager_Model_Observer
{
    public function __construct()
    {
    }

    public function save_productfile($observer)
    {
    
        $product = $observer->getProduct();
        $productId = $product->getId();

        //get productfileId
        $productfiles = Mage::app()->getRequest()->getPost('productfile_productfiles');
        if($productfiles) {
            $productfileIds = array();
            parse_str($productfiles,$productfileIds);
            $productfileIds = array_keys($productfileIds);
        } else {
            $productfileIds = array(0);
            $fproducts = Mage::getModel('ebs_filemanager/fproduct')->getCollection()
                                ->addFieldToFilter('product_id',Mage::app()->getRequest()->getParam('id'));
            if(count($fproducts)) {
                foreach($fproducts as $fproduct) {
                    $productfileIds[] = $fproduct->getProductfileId();
                }    
            }        
        }
    
        if (count($productfileIds)) {
            $productfiles = Mage::getModel('ebs_filemanager/productfile')->getCollection()
                                                ->addFieldToFilter('productfile_id',array('in'=>$productfileIds));
        }
    
        Mage::helper('ebs_filemanager')->assignProductfiles($productfiles,$productId);    
    }
}

?> 