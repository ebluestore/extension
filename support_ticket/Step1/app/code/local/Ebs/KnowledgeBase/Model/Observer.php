<?php
class Ebs_KnowledgeBase_Model_Observer
{
    /**
     *
     * @param Varien_Event_Observer $observer
     * @return Ebs_KnowledgeBase_Model_Observer
     */
    public function beforeFaqDelete(Varien_Event_Observer $observer)
    {
        //foreign key
        $faqId = $observer->getEvent()->getDataObject()->getId();
        $collection = Mage::getModel('knowledgebase/faq_category')->getCollection()
            ->addFaqFilter($faqId);
        foreach ($collection as $row) {
            $row->delete();
        }
        return $this;
    }
}
