<?php


interface Ebs_Totalattributes_Block_Widget_Frontend_ViewInterface
{
    /**
     * @param boolean $isForEdit = false
     *
     * @return mixed
     */
    public function getHtml();

    /**
     * @param Ebs_Totalattributes_Model_Attribute_TypeInterface $type
     *
     * @return Ebs_Totalattributes_Block_Widget_Frontend_ViewAbstract
     */
    public function setTypeModel(Ebs_Totalattributes_Model_Attribute_TypeInterface $type);

    /**
     * @return Ebs_Totalattributes_Model_Attribute_TypeInterface
     */
    public function getTypeModel();

    /**
     * @param mixed $value
     *
     * @return Ebs_Totalattributes_Block_Widget_Frontend_ViewInterface
     */
    public function setValue($value);
}