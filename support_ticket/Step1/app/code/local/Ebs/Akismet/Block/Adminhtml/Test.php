<?php

class Ebs_Akismet_Block_Adminhtml_Test extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) 
    {
        $this->setElement($element);
        return $this->_getWidgetButtonHtml($this->__('Test Api Key'));
    }
    
    protected function _getWidgetButtonHtml($title) 
    {
        $url = Mage::helper('adminhtml')->getUrl('akismet/test/test', array());
        $widgetButton = $this->getLayout()->createBlock('adminhtml/widget_button');
        $widgetButton->setType('button')
            ->setLabel($this->__($title))
            ->setOnClick(sprintf("window.location.href='%s'", $url));
        return $widgetButton->toHtml();
    }
}