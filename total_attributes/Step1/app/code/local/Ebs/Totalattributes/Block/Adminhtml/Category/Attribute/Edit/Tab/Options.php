<?php

class Ebs_Totalattributes_Block_Adminhtml_Category_Attribute_Edit_Tab_Options extends Mage_Eav_Block_Adminhtml_Attribute_Edit_Options_Abstract
{

	public function __construct()
	{
		parent::__construct();
		$this->setTemplate('ebs_totalattributes/category/attribute/options.phtml');
	}
}
