<?php
/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
/**
 * create productlabel table
 */
$installer->run("
-- Create table product_label
DROP TABLE IF EXISTS {$this->getTable('productlabel/productlabel')};
CREATE TABLE {$this->getTable('productlabel/productlabel')} (
    `label_id` int(11) unsigned NOT NULL auto_increment,
    `name` varchar(255) default '',
    `description` text default '',
    `status` smallint(6) NOT NULL default '2',
    `from_date` datetime default NULL,
    `to_date` datetime default NULL,
    `priority` int(11) unsigned default '0',
    `conditions_serialized` mediumtext default NULL,
    `text` varchar(255) NOT NULL default '',
    `image` varchar(255) NOT NULL default '',
    `position` smallint(6) NOT NULL default '1',
    `display` smallint(6) NOT NULL default '0',
    `category_text` varchar(255) NOT NULL default '',
    `category_image` varchar(255) NOT NULL default '',
    `category_position` smallint(6) NOT NULL default '1',
    `category_display` smallint(6) NOT NULL default '0',
    `is_auto_fill` smallint(6) NOT NULL default '1',
    `created_time` datetime NULL,
    `update_time` datetime NULL,
    `condition_selected` varchar(50) NOT NULL,
    `threshold` int(11) unsigned default NULL,
    `is_apply` smallint(6) NOT NULL default '2',
    PRIMARY KEY (`label_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create table product_label_entity
DROP TABLE IF EXISTS {$this->getTable('productlabel/productlabelentity')};
CREATE TABLE {$this->getTable('productlabel/productlabelentity')} (
    `product_label_entity_id` int(11) unsigned NOT NULL auto_increment,
    `same_on_two_page` smallint(6) NOT NULL default '1',
    `text` varchar(255) NOT NULL default '',
    `text_frontend` varchar(255) NOT NULL default '',
    `image` varchar(255) NOT NULL default '',
    `position` smallint(6) NOT NULL default '1',
    `display` smallint(6) NOT NULL default '0',
    `category_text` varchar(255) NOT NULL default '',
    `category_text_frontend` varchar(255) NOT NULL default '',
    `category_image` varchar(255) NOT NULL default '',
    `category_position` smallint(6) NOT NULL default '1',
    `category_display` smallint(6) NOT NULL default '0',
    `created_time` datetime NULL,
    `update_time` datetime NULL,
    `product_id` int(11) unsigned  NOT NULL,
    FOREIGN KEY (`product_id`) REFERENCES {$this->getTable('catalog/product')}  (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (`product_label_entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create table product_label_entity_value cho multistore
DROP TABLE IF EXISTS {$this->getTable('productlabel/productlabelentityvalue')};
CREATE TABLE {$this->getTable('productlabel/productlabelentityvalue')} (
    `value_id` int(10) unsigned NOT NULL auto_increment,
    `product_label_entity_id` int(11) unsigned NOT NULL,
    `store_id` smallint(5) unsigned  NOT NULL,
    `attribute_code` varchar(63) NOT NULL default '',
    `value` text NOT NULL,
    UNIQUE(`product_label_entity_id`,`store_id`,`attribute_code`),
    INDEX (`product_label_entity_id`),
    INDEX (`store_id`),
    FOREIGN KEY (`product_label_entity_id`) REFERENCES {$this->getTable('productlabel/productlabelentity')} (`product_label_entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (`store_id`) REFERENCES {$this->getTable('core/store')} (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (`value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create table product_label_value cho multistore
DROP TABLE IF EXISTS {$this->getTable('productlabel/productlabelvalue')};
CREATE TABLE {$this->getTable('productlabel/productlabelvalue')} (
    `value_id` int(10) unsigned NOT NULL auto_increment,
    `label_id` int(11) unsigned NOT NULL,
    `store_id` smallint(5) unsigned  NOT NULL,
    `attribute_code` varchar(63) NOT NULL default '',
    `value` text NOT NULL,
    UNIQUE(`label_id`,`store_id`,`attribute_code`),
    INDEX (`label_id`),
    INDEX (`store_id`),
    FOREIGN KEY (`label_id`) REFERENCES {$this->getTable('productlabel/productlabel')} (`label_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (`store_id`) REFERENCES {$this->getTable('core/store')} (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (`value_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create table product label flat data
DROP TABLE IF EXISTS {$this->getTable('productlabel/productlabelflatdata')};
CREATE TABLE {$this->getTable('productlabel/productlabelflatdata')} (
    `product_label_flat_data_id` int(11) unsigned NOT NULL auto_increment,
    `text` varchar(255) NOT NULL default '',
    `image` varchar(255) NOT NULL default '',
    `position` smallint(6) NOT NULL default '1',
    `display` smallint(6) NOT NULL default '0',
    `category_text` varchar(255) NOT NULL default '',
    `category_image` varchar(255) NOT NULL default '',
    `category_position` smallint(6) NOT NULL default '1',
    `category_display` smallint(6) NOT NULL default '0',
    `label_id` int(11) unsigned NOT NULL,
    `product_id` int(11) unsigned  NOT NULL,
    `store_id` smallint(5) unsigned NOT NULL DEFAULT '0',
    `priority` int(11) unsigned default '0',
    `from_time` int(11) default 0,
    `to_time` int(11) default 0,
    `update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (`product_id`) REFERENCES {$this->getTable('catalog/product')} (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (`label_id`) REFERENCES {$this->getTable('productlabel/productlabel')} (`label_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (`store_id`) REFERENCES {$this->getTable('core/store')} (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (`product_label_flat_data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$labels = array(
    array('image' => 'saleoff', 'name' => 'SALE OFF'),
    array('image' => 'new', 'name' => 'NEW'),
    array('image' => 'hot', 'name' => 'HOT'),
    array('image' => 'newarrivar', 'name' => 'NEW ARRIVAR'),
    array('image' => 'bigsale', 'name' => 'BIG SALE'),
    array('image' => 'freeship', 'name' => 'FREE SHIP'));


foreach ($labels as $label) {
    $model = Mage::getModel('productlabel/productlabel')->setStoreId(0);
    $data['name'] = $label['name'];
    $data['description'] = 'Product label template ' . $label['name'];
    $data['status'] = 2;
    $data['priority'] = 0;
    $data['display'] = 1;
    $data['text'] = $label['name'];
    $data['image'] = $label['image'] . '_big.png';
    $data['category_display'] = 1;
    $data['category_text'] = $label['name'];
    $data['category_image'] = $label['image'] . '_small.png';
    $data['is_auto_fill'] = 0;
    $data['condition_selected'] = '';
    $data['threshold'] = 10;
    $model->setData($data);
    try {
        $model->save();
    } catch (Exception $exc) {
        echo $exc->getMessage();
    }
}

$installer->endSetup();
