<?php


interface Ebs_Totalattributes_Block_Widget_Backend_PdfInterface
{
    /**
     * @return mixed
     */
    public function getLabel();

    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @param Ebs_Totalattributes_Model_Attribute_TypeInterface $type
     *
     * @return Ebs_Totalattributes_Block_Widget_Backend_PdfAbstract
     */
    public function setTypeModel(Ebs_Totalattributes_Model_Attribute_TypeInterface $type);

    /**
     * @return Ebs_Totalattributes_Model_Attribute_TypeInterface
     */
    public function getTypeModel();

    /**
     * @param mixed $value
     *
     * @return Ebs_Totalattributes_Block_Widget_Backend_PdfInterface
     */
    public function setValue($value);
}