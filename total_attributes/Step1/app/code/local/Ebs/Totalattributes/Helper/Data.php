<?php

class Ebs_Totalattributes_Helper_Data extends Mage_Core_Helper_Data
{
    /**
     * @param string $version
     * @return bool
     */
    public function checkMageVersion($version = '1.4.0.0')
    {
        return version_compare(Mage::getVersion(), $version, '>=');
    }

    /**
     * Retrieve attribute hidden fields
     *
     * @return array
     */
    public function getAttributeHiddenFields()
    {
        if (Mage::registry('attribute_type_hidden_fields')) {
            return Mage::registry('attribute_type_hidden_fields');
        } else {
            return array();
        }
    }

    /**
     * Retrieve attribute disabled types
     *
     * @return array
     */
    public function getAttributeDisabledTypes()
    {
        if (Mage::registry('attribute_type_disabled_types')) {
            return Mage::registry('attribute_type_disabled_types');
        } else {
            return array();
        }
    }
}