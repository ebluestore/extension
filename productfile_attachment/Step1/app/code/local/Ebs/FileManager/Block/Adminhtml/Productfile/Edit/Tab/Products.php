<?php

class Ebs_FileManager_Block_Adminhtml_Productfile_Edit_Tab_Products extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('productfileloadGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
        if ($this->getFile()) {
            $this->setDefaultFilter(array(
                'in_products' => 1
            ));
        }
    }
    
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in product flag
        if ($column->getId() == 'in_products') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array(
                    'in' => $productIds
                ));
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', array(
                        'nin' => $productIds
                    ));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }
    
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
        $this->addColumn('in_products', array(
            
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => 'in_products',
            'values' => $this->_getSelectedProducts(),
            'align' => 'center',
            'index' => 'entity_id'
            
        ));
        
        $this->addColumn('entity_id', array(
            'header' => Mage::helper('ebs_filemanager')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'entity_id'
        ));
        
        $this->addColumn('name', array(
            'header' => Mage::helper('ebs_filemanager')->__('Name'),
            'align' => 'left',
            'index' => 'name'
        ));
        
        $this->addColumn('type', array(
            'header' => Mage::helper('ebs_filemanager')->__('Type'),
            'width' => 100,
            'align' => 'left',
            'index' => 'type_id',
            'type' => 'options',
            'options' => Mage::getSingleton('catalog/product_type')->getOptionArray()
        ));
        
        $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())->load()->toOptionHash();
        
        $this->addColumn('set_name', array(
            'header' => Mage::helper('ebs_filemanager')->__('Attrib. Set Name'),
            'align' => 'left',
            'index' => 'attribute_set_id',
            'type' => 'options',
            'options' => $sets
        ));
        
        $this->addColumn('status', array(
            'header' => Mage::helper('ebs_filemanager')->__('Status'),
            'width' => 90,
            'index' => 'status',
            'type' => 'options',
            'options' => Mage::getSingleton('catalog/product_status')->getOptionArray()
        ));
        
        $this->addColumn('visibility', array(
            'header' => Mage::helper('ebs_filemanager')->__('Visibility'),
            'width' => 90,
            'index' => 'visibility',
            'type' => 'options',
            'options' => Mage::getSingleton('catalog/product_visibility')->getOptionArray()
        ));
        
        $this->addColumn('sku', array(
            'header' => Mage::helper('ebs_filemanager')->__('SKU'),
            'align' => 'left',
            'index' => 'sku'
        ));
        
        $this->addColumn('price', array(
            'header' => Mage::helper('ebs_filemanager')->__('Price'),
            'align' => 'left',
            'type' => 'currency',
            'currency_code' => (String) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'price'
        ));
        
        $this->addColumn('position', array(
            'header' => Mage::helper('ebs_filemanager')->__('Position'),
            'name' => 'position',
            'type' => 'number',
            'validate_class' => 'validate-number',
            'index' => 'position',
            'width' => 60,
            'editable' => true,
            'edit_only' => true
        ));
        
        return parent::_prepareColumns();
    }
    
    public function getRowUrl($row)
    {
        return $this->getUrl('adminhtml/catalog_product/edit', array(
            'id' => $row->getId()
        ));
    }
    
    
    public function getGridUrl()
    {
        return $this->getData('grid_url') ? $this->getData('grid_url') : $this->getUrl('*/*/productGrid', array(
            '_current' => true,
            'id' => $this->getRequest()->getParam('id')
        ));
    }
    
    public function _getSelectedProducts()
    {
        $products = $this->getProducts();
        
        if (!is_array($products)) {
            $products = array_keys($this->getSelectedRelatedProducts());
        }
        return $products;        
    }
    
    public function getSelectedRelatedProducts()
    {
        $productIds = $this->getFile()->getProductIds();
        $products   = array();
        if (count($productIds))
            foreach ($productIds as $productId) {
                $products[$productId] = array(
                    'position' => 1
                );
            }
        return $products;
        
    }
    
    public function getFile()
    {
        return Mage::getModel('ebs_filemanager/productfile')->load($this->getRequest()->getParam('id'));        
    }
    
}

?>