<?php

class Ebs_Totalattributes_Block_Widget_Backend_Pdf_Text
    extends Ebs_Totalattributes_Block_Widget_Backend_PdfAbstract
{
    public function getLabel()
    {
        return $this->_getLabel();
    }

    public function getValue()
    {
        $value = htmlspecialchars($this->_getValue());
        $result = $this->iconvWordwrap($value, 50, "@newline@", true);
        return explode("@newline@", $result);
    }
}