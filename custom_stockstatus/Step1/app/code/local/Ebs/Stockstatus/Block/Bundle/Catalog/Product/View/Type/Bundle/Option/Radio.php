<?php

class Ebs_Stockstatus_Block_Bundle_Catalog_Product_View_Type_Bundle_Option_Radio extends Ebs_Stockstatus_Block_Bundle_Catalog_Product_View_Type_Bundle_Option
{
    public function _construct()
    {
        $this->setTemplate('bundle/catalog/product/view/type/bundle/option/radio.phtml');
    }
}