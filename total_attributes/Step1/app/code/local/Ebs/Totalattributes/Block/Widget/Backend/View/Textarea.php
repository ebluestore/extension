<?php

class Ebs_Totalattributes_Block_Widget_Backend_View_Textarea
    extends Ebs_Totalattributes_Block_Widget_Backend_ViewAbstract
{
    public function getHtml()
    {
        if (!$this->_getValue()) {
            return '';
        }
        $labelHtml = $this->_getLabelHtml();
        $valueHtml = $this->_getValueHtml();
        $attributeCode = $this->_getCode();
        $html = "
            <tr id=\"{$attributeCode}\">
                <td class=\"label\">{$labelHtml}</td>
                <td class=\"value\">{$valueHtml}</td>
            </tr>
        ";
        return $html;
    }

    protected function _getLabelHtml()
    {
        return "<label for=\"{$this->_getCode()}\">{$this->_getLabel()}</label>";
    }

    protected function _getValueHtml()
    {
        $value = nl2br(htmlspecialchars($this->_getValue()));
        return "<strong>{$value}</strong>";
    }
}