<?php

require_once 'Mage/Adminhtml/controllers/Sales/OrderController.php';
class Ebs_Totalattributes_Adminhtml_OrderController extends Mage_Adminhtml_Sales_OrderController
{
    public function indexAction()
    {
        $this->_title($this->__('Order Attributes'))->_title($this->__('Manage Orders'));
        $this->loadLayout();
        $this->_setActiveMenu('sales/ebs_totalattributes');
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    protected function _title($text = null, $resetIfExists = false)
    {
        if (Mage::helper('ebs_totalattributes')->checkMageVersion()) {
            return parent::_title($text, $resetIfExists);
        }
        return $this;
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/ebs_totalattributes/manage_orders');
    }

}