<?php

class Ebs_Akismet_TestController extends Mage_Adminhtml_Controller_Action
{
    public function testAction()
    {
        $akismet = Mage::getSingleton('akismet/akismet');
        $service = $akismet->getService();
        $apiKey = $akismet->getApiKey();
        if ($apiKey && $service->verifyKey($apiKey)) {
            $message = 'Your akismet api key is valid';
            Mage::getSingleton('adminhtml/session')->addSuccess($message);
        } else {
            $message = 'Your akismet api key is not valid';
            Mage::getSingleton('adminhtml/session')->addError($message);
        }
        $this->_redirectReferer();
    }
}