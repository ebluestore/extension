<?php


class Ebs_Totalattributes_Block_Widget_Backend_Grid_Textarea
    extends Ebs_Totalattributes_Block_Widget_Backend_GridAbstract
{
    public function getColumnId()
    {
        return $this->getProperty('code') ? $this->getProperty('code') : null;
    }

    public function getColumnProperties()
    {
        return array(
            'header'   => $this->_getLabel(),
            'index'    => $this->getColumnId(),
        );
    }
}