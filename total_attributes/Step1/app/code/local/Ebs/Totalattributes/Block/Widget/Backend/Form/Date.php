<?php


class Ebs_Totalattributes_Block_Widget_Backend_Form_Date
    extends Ebs_Totalattributes_Block_Widget_Backend_FormAbstract
{
    public function getFieldId()
    {
        return $this->_getCode();
    }

    public function getFieldType()
    {
        return 'date';
    }

    public function getFieldTypeRenderer()
    {
        return null;
    }

    public function getFieldProperties()
    {
        $properties = array(
            'label'        => $this->_getLabel(),
            'title'        => $this->_getLabel(),
            'name'         => $this->_getCode(),
            'image'        => Mage::getDesign()->getSkinUrl('images/grid-cal.gif'),
            'format'       => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'required'     => $this->getProperty('validation_rules/is_required') ? true : false,
            'locale'       => Mage::app()->getLocale()->getLocaleCode(),
        );
        return $properties;
    }

}