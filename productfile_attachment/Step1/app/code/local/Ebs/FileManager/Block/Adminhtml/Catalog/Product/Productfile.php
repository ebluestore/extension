<?php

class Ebs_FileManager_Block_Adminhtml_Catalog_Product_Productfile extends Mage_Adminhtml_Block_Widget_Grid implements Mage_Adminhtml_Block_Widget_Tab_Interface 
{
    public function getTabLabel()    {
        return Mage::helper('ebs_filemanager')->__('Attached Files');
    }

    public function getTabTitle() {
        return Mage::helper('ebs_filemanager')->__('Attached Files');
    }
    
    public function canShowTab()    {
        
            return true;
        
    }
    
    public function isHidden()    {
        
            return false;
    }    
    
    public function getTabClass()
    {
        return 'ajax notloaded';
    }
    
    public function getTabUrl()
    {
        return $this->getUrl('ebs_filemanager/adminhtml_productfile/productfile', array('_current'=>true));
    }
} 

?>