<?php

class Ebs_Totalattributes_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    public function setCollection($collection)
    {
        $collection = Mage::getResourceModel('ebs_totalattributes/sales_order_collection')->addAttributesToOrderCollection();
        return parent::setCollection($collection);
    }

    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        foreach ($this->getOrderAttributeCollection() as $attribute) {
            $gridRenderer = $attribute->unpackData()->getTypeModel()->getBackendGridRenderer();
            $this->addColumnAfter($gridRenderer->getColumnId(), $gridRenderer->getColumnProperties(), 'grand_total');
        }

        $this->addColumn(
            'action',
            array(
                'header'    => $this->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => $this->__('Edit'),
                        'url'       => array('base' => 'adminhtml/sales_order/view'),
                        'field'     => 'order_id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
            )
        );

        $this->sortColumnsByOrder();
        return $this;
    }

    protected function getOrderAttributeCollection()
    {
        return Mage::helper('ebs_totalattributes/order')->getAttributeCollectionForOrderGrid();
    }

    public function removeColumn($columnId)
    {
        if (isset($this->_columns[$columnId])) {
            unset($this->_columns[$columnId]);
        }
        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('adminhtml/sales_order/view', array('order_id' => $row->getId()));
    }
}