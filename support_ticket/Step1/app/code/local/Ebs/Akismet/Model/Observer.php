<?php

class Ebs_Akismet_Model_Observer
{
    public function checkReviewForSpam(Varien_Event_Observer $observer)
    {
        $review = $observer->getEvent()->getObject();
        $akismet = $this->_getAkismet();
        if ($akismet->isActive() && isset($review)) {
            $reviewData = array(
                'name' => $review->getNickname(),
                'comment' => $review->getTitle().' '.$review->getComment()
            );
            if ($akismet->isSpam($reviewData)) {
                throw new Exception('Akismet Spam Detected');
            }
        }
    }
    
    protected function _getAkismet()
    {
        return Mage::getSingleton('akismet/akismet');
    }
}