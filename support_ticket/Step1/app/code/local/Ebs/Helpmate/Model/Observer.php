<?php
class Ebs_Helpmate_Model_Observer
{
    /**
     *
     * @param Mage_Cron_Model_Schedule $schedule
     * @return  Ebs_Helpmate_Model_Observer
     */
    public function sheduledAddEmailedTicket($schedule)
    {
        $departments  = Mage::getModel('helpmate/department')->getCollection()
            ->addActiveFilter()
            ->addGatewayData();

        $statusOpen = Ebs_Helpmate_Model_Status::STATUS_OPEN;
        $priority = Ebs_Helpmate_Model_Priority::PRIORITY_MEDIUM;

        foreach ($departments as $department) {
            $gateway = $department->getGateway();
            if (!$gateway->getStatus()) {
                continue;
            }
            try {
                $storage  = $gateway->getStorage();
                if (!$storage->countMessages()) {
                    continue;
                }

            } catch (Zend_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('helpmate')->__(
                        'Problem connecting to gateway ("%s") "%s" for department "%s".',
                        $gateway->getName(),
                        $e->getMessage(),
                        $department->getName()
                    )
                );
                continue;
            }

            foreach ($storage as $messageNumber => $message) {

                try {

                if ($storage instanceof Zend_Mail_Storage_Imap
                    && $message->hasFlag(Zend_Mail_Storage::FLAG_SEEN)) {

                    continue;
                }

                $ticket = Mage::getModel('helpmate/ticket');
                $theard = Mage::getModel('helpmate/theard');

                $parser = new Ebs_Helpmate_Model_Mail_MessageParser($message);

                $messageId = $parser->getMessageId();
                
                //next message beacouse this message was added before
                if (null !== $messageId) {
                    $theardCollection = $theard->getCollection()
                        ->addMessageIdFilter($messageId);
                    if ($theardCollection->count()) {
                        continue;
                    }
                }
                
                //detect ticketId from In-Reply-To
                $ticketId  = null;
                $inReplyTo = $parser->getInReplyTo();

                if (null !== $inReplyTo) {
                    $theard->getCollection()->addMessageIdFilter($inReplyTo);
                    foreach ($theardCollection as $_theard) {
                        $ticketId = $_theard->getTicketId();
                        break;
                    }
                }
                
                $createdAt    = $parser->getCreatedAt();
                $departmentId = $department->getId();
                $from         = $parser->getFrom();

                
                //detect ticketId from subject
                $subject = $parser->getSubject();
                if (null === $ticketId) {
                    $ticketId = $parser->getTicketIdFromSubject($subject);
                }
                
                if (null !== $ticketId) {
                    $ticket->load($ticketId);
                }
                if (null === $ticket->getId()) {
                    
                    $customerId = $customer = null;

                    $resource = Mage::getSingleton('customer/customer')->getResource();
                    $connection = $resource->getReadConnection();
                    $select = $connection->select()
                        ->from($resource->getEntityTable(), array($resource->getEntityIdField()))
                        ->where('email=:customer_email')
                        ;

                    $customer = Mage::getSingleton('customer/customer')->load(
                        $connection->fetchOne(
                            $select, array('customer_email' => $from)
                        )
                    );

                    if ($customer instanceof Mage_Customer_Model_Customer
                        && $customer->getId()) {

                        $customerId = $customer->getId();
                    }
                    
                    $adminUser = Mage::getModel('helpmate/department_user')
                        ->getCollection()
                        ->addDepartmentFilter($departmentId)
                        ->getFirstItem()
                    ;
                    $userId = null;
                    if ($adminUser) {
                       $userId = $adminUser->getUserId();
                    }
                    
                    $ticket->setCustomerId( $customerId)
                        ->setEmail(         $from)
                        ->setNumber(        )
                        ->setStatus(        $statusOpen)
                        ->setTitle(         $subject)
                        ->setPriority(      $priority)
                        ->setCreatedAt(     $createdAt)
                        ->setModifiedAt(    $createdAt)
                        ->setDepartmentId(  $departmentId)
                        ->setUserId(        $userId)
                        ->setStoreId(       $department->getStoreId())
                        ->setNotes(         'From Department : "' . $department->getName()
                                            . '" Email Gateway : "' . $gateway->getName() . '"')
                        ->save()
                        ;
                    Mage::dispatchEvent('helpmate_notify_customer_ticket_create', array(
                        'ticket'  => $ticket
                    ));
                }

                $ticket->setStatus($statusOpen)
                    ->setModifiedAt($createdAt)
                    ->save();

                $theard->setMessageId( $messageId)
                    ->setTicketId(     $ticket->getId())
                    ->setCreatedAt(    $createdAt)
                    ->setText(         $parser->getContent())
                    ->setFile(         $parser->getFilenames())
                    ->setStatus(       $statusOpen)
                    ->setPriority(     $ticket->getPriority())
                    ->setDepartmentId( $ticket->getDepartmentId())
                    ->save()
                    ;
                Mage::dispatchEvent('helpmate_notify_admin_ticket_change', array(
                    'theard'  => $theard
                ));

            // end foreach storrage messages
                } catch (Zend_Exception $e) {
                    echo $subject . '   ' . $e->getMessage();
                    Mage::getSingleton('adminhtml/session')->addSuccess(
                        $subject . '   ' . $e->getMessage()
                    );
                }
            }
            //REMOVE ALL MESSAGE :(
            for ($i = count($storage); $i; --$i) {
                $storage->removeMessage($i);
            }

        }
        return $this;
    }

    /**
     * send customer email notification about ticket creation
     *
     * @param Varien_Event_Observer $observer
     * @return Ebs_Helpmate_Model_Observer
     */
    public function notifyCustomerTicketCreate(Varien_Event_Observer $observer)
    {

        if (true != Mage::getStoreConfig('helpmate/email/enableCustomerNotification')) {
            return $this;
        }

        $ticket     = $observer->getEvent()->getTicket();
        $department = $ticket->getDepartment();
        $gateway    = $department->getGateway();

        /* @var $mailTemplate Mage_Core_Model_Email_Template */
        $mailTemplate = Mage::getModel('core/email_template');

        $emailTemplateId = $department->getEmailTemplateNew();
        if (0 == $emailTemplateId) {
            $emailTemplateId = Mage::getStoreConfig('helpmate/email/ticket_notification');
        }

        $name = $ticket->getEmail();
        if (null !== $ticket->getCustomerId()) {
            $name = Mage::getModel('customer/customer')
                ->load($ticket->getCustomerId())
                ->getName();
        }

        $vars = new Varien_Object(array(
            'name'   => $name,
            'id'     => 1000000 + $ticket->getId(),
            'number' => $ticket->getNumber(),
            'ticket' => $ticket->getTitle()
        ));

        $mailTemplate->setDesignConfig(array(
                'area' => 'frontend',
                'store' => Mage::app()->getStore($ticket->getStoreId())
            ))
            ->sendTransactional(
                    $emailTemplateId,
                    $department->getSender(),
                    $ticket->getEmail(),
                    $name,
                    array('vars' => $vars)
        );

        if (!$mailTemplate->getSentSuccess()) {
            throw new Mage_Core_Exception('mail not send ' . __METHOD__);
        }

        return $this;
    }

    /**
     * send admin(s) email notification about ticket creation
     *
     * @param Varien_Event_Observer $observer
     * @return Ebs_Helpmate_Model_Observer
     */
    public function notifyAdminTicketChange(Varien_Event_Observer $observer)
    {
        if (true != Mage::getStoreConfig('helpmate/email/enableAdminNotification')) {
            return $this;
        }

        $theard     = $observer->getEvent()->getTheard();
        $ticket     = $theard->getTicket();
        $department = $ticket->getDepartment();
        $gateway    = $department->getGateway();

        /* @var $mailTemplate Mage_Core_Model_Email_Template */
        $mailTemplate = Mage::getModel('core/email_template');

        $emailTemplateId = $department->getEmailTemplateAdmin();
        if (0 == $emailTemplateId) {
            $emailTemplateId = Mage::getStoreConfig('helpmate/email/theard_notification');
        }

        $departmentUser = Mage::getModel('helpmate/department_user');
        $departmentId = $department->getId();
        $users = $departmentUser->getCollection()
            ->addDepartmentFilter($departmentId)
            ->addEmailData()
            ;
        $name = $ticket->getEmail();
        if (null !== $ticket->getCustomerId()) {
            $name = Mage::getModel('customer/customer')
                ->load($ticket->getCustomerId())
                ->getName();
        }
        list($url) = explode('/key/', Mage::helper('adminhtml')->getUrl(
            'helpmate/adminhtml_ticket/edit/', array('id' => $ticket->getId())
        ));
        $vars = new Varien_Object(array(
            'name'   => $name,
            'id'     => 1000000 + $ticket->getId(),
            'number' => $ticket->getNumber(),
            'url'    => $url,
            'ticket' => $ticket->getTitle(),
            'theard' => $theard->getText()
        ));
        foreach ($users as $user) {
            $mailTemplate->setDesignConfig(array(
                    'area' => 'frontend',
                    'store' => Mage::app()->getStore($ticket->getStoreId())
                ))
                ->sendTransactional(
                    $emailTemplateId,
                    $department->getSender(),
                    $user->getEmail(),
                    $name,
                    array('vars' => $vars)
                );

            if (!$mailTemplate->getSentSuccess()) {
                throw new Mage_Core_Exception('mail not send ' . __METHOD__);
            }
        }
        return $this;
    }

    /**
     * send answer for customer
     *
     * @param Varien_Event_Observer $observer
     * @return Ebs_Helpmate_Model_Observer
     */
    public function sendTicketAnswer(Varien_Event_Observer $observer)
    {
        $theard     = $observer->getEvent()->getTheard();
        $ticket     = $theard->getTicket();
        $department = $ticket->getDepartment();
        $gateway    = $department->getGateway();

        $mailTemplate = Mage::getModel('core/email_template');
        /* @var $mailTemplate Mage_Core_Model_Email_Template */

        $messageId = $ticket->getLastMessageId();
        if (false === empty($messageId)) {
            $mailTemplate->getMail()->addHeader('In-Reply-To', '<' . $messageId . '>');
        }
        //
        $newMessageId = $mailTemplate->getMail()->createMessageId();
        $mailTemplate->getMail()->setMessageId('<' . $newMessageId . '>');

        $emailTemplateId = $department->getEmailTemplateAnswer();
        if (0 == $emailTemplateId) {
            $emailTemplateId = Mage::getStoreConfig('helpmate/email/ticket_answer');
        }
        $name = $ticket->getEmail();
        if (null !== $ticket->getCustomerId()) {
            $name = Mage::getModel('customer/customer')
                ->load($ticket->getCustomerId())
                ->getName();
        }
        $vars = new Varien_Object(array(
            'name'   => $name,
            'id'     => 1000000 + $ticket->getId(),
            'number' => $ticket->getNumber(),
            'ticket' => $ticket->getTitle(),
            'theard' => $theard->getText()
        ));
        $file = $theard->getFile();
        if (!empty($file)) {
            $path = Mage::getBaseUrl('media') . 'helpmate' . DS;
            $files = array_filter(explode(';', $file));
            foreach ($files as &$file) {
                $file = '<p><a href="' . $path . $file . '" style="color:#1E7EC8;">' .
                    $file .
                '</a></p>';
            }
            $file = implode('', $files);
        }
        $vars['file'] = $file;

        $mailTemplate->setDesignConfig(array(
                'area' => 'frontend',
                'store' => Mage::app()->getStore($ticket->getStoreId())
            ))
            ->setReplyTo($gateway->getEmail())
            ->sendTransactional(
                $emailTemplateId,
                $department->getSender(),
                $ticket->getEmail(),
                $name,
                array('vars' => $vars)
        );

        if (!$mailTemplate->getSentSuccess()) {
            throw new Mage_Core_Exception('mail not send '  . __METHOD__);
        }
        
        $theard->setMessageId($newMessageId)
            ->save();

        return $this;
    }

    /**
     *
     * @param Mage_Cron_Model_Schedule $schedule
     * @return Ebs_Helpmate_Model_Observer
     */
    public function autoCloseTicketAfterXDay($schedule)
    {
        $xDays = Mage::getStoreConfig('helpmate/general/autoCloseTicketAfterXDay');
        $time = Zend_Date::now()->subDay($xDays);

        $collection = Mage::getModel('helpmate/ticket')->getCollection()
            ->addStatusFilter(Ebs_Helpmate_Model_Status::STATUS_REPLIED)
            ->addLessModifiedAtFilter($time)
            ;

        /* @var $mailTemplate Mage_Core_Model_Email_Template */
        $mailTemplate = Mage::getModel('core/email_template');

        $emailTemplateId = Mage::getStoreConfig('helpmate/email/ticket_autoclose');

        foreach ($collection as $ticket) {
            $notes = $ticket->getNotes();
            $ticket->setStatus(Ebs_Helpmate_Model_Status::STATUS_CLOSE)
                ->setNotes($notes . " Auto close ticket after {$xDays} days")
                ->save()
                    ;

            // send mail
            $department = $ticket->getDepartment();
            $gateway    = $department->getGateway();

            $name = $ticket->getEmail();
            if (null !== $ticket->getCustomerId()) {
                $name = Mage::getModel('customer/customer')
                    ->load($ticket->getCustomerId())
                    ->getName();
            }
            $vars = new Varien_Object(array(
                'name'   => $name,
                'id'     => 1000000 + $ticket->getId(),
                'number' => $ticket->getNumber(),
                'ticket' => $ticket->getTitle(),
                'x_day'  => $xDays
            ));

            $mailTemplate->setDesignConfig(array(
                    'area' => 'frontend',
                    'store' => Mage::app()->getStore($ticket->getStoreId())
                ))
                ->sendTransactional(
                    $emailTemplateId,
                    $department->getSender(),
                    $ticket->getEmail(),
                    $name,
                    array('vars' => $vars)
                );
            if (!$mailTemplate->getSentSuccess()) {
                Mage::log('mail not send '  . __METHOD__, null, 'email.log');               
            }
        }

        return $this;
    }

    /**
     *
     * @param Varien_Event_Observer $observer
     * @return Ebs_Helpmate_Model_Observer
     */
    public function ticketUserChange(Varien_Event_Observer $observer)
    {
        $ticketId = $observer->getEvent()->getTicketId();
        $ticket   = Mage::getModel('helpmate/ticket')->load($ticketId);

        /* @var $mailTemplate Mage_Core_Model_Email_Template */
        $mailTemplate = Mage::getModel('core/email_template');

        $emailTemplateId = Mage::getStoreConfig('helpmate/email/ticket_assigned');

        // send mail
        $department = $ticket->getDepartment();
        list($url) = explode('/key/', Mage::helper('adminhtml')->getUrl(
            'helpmate/adminhtml_ticket/edit/', array('id' => $ticket->getId())
        ));
        $vars = new Varien_Object(array(
            'id'     => 1000000 + $ticket->getId(),
            'number' => $ticket->getNumber(),
            'url'    => $url,
            'ticket' => $ticket->getTitle()
        ));

        $user = $ticket->getAssignedUser();
        $mailTemplate->setDesignConfig(array(
                'area' => 'frontend',
                'store' => Mage::app()->getStore($ticket->getStoreId())
            ))
            ->sendTransactional(
                $emailTemplateId,
                $department->getSender(),
                $user->getEmail(),
                $user->getName(),
                array('vars' => $vars)
            );

        if (!$mailTemplate->getSentSuccess()) {
            throw new Mage_Core_Exception('mail not send '  . __METHOD__);
        }

        return $this;
    }


    /**
     *
     * @param Varien_Event_Observer $observer
     * @return Ebs_Helpmate_Model_Observer
     */
    public function addCreateTicketButton(Varien_Event_Observer $observer)
    {
        $block = $observer->getEvent()->getBlock();
        if ($block instanceof Mage_Adminhtml_Block_Customer_Edit) {
            $url = Mage::getModel('adminhtml/url')->getUrl(
                'helpmate_admin/adminhtml_ticket/new',
                array('customer_id' => $block->getCustomerId())
            );
            $block->addButton('ticket', array(
                'label' => Mage::helper('helpmate')->__('Create Ticket'),
                'onclick' => 'setLocation(\'' . $url . '\')',
                'class' => 'add',
            ), 0, 11);
        }

        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
            $params = array('order_id' => $block->getOrderId());
            $customerId = $block->getOrder()->getCustomerId();
            if (!empty($customerId)) {
                $params['customer_id'] = $customerId;
            }
            $url = Mage::getModel('adminhtml/url')->getUrl(
                'helpmate_admin/adminhtml_ticket/new',
                $params
            );
            $block->addButton('ticket', array(
                'label' => Mage::helper('helpmate')->__('Create Ticket'),
                'onclick' => 'setLocation(\'' . $url . '\')',
                'class' => 'add',
            ), 0, 11);
        }
    }

    /**
     * Check Captcha On Ticket Save
     *
     * @param Varien_Event_Observer $observer
     * @return Mage_Captcha_Model_Observer
     */
    public function checkCaptchaOnTicketSave($observer)
    {
        $formId = 'helpmate_ticket_form';
        $captchaModel = Mage::helper('captcha')->getCaptcha($formId);
        if ($captchaModel->isRequired()) {
            $controller = $observer->getControllerAction();
            $captchaParams = $controller->getRequest()->getPost(
                Mage_Captcha_Helper_Data::INPUT_NAME_FIELD_VALUE
            );
            if (!$captchaModel->isCorrect($captchaParams[$formId])) {
                Mage::getSingleton('core/session')->addError(
                    Mage::helper('captcha')->__('Incorrect CAPTCHA.')
                );
                $controller->setFlag(
                    '', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true
                );
                $controller->getResponse()->setRedirect(
                    Mage::getUrl('*/*/index')
                );
            }
        }
        return $this;
    }
}
