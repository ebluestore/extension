<?php

class Ebs_Totalattributes_Block_Sales_Order_Print extends Mage_Sales_Block_Order_Print
{
    protected $_mockOrder = null;

    public function getOrder()
    {
        if (is_null($this->_mockOrder)) {
            $realOrderObject =  Mage::registry('current_order');
            $this->_mockOrder = Mage::getModel('ebs_totalattributes/sales_order')->setRealOrder($realOrderObject);
        }
        return $this->_mockOrder;
    }

    public function getPaymentInfoHtml()
    {
        $paymentInfoHtml = parent::getPaymentInfoHtml();
        $attributesBlock = Mage::app()->getLayout()->getBlock("oa.payment.method.attributes");
        if ($attributesBlock) {
            $paymentInfoHtml .= $attributesBlock->toHtml();
        }
        return $paymentInfoHtml;
    }

    /**
     * @param mixed $data
     * @param null $allowedTags
     * @return mixed|string
     */
    public function escapeHtml($data, $allowedTags = null)
    {
        return $data;
    }
}