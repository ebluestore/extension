<?php

class Ebs_Totalattributes_Block_Widget_Frontend_View_Date
    extends Ebs_Totalattributes_Block_Widget_Frontend_ViewAbstract
{
    public function getHtml()
    {
        if (is_null($this->_getValue())) {
            return '';
        }
        $labelHtml = $this->_getLabelHtml();
        $valueHtml = $this->_getValueHtml();
        $attributeCode = $this->_getCode();
        $html = "
            <tr id=\"{$attributeCode}\">
                <td class=\"label\">{$labelHtml}</td>
                <td class=\"value\">{$valueHtml}</td>
            </tr>
        ";
        return $html;
    }

    protected function _getLabelHtml()
    {
        return "<label for=\"{$this->_getCode()}\">{$this->_getLabel()}</label>";
    }

    protected function _getValueHtml()
    {
        $dateHtml = $this->_getValue();;
        if (!is_null($dateHtml)) {
            $format = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_FULL);
            $dateHtml = $dateHtml->toString($format);
        } else {
            return '';
        }
        return "<strong>{$dateHtml}</strong>";
    }

    protected function _getValue()
    {
        if (is_null($this->_value)) {
            $value = $this->getProperty('default_value');
            if (!is_null($value) && strlen(trim($value)) > 0) {
                try {
                    return Mage::app()->getLocale()->date($value, Zend_Date::ISO_8601);
                } catch(Exception $e) {
                    return $value;
                }
            } else {
                return null;
            }
        }
        return Mage::app()->getLocale()->date($this->_value, Zend_Date::ISO_8601);
    }
}