<?php

class Ebs_Totalattributes_Block_Sales_Order_Print_Shipment extends Mage_Sales_Block_Order_Print_Shipment
{
    protected $_mockOrder = null;

    public function getOrder()
    {
        if (is_null($this->_mockOrder)) {
            $realOrderObject =  Mage::registry('current_order');
            $this->_mockOrder = Mage::getModel('ebs_totalattributes/sales_order')->setRealOrder($realOrderObject);
        }
        return $this->_mockOrder;
    }

    public function getPaymentInfoHtml()
    {
        $paymentInfoHtml = parent::getPaymentInfoHtml();
        $attributesBlock = Mage::app()->getLayout()->getBlock("oa.payment.method.attributes");
        if ($attributesBlock) {
            $paymentInfoHtml .= $attributesBlock->toHtml();
        }
        return $paymentInfoHtml;
    }

    public function getShipmentAddressFormattedHtml($shipment)
    {
        $shippingAddress = $shipment->getShippingAddress();
        if(!($shippingAddress instanceof Mage_Sales_Model_Order_Address)) {
            return '';
        }
        $mockShippingAddress = Mage::getModel('ebs_totalattributes/sales_order_address');
        $mockShippingAddress->setRealAddress($shippingAddress);
        return $mockShippingAddress->format('html');
    }

    public function getBillingAddressFormattedHtml($order)
    {
        $billingAddress = $order->getRealOrder()->getBillingAddress();
        if(!($billingAddress instanceof Mage_Sales_Model_Order_Address)) {
            return '';
        }
        $mockBillingAddress = Mage::getModel('ebs_totalattributes/sales_order_address');
        $mockBillingAddress->setRealAddress($billingAddress);
        return $mockBillingAddress->format('html');
    }

    /**
     * @param mixed $data
     * @param null $allowedTags
     * @return mixed|string
     */
    public function escapeHtml($data, $allowedTags = null)
    {
        return $data;
    }
}