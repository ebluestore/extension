<?php

class Ebs_Totalattributes_Block_Sales_Order_View_Attributes extends Mage_Adminhtml_Block_Template
{
    public $_valueData = null;

    /**
     * @return array
     */
    public function getLinkedAttributes()
    {
        $values = array();
        foreach ($this->getAttributeCollection() as $attribute) {
            $value = $this->getValueByAttributeId($attribute->getId());
            if (!is_null($value)) {
                $values[] = $attribute;
            }
        }
        return $values;
    }

    public function getAttributeCollection()
    {
        $attributeCollection = Mage::helper('ebs_totalattributes/order')->getAttributeCollectionForCustomerAccount();
        switch ((int)$this->getShowInBlock()) {
            case Ebs_Totalattributes_Model_Source_Showinblock::BILLING_ADDRESS_BLOCK_CODE:
                $attributeCollection->addShowInBillingAddressBlockFilter();
                break;
            case Ebs_Totalattributes_Model_Source_Showinblock::SHIPPING_ADDRESS_BLOCK_CODE:
                $attributeCollection->addShowInShippingAddressBlockFilter();
                break;
            case Ebs_Totalattributes_Model_Source_Showinblock::SHIPPING_METHOD_BLOCK_CODE:
                $attributeCollection->addShowInShippingMethodBlockFilter();
                break;
            case Ebs_Totalattributes_Model_Source_Showinblock::PAYMENT_METHOD_BLOCK_CODE:
                $attributeCollection->addShowInPaymentMethodBlockFilter();
                break;
            case Ebs_Totalattributes_Model_Source_Showinblock::ORDER_REVIEW_BLOCK_CODE:
                $attributeCollection->addShowInOrderReviewBlockFilter();
                break;
            default:
                return null;
        }
        return $attributeCollection;
    }

    public function getValueByAttributeId($attributeId)
    {
        if (is_null($this->_valueData)) {
            $this->_valueData = array();
            $collection = Mage::helper('ebs_totalattributes/order')->getAttributeValueCollectionForQuote($this->getOrder()->getQuoteId());
            foreach ($collection as $item) {
                $this->_valueData[$item->getData('attribute_id')] = $item->getData('value');
            }
        }
        return isset($this->_valueData[$attributeId]) ? $this->_valueData[$attributeId] : null;
    }

    public function getOrder()
    {
        if (Mage::registry('current_order')) {
            return Mage::registry('current_order');
        } elseif (Mage::registry('current_invoice')) {
            return Mage::registry('current_invoice')->getOrder();
        } elseif (Mage::registry('current_shipment')) {
            return Mage::registry('current_shipment')->getOrder();
        } elseif (Mage::registry('current_creditmemo')) {
            return Mage::registry('current_creditmemo')->getOrder();
        }
        return null;
    }
}