<?php


class Ebs_Totalattributes_Block_Widget_Backend_Form_Multipleselect
    extends Ebs_Totalattributes_Block_Widget_Backend_FormAbstract
{
    public function getFieldId()
    {
        return $this->_getCode();
    }

    public function getFieldType()
    {
        return 'multiselect';
    }

    public function getFieldTypeRenderer()
    {
        return null;
    }

    public function getFieldProperties()
    {
        $properties = array(
            'label'    => $this->_getLabel(),
            'name'     => $this->_getCode(),
            'values'   => $this->_getOptionsForSelect(),
            'required' => $this->getProperty('validation_rules/is_required') ? true : false,
        );
        return $properties;
    }

    private function _getOptionsForSelect()
    {
        if (is_null($this->getTypeModel())) {
            return null;
        }
        $attribute = $this->getTypeModel()->getAttribute();
        if (is_null($attribute)) {
            return null;
        }
        $storeId = Mage::app()->getStore()->getId();
        return Mage::helper('ebs_totalattributes/options')
            ->getOptionsForAttributeByStoreIdAsArray($attribute, $storeId, false);
    }
}