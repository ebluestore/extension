<?php


class Ebs_Totalattributes_Block_Widget_Backend_Grid_Yesno
    extends Ebs_Totalattributes_Block_Widget_Backend_GridAbstract
{
    public function getColumnId()
    {
        return $this->getProperty('code') ? $this->getProperty('code') : null;
    }

    public function getColumnProperties()
    {
        return array(
            'header'  => $this->_getLabel(),
            'index'   => $this->getColumnId(),
            'width'   => 60,
            'type'    => 'options',
            'options' => Mage::helper('ebs_totalattributes/options')->getOptionsForYesnoAttribute(false, false)
        );
    }
}