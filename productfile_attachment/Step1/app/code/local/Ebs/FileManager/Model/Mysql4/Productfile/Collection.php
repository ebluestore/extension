<?php

class Ebs_FileManager_Model_Mysql4_Productfile_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected $_store_id = null;
    protected $_addedTable = array();
    
    
    public function _construct()
    {
        parent::_construct();        
        if ($storeId = Mage::app()->getStore()->getId()) {
            $this->setStoreId($storeId);
        }        
        $this->_init('ebs_filemanager/productfile');
    }
    
    public function setStoreId($value){
        $this->_store_id = $value;
        return $this;
    }
    
    public function getStoreId(){
        return $this->_store_id;
    }
    
    protected function _afterLoad(){
        parent::_afterLoad();        
        if ($storeId = $this->getStoreId()) {
            foreach ($this->_items as $item){
                $item->setStoreId($storeId)->loadProductFileValue();
            }
        }
        return $this;
    }
    
    public function addFieldToFilter($field, $condition=null) {
        $attributes = array(
            'title',            
            'content',           
            'status',
        );
        $storeId = $this->getStoreId();
        if (in_array($field, $attributes) && $storeId) {
            if (!in_array($field, $this->_addedTable)) {
                $this->getSelect()
                    ->joinLeft(array($field => $this->getTable('ebs_filemanager/value')),
                        "main_table.productfile_id = $field.productfile_id" .
                        " AND $field.store_id = $storeId" .
                        " AND $field.attribute_code = '$field'",
                        array()
                    );
                $this->_addedTable[] = $field;
            }
            return parent::addFieldToFilter("IF($field.value IS NULL, main_table.$field, $field.value)", $condition);
        }
        if ($field == 'productfile_id') {
            $field = 'main_table.productfile_id';
        }
        return parent::addFieldToFilter($field, $condition);
    }
} 

?>