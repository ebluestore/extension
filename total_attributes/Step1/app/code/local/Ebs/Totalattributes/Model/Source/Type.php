<?php

class Ebs_Totalattributes_Model_Source_Type extends Mage_Adminhtml_Model_System_Config_Source_Yesno
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return Mage::helper('ebs_totalattributes/type')->toOptionArray();
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return Mage::helper('ebs_totalattributes/type')->toArray();
    }

    /**
     * @param string $code
     *
     * @return Ebs_Totalattributes_Model_Attribute_TypeInterface|null
     */
    public function getModelByTypeCode($code)
    {
        return Mage::helper('ebs_totalattributes/type')->getModelByTypeCode($code);
    }
}