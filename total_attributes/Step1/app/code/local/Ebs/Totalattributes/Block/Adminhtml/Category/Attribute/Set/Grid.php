<?php

class Ebs_Totalattributes_Block_Adminhtml_Category_Attribute_Set_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

	public function __construct()
	{
		parent::__construct();
		$this->setId('categorySetGrid');
		$this->setDefaultSort('set_id');
		$this->setDefaultDir('asc');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getResourceModel('eav/entity_attribute_set_collection')
				->setEntityTypeFilter(Mage::registry('entityType'));

		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{

		$this->addColumn('set_name', array(
			'header'    => Mage::helper('ebs_totalattributes')->__('Group Name'),
			'align'     => 'left',
			'sortable'  => true,
			'index'     => 'attribute_set_name',
		));
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/edit', array('id'=>$row->getAttributeSetId()));
	}
}
