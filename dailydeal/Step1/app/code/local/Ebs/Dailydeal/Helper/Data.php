<?php

class Ebs_Dailydeal_Helper_Data extends Mage_Core_Helper_Abstract
{
    /*
     * date formats
     */

    const DATE_PART = "yyyy-MM-dd";
    const DATETIME_PART = "yyyy-MM-dd HH:mm:ss zzzz";
    const DATETIME_NO_TZ_PART = "yyyy-MM-dd HH:mm:ss";
    const DATETIME_JS_PART = "MMM dd yyyy HH:mm:ss zzz";


    public static function IsEnterpriseEdition()
    {
        $modules = Mage::getConfig()->getNode('modules')->children();
        $modulesArray = (array) $modules;
        return isset($modulesArray['Enterprise_Enterprise']);
    }

    public static function GetProductImage($product, $small)
    {
        if ($small)
            return Mage::getBaseUrl('media') . 'catalog/product' . $product->getSmallImage();
        else
            return Mage::getBaseUrl('media') . 'catalog/product' . $product->getImage();

        // TODO: this code is probably better though it produces temporary files:
        $product = Mage::getModel('catalog/product')->load($item->getProductId());
        return (string) Mage::helper('catalog/image')->init($product, 'thumbnail', $product->getSmallImage())->resize(50);
    }

    public static function LogError($ex)
    {
        Mage::log($ex, null, 'deal_error.log');
    }

    /**
     * Returns a string representation of a date converted to store timezone and store Locale
     *
     * When sending strings to this method they should always be formatted according to self::DATETIME_PART !
     *
     * @param   string|integer|Zend_Date|array|null $dateTime date in UTC
     * @return  string
     */
    public static function DateTimeToStoreTZ($dateTime = null)
    {
        return Mage::app()->getLocale()->date($dateTime, self::DATETIME_PART, null, true);
        // This method (storeDate())does not accept datetime part and assumes something like YYYY-DD-MM hh:mm:ss which
        // is bad for us. date() is better because does the same work and allows us to supply our self::DATETIME_PART.
        //return Mage::app()->getLocale()->storeDate(null, $dateTime, true);
    }

    public static function GetLocaleCode()
    {
        try {
            return Mage::app()->getLocale()->getLocaleCode();
        } catch (Exception $ex) {
            self::LogError($ex);
        }
        return "";
    }

    public static function GetCurrencyCodeHtml()
    {
        return '[' . Mage::app()->getStore()->getBaseCurrency()->getCode() . ']';
    }

    public static function GetBaseCurrencySymbol()
    {
        try {
            $base_currency_code = self::GetBaseCurrencyCode();
            $base_currency = Mage::app()->getLocale()->currency($base_currency_code);
            return $base_currency->getSymbol($base_currency_code, self::GetLocaleCode());
        } catch (Exception $ex) {
            self::LogError($ex);
            return Mage::app()->getStore()->getBaseCurrencyCode();
        }
    }

    public static function GetBaseCurrencyCode()
    {
        return Mage::app()->getStore()->getBaseCurrencyCode();
    }

    /**
     * Return template for setTemplate function in layout to set root (1column,2column or 3column)
     *
     * @return string
     */
    public function chooseColumnLayout()
    {
        $columnNumber = Mage::getStoreConfig('dailydeal/general/deallayout');
        switch ($columnNumber) {
            case 'empty':
                return 'page/empty.phtml';
                break;
            case 'one_column':
                return 'page/1column.phtml';
                break;
            case 'two_columns_left':
                return 'page/2columns-left.phtml';
                break;
            case 'two_columns_right':
                return 'page/2columns-right.phtml';
                break;
            case 'three_columns':
                return 'page/3columns.phtml';
                break;
            default:
                return 'page/3columns.phtml';
                break;
        }
    }

    public static function json_decode($data, $as_array = false)
    {
        if (function_exists('json_decode'))
            return Mage::helper('core')->jsonDecode($data, $as_array);
        return null;
    }

    public static function json_encode($data)
    {
        if (function_exists('json_encode'))
            return Mage::helper('core')->jsonEncode($data);
        return null;
    }

    public function getRewriteUrl($id_path)
    {
        $url = Mage::getModel('core/url_rewrite')->loadByIdPath($id_path);
        if ($url) {
            return $url->getRequestPath();
        } else {
            return "";
        }
    }

    public static function reIndexArray(array &$array)
    {
        $temp = $array;
        $array = array();
        foreach ($temp as $key => $value) {
            $array[] = $value;
        }
    }

    /**
     * Get url follow http or https
     */
    public function getUrlHttp($url = '', $rewrite_url = false){
        if($rewrite_url == true){
            $url = $this->getRewriteUrl($url);
        }else{
            
        }
        $link = Mage::getUrl($url,array('_secure'=>Mage::app()->getFrontController()->getRequest()->isSecure()));
        return $link;
    }
    
    /**
     * Render html countdown for category
     */
    public static function categoryDealInfo($product)
    {
        if (!Mage::helper('dailydeal/dailydeal')->isModuleOutputEnabled()) {
            return;
        }

        $layout = Mage::getSingleton('core/layout');
        $block = $layout->createBlock('dailydeal/deal')
                ->setData('product', $product)
                ->setTemplate('dailydeal/catalog/product/countdown.phtml');
        return $block->renderView();
    }

    /**
     * Render html countdown for product detail
     */
    public static function productDealInfo($product)
    {

        if (!Mage::helper('dailydeal/dailydeal')->isModuleOutputEnabled()) {
            return;
        }

        $layout = Mage::getSingleton('core/layout');
        $block = $layout->createBlock('dailydeal/deal')
                ->setData('product', $product)
                ->setTemplate('dailydeal/catalog/product/view_countdown.phtml');
        return $block->renderView();
    }
    
    public static function renderDealQtyOnProductPage($qty){
        $value = self::getConfigDealQtyOnProductPage();
        $search = array('{{qty}}');
        $replace = array($qty);
        $html = str_replace($search, $replace, $value);
        echo $html;
    }
    
    public static function renderDealQtyOnCatalogPage($qty){
        $value = self::getConfigDealQtyOnCatalogPage();
        $search = array('{{qty}}');
        $replace = array($qty);
        $html = str_replace($search, $replace, $value);
        echo $html;
    }
    
    /**
     * Render html countdown for category
     */
    public static function showImage($product)
    {
        if (!Mage::helper('dailydeal/dailydeal')->isModuleOutputEnabled()) {
            return;
        }

        $layout = Mage::getSingleton('core/layout');
        $block = $layout->createBlock('dailydeal/deal')
                ->setData('product', $product)
                ->setTemplate('dailydeal/catalog/product/show_image.phtml');
        return $block->renderView();
    }

    public static function getConfigDisplayQuantity($store_id = "")
    {
        return Mage::getStoreConfig('dailydeal/general/showqty', $store_id);
    }

    public static function getConfigSendMailAdminNotification($store_id = "")
    {
        return Mage::getStoreConfig('dailydeal/global_variable/send_mail_admin_notification', $store_id);
    }

    public static function getConfigAllowSendAdminMail($store_id = "")
    {
        return Mage::getStoreConfig('dailydeal/admin_notification/notify_admin', $store_id);
    }

    public static function getConfigAdminMail($store_id = "")
    {
        return Mage::getStoreConfig('dailydeal/admin_notification/admin_email', $store_id);
    }

    public static function getConfigTemplateIdNoDeal($store_id = "")
    {
        return Mage::getStoreConfig('dailydeal/global_variable/template_id_no_deal', $store_id);
    }

    public static function getConfigIsShowImageCatalogList($store_id = "")
    {
        return Mage::getStoreConfig('dailydeal/general/catalog_list_show_image', $store_id);
    }

    public static function getConfigTodayDealShowDetail($store_id = "")
    {
        return Mage::getStoreConfig('dailydeal/general/today_deal_show_detail', $store_id);
    }

    public static function getConfigProductViewCountdown($store_id = "")
    {
        return Mage::getStoreConfig('dailydeal/general/product_view_show_countdown', $store_id);
    }

    public static function getConfigProductListCountdown($store_id = "")
    {
        return Mage::getStoreConfig('dailydeal/general/product_list_show_countdown', $store_id);
    }
    
    public static function getConfigSortBy($store_id = "")
    {
        return Mage::getStoreConfig('dailydeal/general/sort_by', $store_id);
    }
    
    public static function getConfigDealQtyOnProductPage($store_id = ""){
        return Mage::getStoreConfig('dailydeal/general/deal_qty_on_product_page', $store_id);
    }
    
    public static function getConfigDealQtyOnCatalogPage($store_id = ""){
        return Mage::getStoreConfig('dailydeal/general/deal_qty_on_catalog_page', $store_id);
    }

    public static function getConfigShowSubscription($store_id = ""){
        return Mage::getStoreConfig('dailydeal/deal_subscriber/enable_subscription', $store_id);
    }

    public static function setConfigSendMailAdminNotification($value = "")
    {
        $path = 'dailydeal/global_variable/send_mail_admin_notification';
        Mage::getModel('core/config_data')
                ->load($path, 'path')
                ->setValue($value)
                ->setPath($path)
                ->save();
    }
}