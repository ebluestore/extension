<?php

class Ebs_Totalattributes_Block_Adminhtml_Attribute_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('ebs_totalattributesGrid');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('ebs_totalattributes/attribute_collection')
            ->joinAttributeLabelsForStore()
        ;
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('attribute_id', array(
            'header'        => $this->__('ID'),
            'index'         => 'attribute_id',
            'type'          => 'number',
            'width'         => '20px',
            'filter_index'  => 'main_table.attribute_id'
        ));

        $this->addColumn('code', array(
            'header' => $this->__('Attribute Code'),
            'index'  => 'code',
        ));

        $this->addColumn('label', array(
            'header' => $this->__('Attribute Label'),
            'index'  => 'label',
            'filter_index' => 'attribute_label.value'
        ));

        $this->addColumn('show_in_block', array(
            'header'  => $this->__('Show in Checkout Block'),
            'index'   => 'show_in_block',
            'type'    => 'options',
            'options' => Mage::getModel('ebs_totalattributes/source_showinblock')->toArray(),
        ));

        $this->addColumn('type', array(
            'header'  => $this->__('Type'),
            'index'   => 'type',
            'type'    => 'options',
            'options' => Mage::getModel('ebs_totalattributes/source_type')->toArray(),
            'width'   => '150px',
        ));

        $this->addColumn('is_enabled', array(
            'header'  => $this->__('Is Enabled'),
            'index'   => 'is_enabled',
            'type'    => 'options',
            'options' => Mage::getModel('ebs_totalattributes/source_yesno')->toArray(),
            'width'   => '100px',
        ));

        $this->addColumn('sort_order', array(
            'header' => $this->__('Sort Order'),
            'type'   => 'number',
            'index'  => 'sort_order',
            'width'  => '100px',
        ));

        $this->addColumn('action', array(
            'header'   => $this->__('Action'),
            'width'    => '50px',
            'type'     => 'action',
            'getter'   => 'getId',
            'actions'  => array(
                array(
                    'caption' => $this->__('Edit'),
                    'url'     => array('base' => '*/*/edit'),
                    'field'   => 'id',
                )
            ),
            'filter'   => false,
            'sortable' => false,
            'index'    => 'stores',
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('main_table.attribute_id');
        $this->getMassactionBlock()->setFormFieldName('attribute');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'   => Mage::helper('ebs_totalattributes')->__('Delete'),
            'url'     => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('ebs_totalattributes')->__('Are you sure?')
        ));

        $statuses = array(
            array(
                'value' => 1,
                'label' => Mage::helper('ebs_totalattributes')->__('Enable'),
            ),
            array(
                'value' => 0,
                'label' => Mage::helper('ebs_totalattributes')->__('Disable'),
            ),
        );

        array_unshift($statuses, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('status', array(
            'label'      => Mage::helper('ebs_totalattributes')->__('Change Status'),
            'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
            'additional' => array(
                'visibility' => array(
                    'name'   => 'status',
                    'type'   => 'select',
                    'class'  => 'required-entry',
                    'label'  => Mage::helper('ebs_totalattributes')->__('Status'),
                    'values' => $statuses,
                )
            )
        ));

        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit/', array('id' => $row->getId()));
    }
}