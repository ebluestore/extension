<?php

class Ebs_Totalattributes_Block_Adminhtml_Attribute extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'ebs_totalattributes';
        $this->_controller = 'adminhtml_attribute';
        $this->_headerText = $this->__('Manage Order Attributes');
        $this->_addButtonLabel = $this->__('Add New Attribute');
        parent::__construct();
    }
}