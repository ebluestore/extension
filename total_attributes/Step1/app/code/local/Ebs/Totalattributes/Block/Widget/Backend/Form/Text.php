<?php


class Ebs_Totalattributes_Block_Widget_Backend_Form_Text
    extends Ebs_Totalattributes_Block_Widget_Backend_FormAbstract
{
    public function getFieldId()
    {
        return $this->_getCode();
    }

    public function getFieldType()
    {
        return 'text';
    }

    public function getFieldTypeRenderer()
    {
        return null;
    }

    public function getFieldProperties()
    {
        $properties = array(
            'label'    => $this->_getLabel(),
            'name'     => $this->_getCode(),
            'class'    => $this->_getValidateCssClassNames(),
            'required' => $this->getProperty('validation_rules/is_required') ? true : false,
            'value'    => $this->_getValue(),
        );
        return $properties;
    }

    protected function _getValidateCssClassNames()
    {
        $result = "";
        if ($this->getProperty('validation_rules/minimum_text_length') ||
            $this->getProperty('validation_rules/maximum_text_length')
        ) {
            $result .= "validate-length ";
            if ($length = $this->getProperty('validation_rules/minimum_text_length')) {
                $result .= "minimum-length-" . (int)$length . " ";
            }
            if ($length = $this->getProperty('validation_rules/maximum_text_length')) {
                $result .= "maximum-length-" . (int)$length . " ";
            }
        }
        switch($this->getProperty('validation_rules/input_validation')) {
            case Ebs_Totalattributes_Model_Source_Validation::ALPHANUMERIC_CODE:
                $result .= "validate-alphanum-with-spaces ";
                break;
            case Ebs_Totalattributes_Model_Source_Validation::NUMERIC_CODE:
                $result .= "validate-number ";
                break;
            case Ebs_Totalattributes_Model_Source_Validation::ALPHA_CODE:
                $result .= "validate-alpha ";
                break;
            case Ebs_Totalattributes_Model_Source_Validation::URL_CODE:
                $result .= "validate-url ";
                break;
            case Ebs_Totalattributes_Model_Source_Validation::EMAIL_CODE:
                $result .= "validate-email ";
                break;
            default:
        }
        return trim($result);
    }
}