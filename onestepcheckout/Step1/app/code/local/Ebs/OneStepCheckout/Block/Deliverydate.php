<?php

class Ebs_OneStepCheckout_Block_Deliverydate extends Mage_Checkout_Block_Onepage_Abstract{

	public $config;

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function _construct()
    {
        parent::_construct();
        $this->_loadConfig();
    }

    protected function _loadConfig()
    {
        $this->config = Mage::helper('onestepcheckout/checkout')->loadConfig();
    }
}