<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$installer->run("
CREATE  TABLE IF NOT EXISTS `{$installer->getTable('ebs_totalattributes/attribute')}` (
  `attribute_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `code` VARCHAR(255) NOT NULL ,
  `type` VARCHAR(255) NOT NULL ,
  `default_value` TEXT NULL ,
  `is_enabled` SMALLINT(5) UNSIGNED NOT NULL ,
  `store_ids` VARCHAR(255) NULL ,
  `customer_groups` VARCHAR(255) NULL ,
  `display_on` VARCHAR(255) NULL ,
  `show_in_block` INT UNSIGNED NOT NULL ,
  `sort_order` INT UNSIGNED NOT NULL DEFAULT 0,
  `validation_rules` TEXT NOT NULL ,
  PRIMARY KEY (`attribute_id`) ,
  UNIQUE INDEX `code_UNIQUE` (`code` ASC)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Attribute Table';

CREATE  TABLE IF NOT EXISTS `{$installer->getTable('ebs_totalattributes/value_int')}` (
  `value_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `attribute_id` INT UNSIGNED NOT NULL ,
  `quote_id` INT UNSIGNED NOT NULL ,
  `value` INT NULL ,
  PRIMARY KEY (`value_id`) ,
  KEY `fk_ebs_totalattributes_value_int_quote_id` (`quote_id`),
  INDEX `fk_ebs_totalattributes_value_int_ebs_totalattributes_at_idx` (`attribute_id` ASC) ,
  CONSTRAINT `fk_ebs_totalattributes_value_int_sales_flat_quote`
    FOREIGN KEY (`quote_id`)
    REFERENCES `{$installer->getTable('sales_flat_quote')}` (`entity_id`)
    ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ebs_totalattributes_value_int_ebs_totalattributes_attr1`
    FOREIGN KEY (`attribute_id` )
    REFERENCES `{$installer->getTable('ebs_totalattributes/attribute')}` (`attribute_id` )
    ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Attribute Value(INT) table';

CREATE  TABLE IF NOT EXISTS `{$installer->getTable('ebs_totalattributes/value_varchar')}` (
  `value_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `attribute_id` INT UNSIGNED NOT NULL ,
  `quote_id` INT UNSIGNED NOT NULL ,
  `value` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`value_id`) ,
  KEY `fk_ebs_totalattributes_value_varchar_quote_id` (`quote_id`),
  INDEX `fk_ebs_totalattributes_value_varchar_ebs_totalattributes_idx` (`attribute_id` ASC) ,
  CONSTRAINT `fk_ebs_totalattributes_value_varchar_sales_flat_quote`
    FOREIGN KEY (`quote_id`)
    REFERENCES `{$installer->getTable('sales_flat_quote')}` (`entity_id`)
    ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ebs_totalattributes_value_varchar_ebs_totalattributes_1`
    FOREIGN KEY (`attribute_id` )
    REFERENCES `{$installer->getTable('ebs_totalattributes/attribute')}` (`attribute_id` )
    ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Attribute Value(VARCHAR) table';

CREATE  TABLE IF NOT EXISTS `{$installer->getTable('ebs_totalattributes/value_text')}` (
  `value_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `attribute_id` INT UNSIGNED NOT NULL ,
  `quote_id` INT UNSIGNED NOT NULL ,
  `value` TEXT NOT NULL ,
  PRIMARY KEY (`value_id`) ,
  KEY `fk_ebs_totalattributes_value_text_quote_id` (`quote_id`),
  INDEX `fk_ebs_totalattributes_value_text_ebs_totalattributes_a_idx` (`attribute_id` ASC) ,
  CONSTRAINT `fk_ebs_totalattributes_value_text_sales_flat_quote`
    FOREIGN KEY (`quote_id`)
    REFERENCES `{$installer->getTable('sales_flat_quote')}` (`entity_id`)
    ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ebs_totalattributes_value_text_ebs_totalattributes_att1`
    FOREIGN KEY (`attribute_id` )
    REFERENCES `{$installer->getTable('ebs_totalattributes/attribute')}` (`attribute_id` )
    ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Attribute Value(TEXT) table';

CREATE  TABLE IF NOT EXISTS `{$installer->getTable('ebs_totalattributes/value_date')}` (
  `value_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `attribute_id` INT UNSIGNED NOT NULL ,
  `quote_id` INT UNSIGNED NOT NULL ,
  `value` DATE NULL ,
  PRIMARY KEY (`value_id`) ,
  KEY `fk_ebs_totalattributes_value_date_quote_id` (`quote_id`),
  INDEX `fk_ebs_totalattributes_value_date_ebs_totalattributes_idx` (`attribute_id` ASC) ,
  CONSTRAINT `fk_ebs_totalattributes_value_date_sales_flat_quote`
    FOREIGN KEY (`quote_id`)
    REFERENCES `{$installer->getTable('sales_flat_quote')}` (`entity_id`)
    ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ebs_totalattributes_value_date_ebs_totalattributes1`
    FOREIGN KEY (`attribute_id` )
    REFERENCES `{$installer->getTable('ebs_totalattributes/attribute')}` (`attribute_id` )
    ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Attribute Value(DATE) table';

CREATE  TABLE IF NOT EXISTS `{$installer->getTable('ebs_totalattributes/option')}` (
  `option_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `attribute_id` INT UNSIGNED NOT NULL ,
  `sort_order` INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`option_id`) ,
  INDEX `fk_ebs_totalattributes_option_ebs_totalattributes_attri_idx` (`attribute_id` ASC) ,
  CONSTRAINT `fk_ebs_totalattributes_option_ebs_totalattributes_attribu1`
    FOREIGN KEY (`attribute_id` )
    REFERENCES `{$installer->getTable('ebs_totalattributes/attribute')}` (`attribute_id` )
    ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Attribute Option table';

CREATE  TABLE IF NOT EXISTS `{$installer->getTable('ebs_totalattributes/option_value')}` (
  `value_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `option_id` INT UNSIGNED NOT NULL ,
  `store_id` SMALLINT(5) UNSIGNED NOT NULL ,
  `value` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`value_id`) ,
  KEY `fk_ebs_totalattributes_option_value_store_id` (`store_id`),
  INDEX `fk_ebs_totalattributes_option_value_ebs_totalattributes_idx` (`option_id` ASC) ,
  CONSTRAINT `fk_ebs_totalattributes_option_value_store_entity`
    FOREIGN KEY (`store_id`)
    REFERENCES `{$installer->getTable('core_store')}` (`store_id`)
    ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ebs_totalattributes_option_value_ebs_totalattributes_o1`
    FOREIGN KEY (`option_id` )
    REFERENCES `{$installer->getTable('ebs_totalattributes/option')}` (`option_id` )
    ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Attribute Option Values table';

CREATE  TABLE IF NOT EXISTS `{$installer->getTable('ebs_totalattributes/label')}` (
  `label_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `attribute_id` INT UNSIGNED NOT NULL ,
  `store_id` SMALLINT(5) UNSIGNED NOT NULL ,
  `value` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`label_id`) ,
  KEY `fk_ebs_totalattributes_label_store_id` (`store_id`),
  INDEX `fk_ebs_totalattributes_label_ebs_totalattributes_attrib_idx` (`attribute_id` ASC) ,
  CONSTRAINT `fk_ebs_totalattributes_label_store_entity`
    FOREIGN KEY (`store_id`)
    REFERENCES `{$installer->getTable('core_store')}` (`store_id`)
    ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ebs_totalattributes_label_ebs_totalattributes_attribute1`
    FOREIGN KEY (`attribute_id` )
    REFERENCES `{$installer->getTable('ebs_totalattributes/attribute')}` (`attribute_id` )
    ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Attribute Label table';

CREATE  TABLE IF NOT EXISTS `{$installer->getTable('ebs_totalattributes/customer')}` (
  `id` int unsigned NOT NULL auto_increment,
  `attribute_id` int ,
  `attribute_code` varchar(200) NOT NULL,
  `sort_order` int ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Customer Attribute table';
");

$installer->endSetup();