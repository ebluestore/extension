<?php
class Ebs_Bannerslider_Model_Observer extends Varien_Object
{
    
    public function prepareLayoutBefore(Varien_Event_Observer $observer)
    {
        
        /* @var $block Mage_Page_Block_Html_Head */
        if (!Mage::helper('bannerslider')->isJqueryEnabled()) {
            return $this;
        }
        $block = $observer->getEvent()->getBlock();
        
        if ("head" == $block->getNameInLayout()) {
            foreach (Mage::helper('bannerslider')->getFiles() as $file) {
                $block->addJs(Mage::helper('bannerslider')->getJQueryPath($file));
            }
        }
        return $this;
    }
}