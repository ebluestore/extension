<?php

class Ebs_Totalattributes_Helper_Options extends Mage_Core_Helper_Data
{
    /**
     * for frontend form & backend grid
     *
     * @param Ebs_Totalattributes_Model_Attribute $attribute
     * @param null $storeId
     * @param bool $isAddPleaseSelect
     * @return array
     */
    public function getOptionsForAttributeByStoreId(Ebs_Totalattributes_Model_Attribute $attribute, $storeId = null, $isAddPleaseSelect = true) {
        if (is_null($storeId)) {
            $storeId = Mage::app()->getStore()->getId();
        }
        $options = $attribute->getStoreOptions();
        uasort($options, array($this, '_sortBySortOrder'));
        $resultOptions = array();
        if ($isAddPleaseSelect) {
            $resultOptions = array(
                '' => $this->__('---Please Select---')
            );
        }
        foreach($options as $optionId => $item) {
            if (isset($item['label'][$storeId]) && strlen(trim($item['label'][$storeId])) > 0){
                $resultOptions[$optionId] = $item['label'][$storeId];
            } else {
                $resultOptions[$optionId] = $item['label'][0];
            }
        }
        return $resultOptions;
    }

    /**
     * for backend form edit multiselect
     *
     * @param Ebs_Totalattributes_Model_Attribute $attribute
     * @param null $storeId
     * @param bool $isAddPleaseSelect
     * @return array
     */
    public function getOptionsForAttributeByStoreIdAsArray(Ebs_Totalattributes_Model_Attribute $attribute, $storeId = null, $isAddPleaseSelect = true) {
        if (is_null($storeId)) {
            $storeId = Mage::app()->getStore()->getId();
        }
        $options = $attribute->getStoreOptions();
        uasort($options, array($this, '_sortBySortOrder'));
        $resultOptions = array();
        if ($isAddPleaseSelect) {
            $resultOptions = array(
                '' => $this->__('---Please Select---')
            );
        }
        foreach($options as $optionId => $item) {
            if (isset($item['label'][$storeId]) && strlen(trim($item['label'][$storeId])) > 0){
                $resultOptions[] = array(
                    'value' => $optionId,
                    'label' => $item['label'][$storeId],
                );
            } else {
                $resultOptions[] = array(
                    'value' => $optionId,
                    'label' => $item['label'][0],
                );
            }
        }
        return $resultOptions;
    }

    public function getOptionsForYesnoAttribute($isAddPleaseSelect = true, $isChangeZeroToEmpty = false) {
        $optionSource = array();
        $source = Mage::getModel('ebs_totalattributes/attribute_type_yesno')->getAvailableOptionArray();
        foreach($source as $item) {
            if ($item['value'] === 0 || $item['value'] === '0') {
                if (!$isAddPleaseSelect) {
                    continue;
                }
                if ($isChangeZeroToEmpty) {
                    $item['value'] = '';
                }
            }
            $optionSource[$item['value']] = $item['label'];
        }
        return $optionSource;
    }

    private function _sortBySortOrder($a, $b)
    {
        if ($a['sort_order'] < $b['sort_order']) {
            return -1;
        } else if($a['sort_order'] > $b['sort_order']) {
            return 1;
        }
        return 0;
    }
}