<?php


interface Ebs_Totalattributes_Block_Widget_Backend_GridInterface
{
    /**
     * @return string
     */
    public function getColumnId();

    /**
     * @return array
     */
    public function getColumnProperties();

    /**
     * @param Ebs_Totalattributes_Model_Attribute_TypeInterface $type
     *
     * @return Ebs_Totalattributes_Block_Widget_Backend_GridAbstract
     */
    public function setTypeModel(Ebs_Totalattributes_Model_Attribute_TypeInterface $type);

    /**
     * @return Ebs_Totalattributes_Model_Attribute_TypeInterface
     */
    public function getTypeModel();

    /**
     * @param mixed $value
     *
     * @return Ebs_Totalattributes_Block_Widget_Backend_GridInterface
     */
    public function setValue($value);
}