<?php


class Ebs_Totalattributes_Block_Widget_Backend_Grid_Multipleselect_Renderer
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Options
{
    public function render(Varien_Object $row)
    {
        $row->setData($this->getColumn()->getIndex(), $this->_getRowValue($row));
        return parent::render($row);
    }

    public function _getRowValue($row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        if (is_string($value)) {
            $value = explode(',', $value);
        }
        if (!is_array($value)) {
            $value = array();
        }
        return $value;
    }
}