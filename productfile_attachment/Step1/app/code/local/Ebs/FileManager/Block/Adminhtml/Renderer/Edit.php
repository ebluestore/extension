<?php 

class Ebs_FileManager_Block_Adminhtml_Renderer_Edit extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /* Render Grid Column*/
    public function render(Varien_Object $row) 
    {
        if($this->getRequest()->getParam('id')){
            $pid= $this->getRequest()->getParam('id');
        }
        else $pid = '';
        return sprintf('
            <a href="javascript:void(0);" onclick="%s">%s</a>',
            "window.open('".$this->getUrl('*/*/file', array('_current'=>true, 'pid' => $pid, 'id' => $row->getId()))."','Edit','width=1024,height=650')",
            Mage::helper('ebs_filemanager')->__('Edit')
        );
    }
} 

?>