<?php

class Ebs_Totalattributes_Block_Adminhtml_Attribute_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_attribute';
        $this->_blockGroup = 'ebs_totalattributes';
        parent::__construct();

        $this->_addButton('saveandcontinueedit', array(
            'label'   => $this->__('Save and Continue Edit'),
            'onclick' => "saveAndContinueEdit('{$this->getSaveAndContinueUrl()}')",
            'class'   => 'save',
            'id'      => 'sarp2-save-and-continue',
        ), -200);
    }

    public function getHeaderText()
    {
        if ($this->getAttribute()->getId()) {
            return $this->__('Edit Order Attribute "%s"', $this->escapeHtml($this->getAttribute()->getLabel()));
        } else {
            return $this->__('Create Order Attribute');
        }
    }

    protected function _prepareLayout()
    {
        $tabsBlockJsObject = 'attribute_tabsJsTabs';
        $tabsBlockPrefix   = 'attribute_tabs_';

        $this->_formScripts[] = "
            function saveAndContinueEdit(urlTemplate) {
                var tabsIdValue = {$tabsBlockJsObject}.activeTab.id;
                var tabsBlockPrefix = '{$tabsBlockPrefix}';
                if (tabsIdValue.startsWith(tabsBlockPrefix)) {
                    tabsIdValue = tabsIdValue.substr(tabsBlockPrefix.length)
                }
                var template = new Template(urlTemplate, /(^|.|\\r|\\n)({{(\w+)}})/);
                var url = template.evaluate({tab_id:tabsIdValue});
                editForm.submit(url);
            }
        ";
        return parent::_prepareLayout();
    }

    public function getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', array(
            '_current'   => true,
            'back'       => 'edit',
            'active_tab' => '{{tab_id}}',
        ));
    }

    public function getValidationUrl()
    {
        return $this->getUrl('*/*/validate', array('_current'=>true));
    }

    public function getAttribute()
    {
        return Mage::registry('current_attribute');
    }
}