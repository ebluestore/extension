<?php
/**
 * Ebluestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade InstagramConnect to newer
 * versions in the future.
 *
 * @category    Ebluestore
 * @package     Ebs_SocialLogin
 * @copyright   Copyright (c) 2014 Ebluestore LLC
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ebs_SocialLogin_Block_Share extends Mage_Core_Block_Template {

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('ebs_sociallogin/shareorder/share.phtml');
    }

    public function getEnabled() 
    { 
        return Mage::getStoreConfig('ebs_sociallogin/shareorder/enable');
    }
    
    public function getFacebookImg($_product) 
    {
		if(Mage::getStoreConfig('ebs_sociallogin/shareorder/facebook_enable')) {
			$share = '<a name="fb_share" type="button_count" share_url="' . $_product->getProductUrl() .'"></a>';			
		} else {
			$share = '';
		}		
		return $share;		
    }
     
}
