<?php

class Ebs_Totalattributes_Model_Value extends Mage_Core_Model_Abstract
{

    protected $_attributeInstance = null;

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('ebs_totalattributes/value');
    }

    /**
     * @return Ebs_Totalattributes_Model_Attribute|null
     */
    public function getAttributeModel()
    {
        if (is_null($this->_attributeInstance)) {
            $attributeId = $this->getData('attribute_id');
            $this->_attributeInstance = Mage::getModel('ebs_totalattributes/attribute')->load($attributeId);
            if (is_null($this->_attributeInstance->getId())) {
                $this->_attributeInstance = null;
            }
        }
        return $this->_attributeInstance;
    }

    protected function _beforeSave()
    {
        if (!is_null($this->getAttributeModel())) {
            $typeModel = $this->getAttributeModel()->getTypeModel();
            if (!is_null($typeModel)) {
                $typeModel->beforeSave($this);
            }
        }
        return parent::_beforeSave();
    }
}