<?php

class Ebs_Totalattributes_Model_Source_Displayon
{
    const ORDER_GRID_PAGE_CODE  = 1;
    const ORDER_GRID_PAGE_LABEL = 'Order Grid';

    const PDFS_CODE  = 2;
    const PDFS_LABEL = 'Backend PDFs';

    const CUSTOMER_ACCOUNT_CODE  = 3;
    const CUSTOMER_ACCOUNT_LABEL = 'Customer Account';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $helper = Mage::helper('ebs_totalattributes');
        return array(
            array(
                'value' => self::ORDER_GRID_PAGE_CODE,
                'label' => $helper->__(self::ORDER_GRID_PAGE_LABEL),
            ),
            array(
                'value' => self::PDFS_CODE,
                'label' => $helper->__(self::PDFS_LABEL),
            ),
            array(
                'value' => self::CUSTOMER_ACCOUNT_CODE,
                'label' => $helper->__(self::CUSTOMER_ACCOUNT_LABEL),
            ),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $helper = Mage::helper('ebs_totalattributes');
        return array(
            self::ORDER_GRID_PAGE_CODE  => $helper->__(self::ORDER_GRID_PAGE_LABEL),
            self::PDFS_CODE             => $helper->__(self::PDFS_LABEL),
            self::CUSTOMER_ACCOUNT_CODE => $helper->__(self::CUSTOMER_ACCOUNT_LABEL),
        );
    }
}