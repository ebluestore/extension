<?php

class Ebs_Totalattributes_Block_Adminhtml_Category_Tab_General extends Mage_Adminhtml_Block_Catalog_Category_Tab_General
{

    protected function _getAdditionalElementTypes()
    {
        return array(
            'image' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_category_helper_image'),
            'boolean' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_boolean')
        );
    }
}
