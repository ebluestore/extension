<?php

class Ebs_Totalattributes_Model_Attribute_Type_Text extends Ebs_Totalattributes_Model_Attribute_TypeAbstract
{
    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Grid_Text
     */
    protected function _getBackendGridRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Grid_Text();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Form_Text
     */
    protected function _getBackendFormRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Form_Text();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_View_Text
     */
    protected function _getBackendViewRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_View_Text();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Backend_Pdf_Text
     */
    protected function _getBackendPdfRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Backend_Pdf_Text();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Frontend_Form_Text
     */
    protected function _getFrontendFormRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Frontend_Form_Text();
    }

    /**
     * @return Ebs_Totalattributes_Block_Widget_Frontend_View_Text
     */
    protected function _getFrontendViewRenderer()
    {
        return new Ebs_Totalattributes_Block_Widget_Frontend_View_Text();
    }

    public function getValueType()
    {
        return Ebs_Totalattributes_Model_Resource_Value::VARCHAR_TYPE;
    }

    /**
     * @param mixed $value
     */
    public function validate($value)
    {
        $helper = Mage::helper('ebs_totalattributes');
        $storeId = Mage::app()->getStore()->getId();
        $label = $this->getAttribute()->getLabel($storeId);
        $label = Mage::helper('core')->escapeHtml($label);
        if (strlen(trim($value)) < 1) {
            if ($this->getAttribute()->getData('validation_rules/is_required')) {
                throw new Exception($helper->__('%s is required', $label));
            }
        } else {
            //if value specified
            switch($this->getAttribute()->getData('validation_rules/input_validation')) {
                case Ebs_Totalattributes_Model_Source_Validation::ALPHANUMERIC_CODE:
                    if (!Zend_Validate::is($value, 'Alnum', array('allowWhiteSpace' => true))) {
                        throw new Exception($helper->__('%s can not contain characters other than letters, digits and spaces', $label));
                    }
                    break;
                case Ebs_Totalattributes_Model_Source_Validation::NUMERIC_CODE:
                    if (!Zend_Validate::is($value, 'Digits')) {
                        throw new Exception($helper->__('%s contains not only digit characters', $label));
                    }
                    break;
                case Ebs_Totalattributes_Model_Source_Validation::ALPHA_CODE:
                    if (!Zend_Validate::is($value, 'Alpha')) {
                        throw new Exception($helper->__('%s has not only alphabetic characters', $label));
                    }
                    break;
                case Ebs_Totalattributes_Model_Source_Validation::EMAIL_CODE:
                    if (!Zend_Validate::is($value, 'EmailAddress')) {
                        throw new Exception($helper->__('%s has invalid email address', $label));
                    }
                    break;
                default:
            }
        }
        if ($length = $this->getAttribute()->getData('validation_rules/minimum_text_length')) {
            if (!Zend_Validate::is($value, 'StringLength', array('min' => $length, 'max' => null))) {
                throw new Exception($helper->__("%s is less than '%s' symbols", $label, $length));
            }
        }
        if ($length = $this->getAttribute()->getData('validation_rules/maximum_text_length')) {
            if (!Zend_Validate::is($value, 'StringLength', array('min' => 0, 'max' => $length))) {
                throw new Exception($helper->__("%s is greater than '%s' symbols", $label, $length));
            }
        }
        return;
    }
}