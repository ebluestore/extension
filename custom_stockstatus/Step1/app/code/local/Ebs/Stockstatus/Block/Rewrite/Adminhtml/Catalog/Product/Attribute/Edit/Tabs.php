<?php

class Ebs_Stockstatus_Block_Rewrite_Adminhtml_Catalog_Product_Attribute_Edit_Tabs extends Mage_Adminhtml_Block_Catalog_Product_Attribute_Edit_Tabs
{
    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();

        if ('custom_stock_status' == Mage::registry('entity_attribute')->getData('attribute_code'))
        {
            $this->addTab('icons', array(
                'label'     => Mage::helper('stockstatus')->__('Manage Icons'),
                'title'     => Mage::helper('stockstatus')->__('Manage Icons'),
                'content'   => $this->getLayout()->createBlock('stockstatus/icons')->toHtml(),
            ));
            $this->addTab('ranges', array(
                'label'     => Mage::helper('stockstatus')->__('Quantity Range Statuses'),
                'title'     => Mage::helper('stockstatus')->__('Quantity Range Statuses'),
                'content'   => $this->getLayout()->createBlock('stockstatus/ranges')->toHtml(),
            ));
            return Mage_Adminhtml_Block_Widget_Tabs::_beforeToHtml();
        }
        
        return $this;
    }
}