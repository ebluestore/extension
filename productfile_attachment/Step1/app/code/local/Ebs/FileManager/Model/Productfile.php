<?php

class Ebs_FileManager_Model_Productfile extends Mage_Core_Model_Abstract
{
    protected $_store_id = null;
    
    public function _construct()
    {
        parent::_construct();        
        if ($storeId = Mage::app()->getStore()->getId()) {            
            $this->setStoreId($storeId);
        }
        $this->_init('ebs_filemanager/productfile');
    }
           
    public function setStoreId($value) {
        $this->_store_id = $value;
        return $this;
    }
    
    public function getStoreId() {
        return $this->_store_id;
    }
    
    public function getProductFlieAttributes() {
        return array(
            'title',           
            'content',
            'status',
        );
    }
    
    public function load($id, $field = null){
        parent::load($id, $field);                                    
        if ($this->getStoreId()) {    
            
            $this->loadProductFileValue();                        
        }                
        return $this;
    }
    
    public function loadProductFileValue($storeId = null) {
        if (!$storeId) {
            $storeId = $this->getStoreId();
        }
        if (!$storeId) {
            return $this;
        }                
        $storeValues = Mage::getModel('ebs_filemanager/value')->getCollection()
            ->addFieldToFilter('productfile_id',$this->getId())
            ->addFieldToFilter('store_id',$storeId);        
        foreach ($storeValues as $value) {
            $this->setData($value->getAttributeCode().'_in_store',true);
            $this->setData($value->getAttributeCode(),$value->getValue());
        }    
        return $this;
    }
    
     protected function _beforeSave(){                        
        if ($storeId = $this->getStoreId()){        
            $defaultProductFile = Mage::getModel('ebs_filemanager/productfile')->load($this->getId());                
            $productFileAttributes = $this->getProductFlieAttributes();
            foreach ($productFileAttributes as $attribute){        
                
                if ($this->getData($attribute.'_default')){                
                    $this->setData($attribute.'_in_store',false);
                }else{                    
                    $this->setData($attribute.'_in_store',true);
                    $this->setData($attribute.'_value',$this->getData($attribute));
                }
                $this->setData($attribute,$defaultProductFile->getData($attribute));
            }           
        }        
        return parent::_beforeSave();
    }
    
    protected function _afterSave(){
        if ($storeId = $this->getStoreId()){    
            $defaultProductFile = $this->getProductFlieAttributes();            
            foreach ($defaultProductFile as $attribute){
                $attributeValue = Mage::getModel('ebs_filemanager/value')
                    ->loadAttributeValue($this->getId(),$storeId,$attribute);
                if ($this->getData($attribute.'_in_store')){
                    try{
                        $attributeValue->setValue($this->getData($attribute.'_value'))->save();
                    }catch(Exception $e){
                        
                    }
                }elseif($attributeValue && $attributeValue->getId()){
                    try{
                        $attributeValue->delete();
                    }catch(Exception $e){
                        
                    }
                }
            }
        }
        return parent::_afterSave();
    }
    
    public function getProductIds()
    {
        $productIds = array();
        $collection = Mage::getResourceModel('ebs_filemanager/fproduct_collection')->addFieldToFilter('productfile_id',$this->getId());
        if (count($collection))
        {
            foreach ($collection as $item) {
                $productIds[] = $item->getProductId();
            }
            return $productIds;
        }
    }
        
    public function getProductfiles()
    {
        $collection  = Mage::getResourceModel('ebs_filemanager/productfile_collection')                        
                        ->join('fproduct', 'fproduct.productfile_id=main_table.productfile_id',array('product_id'=>'product_id'))                       
                        ->addFieldToFilter('fproduct.product_id',Mage::app()->getRequest()->getParam('id'));        
                
        $collection = $collection->addFieldToFilter('status', 1);
        $collection->setOrder('title','ASC');
        
        return $collection;                    
    }  
    
} 

?>