<?php

class Ebs_Totalattributes_Model_Source_Yesno extends Mage_Adminhtml_Model_System_Config_Source_Yesno
{
    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            0 => Mage::helper('adminhtml')->__('No'),
            1 => Mage::helper('adminhtml')->__('Yes'),
        );
    }
}