<?php

class Ebs_Totalattributes_Model_Resource_Option_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ebs_totalattributes/option');
    }

    /**
     * @param $attribute Ebs_Totalattributes_Model_Attribute|int
     *
     * @return Ebs_Totalattributes_Model_Resource_Option_Collection
     */
    public function addAttributeFilter($attribute)
    {
        if ($attribute instanceof Ebs_Totalattributes_Model_Attribute) {
            $attributeId = $attribute->getId();
        } elseif(is_numeric($attribute)) {
            $attributeId = $attribute;
        } else {
            return $this;
        }
        $this->addFieldToFilter('attribute_id', $attributeId);
        return $this;
    }
}