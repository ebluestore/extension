<?php

class Ebs_Dailydeal_Block_Adminhtml_Subscriber_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        $this->setId('dailydealmailGrid');
        $this->setDefaultSort('deal_email_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        parent::__construct();
    }
    
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('dailydeal/subscriber')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {
        $this->addColumn('deal_email_id', array(
            'header' => Mage::helper('dailydeal')->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'deal_email_id'
        ));
        
        $this->addColumn('customer_name', array(
            'header' => Mage::helper('dailydeal')->__('Name'),
            'align' => 'left',
            'index' => 'customer_name'
        ));
        
        $this->addColumn('email', array(
            'header' => Mage::helper('dailydeal')->__('Email'),
            'align' => 'left',
            'index' => 'email'
        ));
        
        $this->addColumn('status', array(
            'header' => Mage::helper('dailydeal')->__('Status'),
            'align' => 'left',
            'width' => '80px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                Ebs_Dailydeal_Model_Status::SUBSCRIBED      => $this->__('Subscribed'),
                Ebs_Dailydeal_Model_Status::UNSUBSCRIBED    => $this->__('Unsubscribed')
            )
        ));

        $this->addColumn('action', array(
            'header' => Mage::helper('catalog')->__('Action'),
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('catalog')->__('Edit'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'is_system' => true,
        ));
        
        $this->addExportType('*/*/exportCsv', Mage::helper('dailydeal')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('dailydeal')->__('XML'));
        return parent::_prepareColumns();
    }
    
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('deal_email_id');
        $this->getMassactionBlock()->setFormFieldName('dailydeal');
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('dailydeal')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('dailydeal')->__('Are you sure?')
        ));
        
        $options = array(
            array(
                'label' => $this->__('Subscribed'),
                'value' => Ebs_Dailydeal_Model_Status::SUBSCRIBED
            ),
            array(
                'label' => $this->__('Unsubscribed'),
                'value' => Ebs_Dailydeal_Model_Status::UNSUBSCRIBED
            )
        );
        
        $this->getMassactionBlock()->addItem('status', array(
            'label' => Mage::helper('dailydeal')->__('Change status'),
            'url' => $this->getUrl('*/*/massStatus', array(
                '_current' => true
            )),
            'additional' => array(
                'visibility' => array(
                    'name' => 'status',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('dailydeal')->__('Status'),
                    'values' => $options
                )
            )
        ));
        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }
}