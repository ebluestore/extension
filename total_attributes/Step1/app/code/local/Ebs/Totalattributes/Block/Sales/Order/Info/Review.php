<?php

class Ebs_Totalattributes_Block_Sales_Order_Info_Review extends Mage_Core_Block_Template
{
    public function isOrderHasOrderReviewAttributes()
    {
        return !!Mage::helper('ebs_totalattributes/order')
            ->getAttributeCollectionForCustomerAccount()
            ->addShowInOrderReviewBlockFilter()
            ->getSize()
        ;
    }
}