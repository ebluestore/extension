<?php

class Ebs_Totalattributes_Adminhtml_Category_SetController extends Mage_Adminhtml_Controller_Action
{
	
	public function getHelper() {
		return Mage::helper('ebs_totalattributes');
	}

	public function indexAction()
	{
		$this->_title($this->getHelper()->__('Category Attributes'))
				->_title($this->getHelper()->__('Manage Attribute Group'));

		$this->_setTypeId();

		$this->loadLayout();
		$this->_setActiveMenu('totalattributes/ebs_categoryattributes');

		$this->_addBreadcrumb($this->getHelper()->__('Category Attributes'), $this->getHelper()->__('Category Attributes'));
		$this->_addBreadcrumb($this->getHelper()->__('Manage Attribute Group'), $this->getHelper()->__('Manage Attribute Group'));

		$this->_addContent($this->getLayout()->createBlock('ebs_totalattributes/adminhtml_category_attribute_set_toolbar_main'));
		$this->_addContent($this->getLayout()->createBlock('ebs_totalattributes/adminhtml_category_attribute_set_grid'));

		$this->renderLayout();
	}

	public function editAction()
	{
		$this->_title($this->getHelper()->__('Category Attributes'))
				->_title($this->getHelper()->__('Manage Attribute Group'));

		$this->_setTypeId();
		$attributeSet = Mage::getModel('eav/entity_attribute_set')
				->load($this->getRequest()->getParam('id'));

		if (!$attributeSet->getId()) {
			$this->_redirect('*/*/index');
			return;
		}

		$this->_title($attributeSet->getId() ? $attributeSet->getAttributeSetName() : $this->getHelper()->__('New Group'));

		Mage::register('current_attribute_set', $attributeSet);

		$this->loadLayout();
		$this->_setActiveMenu('totalattributes/ebs_categoryattributes');
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

		$this->_addBreadcrumb($this->getHelper()->__('Category Attributes'), $this->getHelper()->__('Category Attributes'));
		$this->_addBreadcrumb($this->getHelper()->__('Manage Category Group'), $this->getHelper()->__('Manage Category Group'));

		$this->_addContent($this->getLayout()->createBlock('ebs_totalattributes/adminhtml_category_attribute_set_main'));

		$this->renderLayout();
	}

	public function setGridAction()
	{
		$this->_setTypeId();
		$this->getResponse()->setBody($this->getLayout()->createBlock('ebs_totalattributes/adminhtml_category_attribute_set_grid')->toHtml());
	}

	/**
	* Save attribute set action
	*
	* [POST] Create attribute set from another set and redirect to edit page
	* [AJAX] Save attribute set data
	*
	*/
	public function saveAction()
	{
		$entityTypeId   = $this->_getEntityTypeId();
		$hasError       = false;
		$attributeSetId = $this->getRequest()->getParam('id', false);
		$isNewSet       = $this->getRequest()->getParam('gotoEdit', false) == '1';

		/* @var $model Mage_Eav_Model_Entity_Attribute_Set */
		$model  = Mage::getModel('eav/entity_attribute_set')
				->setEntityTypeId($entityTypeId);

		try {
			if ($isNewSet) {
				$model->setAttributeSetName($this->getRequest()->getParam('attribute_set_name'));
			} else {
				if ($attributeSetId) {
					$model->load($attributeSetId);
				}
				if (!$model->getId()) {
					Mage::throwException($this->getHelper()->__('This attribute group no longer exists.'));
				}
				$data = Mage::helper('core')->jsonDecode($this->getRequest()->getPost('data'));
				$model->organizeData($data);
			}

			$model->validate();
			if ($isNewSet) {
				$model->save();
				$model->initFromSkeleton($this->getRequest()->getParam('skeleton_set'));
			}
			$model->save();
			$this->_getSession()->addSuccess($this->getHelper()->__('The attribute group has been saved.'));
		} catch (Mage_Core_Exception $e) {
			$this->_getSession()->addError($e->getMessage());
			$hasError = true;
		} catch (Exception $e) {
			$this->_getSession()->addException($e, $this->getHelper()->__('An error occurred while saving the attribute group.'));
			$hasError = true;
		}

		if ($isNewSet) {
			if ($hasError) {
				$this->_redirect('*/*/add');
			} else {
				$this->_redirect('*/*/edit', array('id' => $model->getId()));
			}
		} else {
			$response = array();
			if ($hasError) {
				$this->_initLayoutMessages('adminhtml/session');
				$response['error']   = 1;
				$response['message'] = $this->getLayout()->getMessagesBlock()->getGroupedHtml();
			} else {
				$response['error']   = 0;
				$response['url']     = $this->getUrl('*/*/');
			}
			$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
		}
	}

	public function addAction()
	{
		$this->_title($this->getHelper()->__('Category Attributes'))
				->_title($this->getHelper()->__('Manage Attribute Group'))
				->_title($this->getHelper()->__('New Group'));

		$this->_setTypeId();

		$this->loadLayout();
		$this->_setActiveMenu('totalattributes/ebs_categoryattributes');

		$this->_addContent($this->getLayout()->createBlock('ebs_totalattributes/adminhtml_category_attribute_set_toolbar_add'));

		$this->renderLayout();
	}

	public function deleteAction()
	{
		$setId = $this->getRequest()->getParam('id');
		try {
			Mage::getModel('eav/entity_attribute_set')
					->setId($setId)
					->delete();

			$this->_getSession()->addSuccess($this->getHelper()->__('The attribute group has been removed.'));
			$this->getResponse()->setRedirect($this->getUrl('*/*/'));
		} catch (Exception $e) {
			$this->_getSession()->addError($this->getHelper()->__('An error occurred while deleting this group.'));
			$this->_redirectReferer();
		}
	}

	/**
	* Define in register catalog_product entity type code as entityType
	*
	*/
	protected function _setTypeId()
	{
		Mage::register('entityType',
		Mage::getModel('catalog/category')->getResource()->getTypeId());
	}

	/**
	* Retrieve catalog category entity type id
	*
	* @return int
	*/
	protected function _getEntityTypeId()
	{
		if (is_null(Mage::registry('entityType'))) {
			$this->_setTypeId();
		}
		return Mage::registry('entityType');
	}

}
