<?php


interface Ebs_Totalattributes_Block_Widget_Frontend_FormInterface
{
    /**
     * @param boolean $isForEdit = false
     *
     * @return mixed
     */
    public function getHtml($isForEdit = false);

    /**
     * @param Ebs_Totalattributes_Model_Attribute_TypeInterface $type
     *
     * @return Ebs_Totalattributes_Block_Widget_Frontend_FormAbstract
     */
    public function setTypeModel(Ebs_Totalattributes_Model_Attribute_TypeInterface $type);

    /**
     * @return Ebs_Totalattributes_Model_Attribute_TypeInterface
     */
    public function getTypeModel();

    /**
     * @param mixed $value
     *
     * @return Ebs_Totalattributes_Block_Widget_Frontend_FormInterface
     */
    public function setValue($value);
}