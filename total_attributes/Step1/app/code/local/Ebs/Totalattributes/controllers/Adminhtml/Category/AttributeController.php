<?php

class Ebs_Totalattributes_Adminhtml_Category_AttributeController extends Mage_Adminhtml_Controller_Action
{

	protected $_entityTypeId;
	
	public function getHelper() {
		return Mage::helper('ebs_totalattributes');
	}

	public function preDispatch()
	{
		parent::preDispatch();
		$this->_entityTypeId = Mage::getModel('eav/entity')->setType('catalog_category')->getTypeId();
	}

	protected function _initAction()
	{
		$this->_title($this->getHelper()->__('Category Attributes'))
			->_title($this->getHelper()->__('Manage Attributes'));

		if($this->getRequest()->getParam('popup')) {
			$this->loadLayout('popup');
		} else {
			$this->loadLayout()
				->_setActiveMenu('totalattributes/ebs_categoryattributes')
				->_addBreadcrumb($this->getHelper()->__('Category Attributes'), $this->getHelper()->__('Category Attributes'))
				->_addBreadcrumb($this->getHelper()->__('Manage Category Attributes'), $this->getHelper()->__('Manage Category Attributes'));
		}
		return $this;
	}

	public function indexAction()
	{
		$this->_initAction();
        $this->renderLayout();
	}

	public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
	
	public function newAction()
	{
		$this->_forward('edit');
	}

	public function editAction()
	{
		$id = $this->getRequest()->getParam('attribute_id');
		$model = Mage::getModel('catalog/resource_eav_attribute')
				->setEntityTypeId($this->_entityTypeId);
		if ($id) {
			$model->load($id);

			if (! $model->getId()) {
				Mage::getSingleton('adminhtml/session')->addError($this->getHelper()->__('This attribute no longer exists'));
				$this->_redirect('*/*/');
				return;
			}

			// entity type check
			if ($model->getEntityTypeId() != $this->_entityTypeId) {
				Mage::getSingleton('adminhtml/session')->addError($this->getHelper()->__('This attribute cannot be edited.'));
				$this->_redirect('*/*/');
				return;
			    }
		}

		// set entered data if was error when we do save
		$data = Mage::getSingleton('adminhtml/session')->getCategoryAttributeData(true);
		if (! empty($data)) {
			$model->addData($data);
		}
	
		Mage::register('entity_attribute', $model);
	
		$this->_initAction();
	
		$this->_title($id ? $model->getName() : $this->getHelper()->__('New Attribute'));
	
		$item = $id ? $this->getHelper()->__('Edit Category Attribute') : $this->getHelper()->__('New Category Attribute');
	
		$this->_addBreadcrumb($item, $item)			 
             ->_addJs( $this->getLayout()->getBlock('category_attribute_edit_js')->setIsPopup((bool)$this->getRequest()->getParam('popup')) );		
		//$this->getLayout()->getBlock('category_attribute_edit_js')->setIsPopup((bool)$this->getRequest()->getParam('popup'));
	
		$this->renderLayout();

	}

	public function validateAction()
	{
		$response = new Varien_Object();
		$response->setError(false);
		
		$attributeCode  = $this->getRequest()->getParam('attribute_code');
		$attributeId    = $this->getRequest()->getParam('attribute_id');
		$attribute = Mage::getModel('catalog/resource_eav_attribute')->loadByCode($this->_entityTypeId, $attributeCode);
	
		if ($attribute->getId() && !$attributeId) {
			Mage::getSingleton('adminhtml/session')->addError($this->getHelper()->__('Attribute with the same code already exists'));
			$this->_initLayoutMessages('adminhtml/session');
			$response->setError(true);
			$response->setMessage($this->getLayout()->getMessagesBlock()->getGroupedHtml());
		}
		
		$this->getResponse()->setBody($response->toJson());
	}
	
	public function saveAction()
	{
		if ($data = $this->getRequest()->getPost()) {
			$redirectBack   = $this->getRequest()->getParam('back', false);
			/* @var $model Mage_Catalog_Model_Entity_Attribute */
			$model = Mage::getModel('catalog/resource_eav_attribute');
			/* @var $helper Ebs_Totalattributes_Helper_Category */
			$helper = Mage::helper('ebs_totalattributes/category');
		
			if ($id = $this->getRequest()->getParam('attribute_id')) {
				$model->load($id);
			
				if (!$model->getId()) {
					Mage::getSingleton('adminhtml/session')->addError($this->getHelper()->__('This Attribute no longer exists'));
					$this->_redirect('*/*/');
					return;
				}
			
				// entity type check
				if ($model->getEntityTypeId() != $this->_entityTypeId) {
					Mage::getSingleton('adminhtml/session')->addError($this->getHelper()->__('This attribute cannot be updated.'));
					Mage::getSingleton('adminhtml/session')->setAttributeData($data);
					$this->_redirect('*/*/');
					return;
				}
			
				$data['attribute_code'] = $model->getAttributeCode();
				$data['is_user_defined'] = $model->getIsUserDefined();
				$data['frontend_input'] = $model->getFrontendInput();
			} else {
				/**
				* @todo add to helper and specify all relations for properties
				*/
				$data['source_model'] = $helper->getAttributeSourceModelByInputType($data['frontend_input']);
				$data['backend_model'] = $helper->getAttributeBackendModelByInputType($data['frontend_input']);
			}
		
			if (!isset($data['is_configurable'])) {
				$data['is_configurable'] = 0;
			}
			if (!isset($data['is_filterable'])) {
				$data['is_filterable'] = 0;
			}
			if (!isset($data['is_filterable_in_search'])) {
				$data['is_filterable_in_search'] = 0;
			}
		
			if (is_null($model->getIsUserDefined()) || $model->getIsUserDefined() != 0) {
				$data['backend_type'] = $model->getBackendTypeByInput($data['frontend_input']);
			}
		
			$defaultValueField = $model->getDefaultValueByInput($data['frontend_input']);
			if ($defaultValueField) {
				$data['default_value'] = $this->getRequest()->getParam($defaultValueField);
			}
		
			if(!isset($data['apply_to'])) {
				$data['apply_to'] = array();
			}
		
			$model->addData($data);
		
			if (!$id) {
				$model->setEntityTypeId($this->_entityTypeId);
				$model->setIsUserDefined(1);
			}
		
		
			if($this->getRequest()->getParam('set') && $this->getRequest()->getParam('group')) {
				// For creating category attribute on category page we need specify attribute set and group
				$model->setAttributeSetId($this->getRequest()->getParam('set'));
				$model->setAttributeGroupId($this->getRequest()->getParam('group'));
			}
		
			try {
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess($this->getHelper()->__('The category attribute has been saved.'));
		
				/**
				* Clear translation cache because attribute labels are stored in translation
				*/
				Mage::app()->cleanCache(array(Mage_Core_Model_Translate::CACHE_TAG));
				Mage::getSingleton('adminhtml/session')->setAttributeData(false);
				if ($this->getRequest()->getParam('popup')) {
					$this->_redirect('adminhtml/catalog_category/addAttribute', array(
					'id'       => $this->getRequest()->getParam('category'),
					'attribute'=> $model->getId(),
					'_current' => true
					));
				} elseif ($redirectBack) {
					$this->_redirect('*/*/edit', array('attribute_id' => $model->getId(),'_current'=>true));
				} else {
					$this->_redirect('*/*/', array());
				}
				return;
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setAttributeData($data);
				$this->_redirect('*/*/edit', array('_current' => true));
				return;
			}
		}
		$this->_redirect('*/*/');
	}
	
	public function deleteAction()
	{
		if ($id = $this->getRequest()->getParam('attribute_id')) {
			$model = Mage::getModel('catalog/resource_eav_attribute');
		
			// entity type check
			$model->load($id);
			if ($model->getEntityTypeId() != $this->_entityTypeId) {
				Mage::getSingleton('adminhtml/session')->addError($this->getHelper()->__('This attribute cannot be deleted.'));
				$this->_redirect('*/*/');
				return;
			}
		
			try {
				$model->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess($this->getHelper()->__('The category attribute has been deleted.'));
				$this->_redirect('*/*/');
				return;
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('attribute_id' => $this->getRequest()->getParam('attribute_id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError($this->getHelper()->__('Unable to find an attribute to delete.'));
		$this->_redirect('*/*/');
	}

}
