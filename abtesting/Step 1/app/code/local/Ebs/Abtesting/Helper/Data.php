<?php
/**
 * Ebluestore
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Abtesting to newer
 * versions in the future.
 *
 * @category    Ebluestore
 * @package     Ebs_Abtesting
 * @copyright   Copyright (c) 2014 Ebluestore LLC (http://www.ebluestore.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Ebs_Abtesting_Helper_Data extends Mage_Core_Helper_Abstract
{
}
	 