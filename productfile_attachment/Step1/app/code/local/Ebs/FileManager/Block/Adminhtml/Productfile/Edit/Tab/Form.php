<?php

class Ebs_FileManager_Block_Adminhtml_Productfile_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('productfile_form', array(
            'legend' => Mage::helper('ebs_filemanager')->__('File information')
        ));
        
        if (Mage::getSingleton('adminhtml/session')->getProductfileData()) {
            $data = Mage::getSingleton('adminhtml/session')->getProductfileData();
        } elseif (Mage::registry('productfile_data')) {
            $data = Mage::registry('productfile_data')->getData();
        }
        
        $inStore      = $this->getRequest()->getParam('store');
        $defaultLabel = Mage::helper('ebs_filemanager')->__('Use Default');
        $defaultTitle = Mage::helper('ebs_filemanager')->__('-- Please Select --');
        $scopeLabel   = Mage::helper('ebs_filemanager')->__('STORE VIEW');
        
        $fieldset->addField('title', 'text', array(
            'label' => Mage::helper('ebs_filemanager')->__('Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title',
            'disabled' => ($inStore && !$data['title_in_store']),
            'after_element_html' => $inStore ? '<td class="use-default">
            <input id="title_default" name="title_default" type="checkbox" value="1" class="checkbox config-inherit" ' . ($data['title_in_store'] ? '' : 'checked="checked"') . ' onclick="toggleValueElements(this, Element.previous(this.parentNode))" />
            <label for="title_default" class="inherit" title="' . $defaultTitle . '">' . $defaultLabel . '</label>
          </td><td class="scope-label">
            [' . $scopeLabel . ']</td>
          ' : '<td class="scope-label">
            [' . $scopeLabel . ']</td>'
        ));
        
        if ($data)
            $required = false;
        else
            $required = true;
        
        $fieldset->addField('filename', 'file', array(
            'label' => Mage::helper('ebs_filemanager')->__('File'),
            'required' => $required,
            'name' => 'filename'
        ));
                
        if ($data) {
            $fieldset->addField('currentfile', 'note', array(
                'label' => Mage::helper('ebs_filemanager')->__('Current file'),
                'required' => false,
                'text' => $data['filename']
                
                
            ));
        }
        
        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('ebs_filemanager')->__('Status'),
            'name' => 'file_status',
            'values' => array(
                array(
                    'value' => 1,
                    'label' => Mage::helper('ebs_filemanager')->__('Enabled')
                ),
                
                array(
                    'value' => 2,
                    'label' => Mage::helper('ebs_filemanager')->__('Disabled')
                )
            ),
            'disabled' => ($inStore && !$data['status_in_store']),
            'after_element_html' => $inStore ? '<td class="use-default">
            <input id="status_default" name="status_default" type="checkbox" value="1" class="checkbox config-inherit" ' . ($data['status_in_store'] ? '' : 'checked="checked"') . ' onclick="toggleValueElements(this, Element.previous(this.parentNode))" />
            <label for="status_default" class="inherit" title="' . $defaultTitle . '">' . $defaultLabel . '</label>
          </td><td class="scope-label">
            [' . $scopeLabel . ']</td>
          ' : '<td class="scope-label">
            [' . $scopeLabel . ']</td>'
        ));
        
        $fieldset->addField('content', 'editor', array(
            'name' => 'content',
            'label' => Mage::helper('ebs_filemanager')->__('Content'),
            'title' => Mage::helper('ebs_filemanager')->__('Content'),
            'style' => 'width:500px; height:150px;',
            'wysiwyg' => false,
            'required' => false,
            'disabled' => ($inStore && !$data['content_in_store']),
            'after_element_html' => $inStore ? '<td class="use-default">
            <input id="content_default" name="content_default" type="checkbox" value="1" class="checkbox config-inherit" ' . ($data['content_in_store'] ? '' : 'checked="checked"') . ' onclick="toggleValueElements(this, Element.previous(this.parentNode))" />
            <label for="content_default" class="inherit" title="' . $defaultTitle . '">' . $defaultLabel . '</label>
          </td><td class="scope-label">
            [' . $scopeLabel . ']</td>
          ' : '<td class="scope-label">
            [' . $scopeLabel . ']</td>'
        ));
        
        if (Mage::getSingleton('adminhtml/session')->getProductfileData()) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getProductfileData());
            Mage::getSingleton('adminhtml/session')->setProductfileData(null);
        } elseif (Mage::registry('productfile_data')) {
            $form->setValues(Mage::registry('productfile_data')->getData());
        }
        return parent::_prepareForm();
    }
}

?>