<?php

class Ebs_Helpmate_Block_Adminhtml_Ticket_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $id = $this->getRequest()->getParam('id');

        $form = new Varien_Data_Form(array(
            'id'      => 'edit_form',
            'action'  => $this->getUrl('*/*/save', array('id' => $id)),
            'method'  => 'post'

        ));

        $this->setForm($form);

        if (Mage::registry('helpmate_ticket_data') ) {
            $data = Mage::registry('helpmate_ticket_data')->getData();
        }
        $isOldTicket = isset($data['id']);
        $fieldsetGeneral = $form->addFieldset(
            'ticket_general_form',
            array('legend' => Mage::helper('helpmate')->__('General Details'))
        );
        $fieldsetGeneral->addField('id', 'hidden', array(
            'name'      => 'id'
        ));
        
        $fieldsetGeneral->addField('number', 'hidden', array(
            'name'      => 'number'
        ));

        $fieldsetGeneral->addField('title', 'text', array(
            'label'     => Mage::helper('helpmate')->__('Title'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'title',
        ));

        $fieldsetGeneral->addField('created_at', 'date', array(
            'label'     => Mage::helper('helpmate')->__('Create date'),
            'disabled'  => $isOldTicket,
            'image'     => $this->getSkinUrl('images/grid-cal.gif'),
            'format'    => Varien_Date::DATETIME_INTERNAL_FORMAT,
            'name'      => 'created_at',
        ));

        $fieldsetGeneral->addField('modified_at', 'date', array(
            'label'     => Mage::helper('helpmate')->__('Modified date'),
            'disabled'  => $isOldTicket,
            'image'     => $this->getSkinUrl('images/grid-cal.gif'),
            'format'    => Varien_Date::DATETIME_INTERNAL_FORMAT,
            'name'      => 'modified_at',
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $fieldsetGeneral->addField('store_id', 'select', array(
                'name'      => 'store_id',
                'label'     => Mage::helper('cms')->__('Store View'),
                'title'     => Mage::helper('cms')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')
                    ->getStoreValuesForForm(false, true),
            ));
        }

        $departments = array();
        $collection = Mage::getModel('helpmate/department')->getCollection();
        foreach ($collection as $department) {
            $departments[] = array(
                'value' => $department->id,
                'label' => $department->name
            );
        }
        $fieldsetGeneral->addField('department_id', 'select', array(
            'label'     => Mage::helper('helpmate')->__('Department'),
            'name'      => 'department_id',
            'values'    => $departments
        ));

        $users = array();
        $users[] = array(
            'value' => null,
            'label' => 'Not assigned'
        );

        foreach (Mage::getModel('admin/user')->getCollection() as $user) {
            $users[] = array(
                'value' => $user->user_id,
                'label' => $user->username
            );
        }

        $fieldsetGeneral->addField('user_id', 'select', array(
            'label'  => Mage::helper('helpmate')->__('Assigned'),
            'name'   => 'user_id',
            'values' => $users
        ));


        $statuses = array();
        foreach (Mage::getModel('helpmate/status')->getOptionArray() as $key => $value) {
            $statuses[] = array(
                'value' => $key,
                'label' => $value
            );
        }
        $fieldsetGeneral->addField('status', 'select', array(
            'label'     => Mage::helper('helpmate')->__('Status'),
            'name'      => 'status',
            'values'    => $statuses
        ));

        $priorities = array();
        foreach (Mage::getModel('helpmate/priority')->getOptionArray() as $key => $value) {
            $priorities[] = array(
                'value' => $key,
                'label' => $value
            );
        }
        $fieldsetGeneral->addField('priority', 'select', array(
            'label'     => Mage::helper('helpmate')->__('Priority'),
            'name'      => 'priority',
            'values'    => $priorities
        ));

        $fieldsetGeneral->addType(
            'helpmate_autocompleter',
            'Ebs_Helpmate_Block_Adminhtml_Ticket_Edit_Form_Element_Autocompleter'
        );

        if (isset($data['customer_id'])) {
            $customer = Mage::getSingleton('customer/customer')->load($data['customer_id']);
        }
        if (!$customer) {
            $resource = Mage::getSingleton('customer/customer')->getResource();
            $connection = $resource->getReadConnection();
            $select = $connection->select()
                ->from($resource->getEntityTable(), array($resource->getEntityIdField()))
                ->where('email=:customer_email')
                ;
            $customer = Mage::getSingleton('customer/customer')->load(
                $connection->fetchOne(
                    $select, array('customer_email' => $data['email'])
                )
            );
        }

        if ($customer->getId()) {
            $data['email'] = $customer->getEmail();
            $data['customer_link'] = $customer->getName();

            $link = Mage::helper("adminhtml")->getUrl(
                "adminhtml/customer/edit",
                    array('id' => $customer->getId())
            );

            $fieldsetGeneral->addField('customer_link', 'link', array(
                'href'      => $link,
                'label'     => Mage::helper('helpmate')->__('Customer'),
                'name'      => 'customer_link'
            ));
            $data['customer_id'] = $customer->getId();
            
            $fieldsetGeneral->addField('email', 'label', array(
                'label'    => Mage::helper('helpmate')->__('Email'),
                'class'    => 'required-entry',
                'required' => true,
                'disabled' => true,
                'name'     => 'email',
            ));
            $fieldsetGeneral->addField('customer_id', 'hidden', array(
                'name' => 'customer_id'
            ));
        } elseif($isOldTicket) {
            $fieldsetGeneral->addField('email', 'label', array(
                'label'    => Mage::helper('helpmate')->__('Email'),
                'class'    => 'required-entry',
                'required' => true,
                'disabled' => true,
                'name'     => 'email',
            ));
        } else {
            $fieldsetGeneral->addField('customer_id', 'helpmate_autocompleter', array(
                'label'              => Mage::helper('helpmate')->__('Email'),
                'name'               => 'customer_id',
                'autocompleterUrl'   => Mage::getUrl('*/*/customer'),
                'autocompleterValue' => '',
                'required'           => true,
            ));
        }

        $_order = Mage::getModel('sales/order');
        if (isset($data['order_id']) && !empty($data['order_id'])) {
            $_order = $_order->load($data['order_id']);
            if ($_order) {
                $data['order_number'] = $_order->getNumber();
            }
        }

        $fieldsetGeneral->addField('order_id', 'helpmate_autocompleter', array(
            'label'     => Mage::helper('helpmate')->__('Order Number'),
            'name'      => 'order_id',
            'autocompleterUrl'   => Mage::getUrl('*/*/order'),
            'autocompleterValue' => isset($data['order_number']) ? $data['order_number'] : ''
        ));

        $field = Mage::getStoreConfig("helpmate/ticketForm/field0");
        if(!empty($field)) {
            $fieldsetGeneral->addField('field0', 'text', array(
                'label'     => Mage::helper('helpmate')->__($field),
                'name'      => 'field0',
            ));
        }

        $field = Mage::getStoreConfig("helpmate/ticketForm/field1");
        if(!empty($field)) {
            $fieldsetGeneral->addField('field1', 'text', array(
                'label'     => Mage::helper('helpmate')->__($field),
                'name'      => 'field1',
            ));
        }

        $field = Mage::getStoreConfig("helpmate/ticketForm/field2");
        if(!empty($field)) {
            $fieldsetGeneral->addField('field2', 'text', array(
                'label'     => Mage::helper('helpmate')->__($field),
                'name'      => 'field2',
            ));
        }

        if ($_order->getId()) {
            $data['order_link'] = $_order->getRealOrderId();
       
            $link = Mage::helper("adminhtml")->getUrl(
                "adminhtml/sales_order/view",
                    array('order_id' => $data['order_id'])
            );
            $fieldsetGeneral->addField('order_link', 'link', array(
                'href'      => $link,
                'label'     => Mage::helper('helpmate')->__('Order Info'),
                'name'      => 'order_link'
            ));
        }

        if ($isOldTicket) {
            $fieldsetComments = $form->addFieldset(
                'ticket_comments_form',
                array(
                    'legend' => Mage::helper('helpmate')->__('Comments')
                )
            );
            
            $fieldsetComments->addType('helpmate_theard', 'Ebs_Helpmate_Block_Adminhtml_Ticket_Edit_Form_Element_Theard');
            $fieldsetComments->addField('theard', 'helpmate_theard', array(
                'name'      => 'theard'
            ));
        }

        $fieldsetAddComment = $form->addFieldset(
            'ticket_add_comment_form',
            array(
                'legend' => Mage::helper('helpmate')->__('Add Comment')

            )
        );
        $faqs = array(array(
            'value' => '',
            'label' => ''
        ));
        foreach (Mage::getModel('knowledgebase/faq')->getCollection() as $faq) {
            $_content = strip_tags(
                @html_entity_decode($faq->content, ENT_COMPAT | ENT_HTML401, 'UTF-8')
            );
            $faqs[] = array(
                'value' => $_content,
                'label' => $faq->title
            );
        }
        $fieldsetAddComment->addField('faq', 'select', array(
            'label'     => Mage::helper('helpmate')->__('Knowledge Faq'),
            'name'      => 'faq',
            'values'    => $faqs,
            'note'      => 'Quick answer based on predefined templates',
            'onchange'  => "$('text').value = this.value;"
        ));

        $fieldsetAddComment->addField('text', 'textarea', array(
            'label'     => Mage::helper('helpmate')->__('Comment'),
            'name'      => 'text'
        ));
        $fieldsetAddComment->addField('enabled', 'checkbox', array(
            'label' => Mage::helper('helpmate')->__('Hidden comment'),
            'name'  => 'enabled',
            'id'    => 'enabled',
        ));
        $fieldsetAddComment->addField('file', 'file', array(
            'name'      => 'file',
            'label'     => Mage::helper('helpmate')->__('File'),
            'title'     => Mage::helper('helpmate')->__('File')
        ));

        $form->setValues($data);
        $onclick = "if ($('text').value == '') " .
                " $('text').addClassName('required-entry validation-failed');" .
                "editForm.submit();return false;";
        $fieldsetAddComment->addField('add', 'button', array(
           'value' => Mage::helper('helpmate')->__('Add Comment'),
           'class' => 'form-button',
           'name'  => 'add_comment_button',
           'onclick' => $onclick
        ));
        return parent::_prepareForm();
    }

    protected function _getAdditionalElementTypes()
    {
        return array(
            'image' => Mage::getConfig()->getBlockClassName('helpmate/adminhtml_ticket_helper_file')
        );
    }
}
